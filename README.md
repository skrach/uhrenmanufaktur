# Installation

Run `mvn clean install` in the root directory.

# Usage

	. env.sh

The script prints all available commands upon invocation.