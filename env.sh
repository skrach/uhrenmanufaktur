#!/bin/bash

# export JAVA_HOME=/usr/lib/jvm/java-8-oracle

export _JAVA_OPTIONS="-Djava.net.preferIPv4Stack=true"
export JMS_FACTORY="at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.jms.JMSFactory|tcp://localhost:61616"
export MZS_FACTORY="at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs.MZSFactory|xvsm://localhost:9876"

alias umf_factory_gui='mvn -f umf-gui/pom.xml exec:java -Dexec.mainClass="at.ac.tuwien.sbc2014s.g6.umf.gui.UmfGui"'
alias umf_factory_gui_dump='mvn -f umf-gui/pom.xml exec:java -Dexec.mainClass="at.ac.tuwien.sbc2014s.g6.umf.gui.UmfGui" -Dexec.args="dump"'
alias umf_distributor_gui='mvn -f umf-gui/pom.xml exec:java -Dexec.mainClass="at.ac.tuwien.sbc2014s.g6.umf.gui.DistributorGui"'

alias umf_service_factory_mzs='mvn -f umf-logistics-mzs/pom.xml exec:java -Dexec.mainClass="at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs.MZSFactoryService"'
alias umf_service_factory_jms='mvn -f umf-logistics-jms/pom.xml exec:java -Dexec.mainClass="at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.jms.JMSFactoryService" -Dexec.args="tcp://localhost:61616"'

alias umf_service_distributor_jms='mvn -f umf-logistics-jms/pom.xml exec:java -Dexec.mainClass="at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.jms.JMSDistributorService"'

function conn {
	if [ "$1" == "JMS" ]; then
		echo "$JMS_FACTORY"
	fi
	
	if [ "$1" == "MZS" ]; then
		echo "$MZS_FACTORY"
	fi
}

function umf_worker_supplier {
	if [ "$#" -ne 3 ]; then
		echo "Usage: umf_worker_supplier JMS|MZS CASING|CLOCKWORK|HAND|WRISTBAND amount"
		return 1
	fi
	
	CONN=$(conn $1)
	mvn -f umf-worker/pom.xml exec:java -Dexec.mainClass="at.ac.tuwien.sbc2014s.g6.umf.worker.supplier.SupplierWorker" -Dexec.args="$CONN $2 $3" &
}

function umf_worker_logistics {
	if [ "$#" -ne 2 ]; then
		echo "Usage: umf_worker_logistics JMS|MZS A|B"
		return 1
	fi
	
	CONN=$(conn $1)
	mvn -f umf-worker/pom.xml exec:java -Dexec.mainClass="at.ac.tuwien.sbc2014s.g6.umf.worker.logistics.LogisticsWorker" -Dexec.args="$CONN $2" &
}

function umf_worker_benchmark {
	if [ "$#" -ne 1 ]; then
		echo "Usage: umf_worker_assembly JMS|MZS"
		return 1
	fi
	
	CONN=$(conn $1)
	mvn -f umf-worker/pom.xml exec:java -Dexec.mainClass="at.ac.tuwien.sbc2014s.g6.umf.worker.AbstractWorker" -Dexec.args="$CONN"
}

function umf_worker_assembly {
	if [ "$#" -ne 1 ]; then
		echo "Usage: umf_worker_assembly JMS|MZS"
		return 1
	fi
	
	CONN=$(conn $1)
	mvn -f umf-worker/pom.xml exec:java -Dexec.mainClass="at.ac.tuwien.sbc2014s.g6.umf.worker.assembly.AssemblyWorker" -Dexec.args="$CONN" &
}

function umf_worker_qa {
	if [ "$#" -ne 1 ]; then
		echo "Usage: umf_worker_qa JMS|MZS"
		return 1
	fi
	
	CONN=$(conn $1)
	mvn -f umf-worker/pom.xml exec:java -Dexec.mainClass="at.ac.tuwien.sbc2014s.g6.umf.worker.qa.QaWorker" -Dexec.args="$CONN" &
}

function umf_worker_account_manager {
	if [ "$#" -ne 1 ]; then
		echo "Usage: umf_worker_account_manager JMS|MZS"
		return 1
	fi
	
	CONN=$(conn $1)
	mvn -f umf-worker/pom.xml exec:java -Dexec.mainClass="at.ac.tuwien.sbc2014s.g6.umf.worker.accountmanager.AccountManagerWorker" -Dexec.args="$CONN" &
}

echo -en "\xE2\x8C\x9A"
echo -n "  "

cat <<"EOF"
Uhrenmanufaktur is ready for clock production!

            (()
            ())
            (()
            ())
            (()
            ())
            (()
            ())
            (()
            .+.
        _.-//_\\-._
      .'.-' XII '-.'.
    /`.'*         *'.`\
   / /*        /    *\ \
  | ;        _/       ; |
  | |IX     (_)    III| |
  | ;         \       ; |
   \ \*        \    */ /
    \ '.*       \ *.'./
     '._'-.__VI_.-'_.'
        '-.,___,.-'


Commands:
 - umf_gui
 - umf_service_factory_mzs
 - umf_service_factory_jms
 - umf_worker_supplier
 - umf_worker_assembly
 - umf_worker_qa
 - umf_worker_logistics
 - umf_worker_account_manager
 - umf_distributor_gui
 - umf_factory_gui
EOF
