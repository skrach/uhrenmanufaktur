package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.jms;

import java.io.InterruptedIOException;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.Session;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.ItemUpdateCallback;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionTimeoutException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.Factory;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Casing;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Item;

public class JMSFactory extends AbstractJMSProvider implements Factory {
	
	private static Logger log = LogManager.getLogger(JMSFactory.class);

	public static enum FactoryQueueName implements AbstractJMSProvider.QueueName {
		BENCHMARK, COMPONENT_CASING, COMPONENT_CLOCKWORK, COMPONENT_HAND, COMPONENT_METALWRISTBAND, COMPONENT_LEATHERWRISTBAND, ASSEMBLED, TESTED, DELIVERY, DELIVERED, RECYCLED, SEQUENCE_WORKER_SUPPLIER, SEQUENCE_WORKER_ASSEMBLY, SEQUENCE_WORKER_QA, SEQUENCE_WORKER_LOGISTICS, SEQUENCE_WORKER_ACCOUNTMANAGER, SEQUENCE_WORKER_DISTRIBUTOR, SEQUENCE_ITEM_CASING, SEQUENCE_ITEM_CLOCKWORK, SEQUENCE_ITEM_HAND, SEQUENCE_ITEM_METAL_WRISTBAND, SEQUENCE_ITEM_LEATHER_WRISTBAND, SEQUENCE_ITEM_ORDER, SEQUENCE_ITEM_WATCH, DISTRIBUTOR_REGISTRATION, ITEM_ORDER, ACCOUNT_MANAGER_WORKER_ITEM, ORDER_WORK_ITEM
	}

	public JMSFactory() {
		super();
	}

	public JMSFactory(String brokerUrl) {
		super(brokerUrl);
	}

	@Override
	public List<? extends Item> readDepotComponent() throws LogisticsException {
		try {
			return this.readQueueItems(FactoryQueueName.COMPONENT_CASING, FactoryQueueName.COMPONENT_CLOCKWORK,
					FactoryQueueName.COMPONENT_HAND, FactoryQueueName.COMPONENT_LEATHERWRISTBAND,
					FactoryQueueName.COMPONENT_METALWRISTBAND);
		} catch (JMSException e) {
			throw new LogisticsException(e);
		}
	}

	@Override
	public List<? extends Item> readDepotAssembled() throws LogisticsException {
		try {
			return this.readQueueItems(FactoryQueueName.ASSEMBLED);
		} catch (JMSException e) {
			throw new LogisticsException(e);
		}
	}

	@Override
	public List<? extends Item> readDepotTested() throws LogisticsException {
		try {
			return this.readQueueItems(FactoryQueueName.TESTED);
		} catch (JMSException e) {
			throw new LogisticsException(e);
		}
	}

	@Override
	public List<? extends Item> readDepotDelivery() throws LogisticsException {
		try {
			return this.readQueueItems(FactoryQueueName.DELIVERY);
		} catch (JMSException e) {
			throw new LogisticsException(e);
		}
	}

	@Override
	public List<? extends Item> readDepotRecycled() throws LogisticsException {
		try {
			return this.readQueueItems(FactoryQueueName.RECYCLED);
		} catch (JMSException e) {
			throw new LogisticsException(e);
		}
	}

	@Override
	public List<? extends Item> readDepotOrder() throws LogisticsException {
		try {
			return this.readQueueItems(FactoryQueueName.ITEM_ORDER);
		} catch (JMSException e) {
			throw new LogisticsException(e);
		}
	}

	@Override
	public List<? extends Item> readDepotDelivered() throws LogisticsException {
		try {
			return this.readQueueItems(FactoryQueueName.DELIVERED);
		} catch (JMSException e) {
			throw new LogisticsException(e);
		}
	}

	@Override
	public List<? extends Item> readListDistributorRegistrations() throws LogisticsException {
		try {
			return this.readQueueItems(FactoryQueueName.DISTRIBUTOR_REGISTRATION);
		} catch (JMSException e) {
			throw new LogisticsException(e);
		}
	}

	@Override
	public void subscribeToDepotComponent(ItemUpdateCallback callback) {
		this.queueChangeCallBacks.put(FactoryQueueName.COMPONENT_CASING, callback);
		this.queueChangeCallBacks.put(FactoryQueueName.COMPONENT_CLOCKWORK, callback);
		this.queueChangeCallBacks.put(FactoryQueueName.COMPONENT_HAND, callback);
		this.queueChangeCallBacks.put(FactoryQueueName.COMPONENT_LEATHERWRISTBAND, callback);
		this.queueChangeCallBacks.put(FactoryQueueName.COMPONENT_METALWRISTBAND, callback);
	}

	@Override
	public void subscribeToDepotAssembled(ItemUpdateCallback callback) {
		this.queueChangeCallBacks.put(FactoryQueueName.ASSEMBLED, callback);
	}

	@Override
	public void subscribeToDepotTested(ItemUpdateCallback callback) {
		this.queueChangeCallBacks.put(FactoryQueueName.TESTED, callback);
	}

	@Override
	public void subscribeToDepotDelivery(ItemUpdateCallback callback) {
		this.queueChangeCallBacks.put(FactoryQueueName.DELIVERY, callback);
	}

	@Override
	public void subscribeToDepotRecycled(ItemUpdateCallback callback) {
		this.queueChangeCallBacks.put(FactoryQueueName.RECYCLED, callback);
	}

	@Override
	public void subscribeToDepotOrder(ItemUpdateCallback callback) {
		this.queueChangeCallBacks.put(FactoryQueueName.ITEM_ORDER, callback);
	}

	@Override
	public void subscribeToListDistributorRegistrations(ItemUpdateCallback callback) {
		this.queueChangeCallBacks.put(FactoryQueueName.DISTRIBUTOR_REGISTRATION, callback);
	}

	@Override
	public void subscribeToDepotDelivered(ItemUpdateCallback callback) {
		this.queueChangeCallBacks.put(FactoryQueueName.DELIVERED, callback);
	}

	@Override
	public QueueName getQueueName(String value) {
		return FactoryQueueName.valueOf(value);
	}

	@Override
	public QueueName[] getQueueNames() {
		return FactoryQueueName.values();
	}

	@Override
	public JMSFactoryTransaction begin() throws LogisticsTransactionException, InterruptedException {
		try {
			return new JMSFactoryTransaction(this.connection.createSession(true, Session.SESSION_TRANSACTED));
		} catch (JMSException e) {
			if (e.getCause() instanceof InterruptedException || e.getCause() instanceof InterruptedIOException) {
				throw new InterruptedException();
			} else {
				throw new LogisticsTransactionException(e);
			}
		}
	}

	@Override
	public void waitForBenchmarkSignal() {
		try {
			AbstractJMSTransactionProvider transaction = this.getInternalTransaction();
			transaction.takeObject(JMSFactory.FactoryQueueName.BENCHMARK);
			transaction.commit();
		} catch (LogisticsTransactionException | InterruptedException e) {
			throw new RuntimeException(e);
		}
		catch (JMSException e) {
			throw new RuntimeException(e);
		} catch (LogisticsTransactionTimeoutException e) {
			throw new RuntimeException(e);
		}

	}

	@Override
	public void signalBenchmarkStart() {
		try {
			AbstractJMSTransactionProvider transaction = this.getInternalTransaction();
			// TODO clean up
			transaction.putObject(JMSFactory.FactoryQueueName.BENCHMARK, new Casing("1", "bli"));
			transaction.putObject(JMSFactory.FactoryQueueName.BENCHMARK, new Casing("1", "bli"));
			transaction.putObject(JMSFactory.FactoryQueueName.BENCHMARK, new Casing("1", "bli"));
			transaction.putObject(JMSFactory.FactoryQueueName.BENCHMARK, new Casing("1", "bli"));
			transaction.putObject(JMSFactory.FactoryQueueName.BENCHMARK, new Casing("1", "bli"));
			transaction.putObject(JMSFactory.FactoryQueueName.BENCHMARK, new Casing("1", "bli"));
			transaction.putObject(JMSFactory.FactoryQueueName.BENCHMARK, new Casing("1", "bli"));
			transaction.putObject(JMSFactory.FactoryQueueName.BENCHMARK, new Casing("1", "bli"));
			transaction.putObject(JMSFactory.FactoryQueueName.BENCHMARK, new Casing("1", "bli"));
			transaction.putObject(JMSFactory.FactoryQueueName.BENCHMARK, new Casing("1", "bli"));
			transaction.commit();
		} catch (LogisticsTransactionException e) {
			throw new RuntimeException(e);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		} catch (JMSException e) {
			throw new RuntimeException(e);
		} catch (LogisticsTransactionTimeoutException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void start() throws LogisticsException {
		super.start();
		log.info("Started JMSFactory instance ");
	}
}
