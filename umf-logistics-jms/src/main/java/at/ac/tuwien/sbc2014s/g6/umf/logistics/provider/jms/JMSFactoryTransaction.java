package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.jms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.commons.configuration.Configuration;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.WatchType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionTimeoutException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.ComponentType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.FactoryTransaction;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Casing;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Clockwork;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Component;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.DistributorRegistration;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Hand;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Item;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.LeatherWristband;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.MetalWristband;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Order;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.OrderWorkItem;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Watch;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.jms.config.JMSFactoryTransactionConfig;

public class JMSFactoryTransaction extends AbstractJMSTransactionProvider implements FactoryTransaction {
	private final Configuration configuration;

	@Override
	protected void initialize() throws JMSException {
		super.initialize();

		for (AbstractJMSProvider.QueueName queueName : JMSFactory.FactoryQueueName.values()) {
			Queue queue = this.session.createQueue(queueName.toString());
			this.queues.put(queueName, this.session.createQueue(queueName.toString()));
			this.producers.put(queue, this.session.createProducer(queue));
		}
	}

	private static enum MessageSelectorDistributorRegistration {
		REGISTRATION_ID, STOCK_FULL_CLASSIC_WATCH, STOCK_FULL_SPORT_WATCH, STOCK_FULL_SPORT_WATCH_TZS;

		public static MessageSelectorDistributorRegistration getMessageSelectorDistributorRegistration(WatchType watchType) {
			switch (watchType) {
			case CLASSIC_WATCH:
				return MessageSelectorDistributorRegistration.STOCK_FULL_CLASSIC_WATCH;
			case SPORT_WATCH:
				return MessageSelectorDistributorRegistration.STOCK_FULL_SPORT_WATCH;
			case SPORT_WATCH_TZS:
				return MessageSelectorDistributorRegistration.STOCK_FULL_SPORT_WATCH_TZS;
			default:
				throw new IllegalStateException("Unknown watch type encountered");
			}
		}
	}

	private static enum MessageSelectorOrder {
		PRIORITY, ORDER_ID, OPEN_ORDER_COUNT_CLASSIC_WATCH, OPEN_ORDER_COUNT_SPORT_WATCH, OPEN_ORDER_COUNT_SPORT_WATCH_TZS;
	};

	private static enum MessageSelectorOrderWorkItem {
		ORDER_ID, PRIORITY, WATCH_TYPE
	}

	private static enum MessageSelectorWatch {
		QUALITY_RATING, WATCH_TYPE, WATCH_ID
	};

	protected JMSFactoryTransaction(Session session) throws JMSException {
		super(session);
		this.configuration = JMSFactoryTransactionConfig.getConfiguration();
	}

	@Override
	public void put(Component component) throws LogisticsTransactionException {
		String componentType = component.getClass().getSimpleName().toUpperCase();
		try {
			AbstractJMSProvider.QueueName queueName = JMSFactory.FactoryQueueName.valueOf(String.format("COMPONENT_%s",
					componentType));
			this.putObject(queueName, component);
		} catch (IllegalArgumentException e) {
			throw new LogisticsTransactionException(String.format("No queue defined for the component type %s",
					componentType));
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}
	}

	private Message createOrderMessage(Order order) throws JMSException {
		Message message = this.session.createObjectMessage(order);
		message.setIntProperty(MessageSelectorOrder.PRIORITY.toString(), order.getPriority());
		message.setIntProperty(MessageSelectorOrder.OPEN_ORDER_COUNT_CLASSIC_WATCH.toString(),
				order.getOpenOrderCountClassicWatch());
		message.setIntProperty(MessageSelectorOrder.OPEN_ORDER_COUNT_SPORT_WATCH.toString(),
				order.getOpenOrderCountSportWatch());
		message.setIntProperty(MessageSelectorOrder.OPEN_ORDER_COUNT_SPORT_WATCH_TZS.toString(),
				order.getOpenOrderCountSportWatchTZS());
		message.setStringProperty(MessageSelectorOrder.ORDER_ID.toString(), order.getId());
		return message;
	}

	@Override
	public void put(DistributorRegistration distributorRegistration) throws LogisticsTransactionException,
			LogisticsTransactionTimeoutException {

		try {
			Message message = this.session.createObjectMessage(distributorRegistration);
			message.setStringProperty(MessageSelectorDistributorRegistration.REGISTRATION_ID.toString(),
					distributorRegistration.getId());
			message.setBooleanProperty(MessageSelectorDistributorRegistration.STOCK_FULL_CLASSIC_WATCH.toString(),
					distributorRegistration.isStockFullClassicWatch());
			message.setBooleanProperty(MessageSelectorDistributorRegistration.STOCK_FULL_SPORT_WATCH.toString(),
					distributorRegistration.isStockFullSportWatch());
			message.setBooleanProperty(MessageSelectorDistributorRegistration.STOCK_FULL_SPORT_WATCH_TZS.toString(),
					distributorRegistration.isStockFullSportWatchTZS());
			this.putMessage(JMSFactory.FactoryQueueName.DISTRIBUTOR_REGISTRATION, message);
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}
	}

	@Override
	public void update(DistributorRegistration distributorRegistration) throws LogisticsTransactionException,
			LogisticsTransactionTimeoutException {

		try {
			// TODO @ Daniel: vllt kann man das noch vereinfachen, hab die put funktion kopiert um die interface änderung abzudecken
			Message message = this.session.createObjectMessage(distributorRegistration);
			message.setStringProperty(MessageSelectorDistributorRegistration.REGISTRATION_ID.toString(),
					distributorRegistration.getId());
			message.setBooleanProperty(MessageSelectorDistributorRegistration.STOCK_FULL_CLASSIC_WATCH.toString(),
					distributorRegistration.isStockFullClassicWatch());
			message.setBooleanProperty(MessageSelectorDistributorRegistration.STOCK_FULL_SPORT_WATCH.toString(),
					distributorRegistration.isStockFullSportWatch());
			message.setBooleanProperty(MessageSelectorDistributorRegistration.STOCK_FULL_SPORT_WATCH_TZS.toString(),
					distributorRegistration.isStockFullSportWatchTZS());
			this.putMessage(JMSFactory.FactoryQueueName.DISTRIBUTOR_REGISTRATION, message);

			// add distributor registration to account manager worker item queue
			this.putMessage(JMSFactory.FactoryQueueName.ACCOUNT_MANAGER_WORKER_ITEM, message);
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}
	}

	@Override
	public void put(Order order) throws LogisticsTransactionException {
		try {
			Message message = this.createOrderMessage(order);
			this.putMessage(JMSFactory.FactoryQueueName.ITEM_ORDER, message);
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}
	}

	private void setWatchMessageProperties(Watch watch, Message message) throws JMSException {
		if (watch.getQualityRating() != null) {
			message.setIntProperty(MessageSelectorWatch.QUALITY_RATING.toString(), watch.getQualityRating());
		}

		message.setStringProperty(MessageSelectorWatch.WATCH_ID.toString(), watch.getId());
		message.setStringProperty(MessageSelectorWatch.WATCH_TYPE.toString(), WatchType.getWatchType(watch).toString());
	}

	@Override
	public void put(Watch watch) throws LogisticsTransactionException {
		try {
			Message message = this.session.createObjectMessage(watch);
			this.setWatchMessageProperties(watch, message);

			if (watch.getDistributorId() != null) {
				this.putMessage(JMSFactory.FactoryQueueName.DELIVERED, message);
			} else if (watch.getQualityRating() != null && watch.getQualityRating() > 0) {
				// quality rating has been set
				// attach quality rating as JMS property, this is needed for
				// filtering
				this.putMessage(JMSFactory.FactoryQueueName.TESTED, message);
			} else {
				this.putMessage(JMSFactory.FactoryQueueName.ASSEMBLED, message);
			}
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}
	}

	@Override
	public void dispatch(Watch watch) throws LogisticsTransactionException, InterruptedException,
			LogisticsTransactionTimeoutException {
		try {
			Message message = this.session.createObjectMessage(watch);
			this.setWatchMessageProperties(watch, message);
			this.putMessage(JMSFactory.FactoryQueueName.DELIVERY, message);

			// add watch to account manager worker item queue
			this.putMessage(JMSFactory.FactoryQueueName.ACCOUNT_MANAGER_WORKER_ITEM, message);
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}
	}

	@Override
	public void recycle(Watch watch) throws LogisticsTransactionException {
		try {
			this.putObject(JMSFactory.FactoryQueueName.RECYCLED, watch);
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}
	}

	@Override
	public Casing takeCasing() throws LogisticsTransactionException, InterruptedException,
			LogisticsTransactionTimeoutException {
		try {
			return this.takeObject(JMSFactory.FactoryQueueName.COMPONENT_CASING, null,
					this.configuration.getInt(JMSFactoryTransactionConfig.TAKE_TIMEOUT_COMPONENT) * 1000, true);
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}
	}

	@Override
	public Clockwork takeClockwork() throws LogisticsTransactionException, InterruptedException,
			LogisticsTransactionTimeoutException {
		try {
			return this.takeObject(JMSFactory.FactoryQueueName.COMPONENT_CLOCKWORK, null,
					this.configuration.getInt(JMSFactoryTransactionConfig.TAKE_TIMEOUT_COMPONENT) * 1000, true);
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}
	}

	@Override
	public Hand takeHand() throws LogisticsTransactionException, InterruptedException, LogisticsTransactionTimeoutException {
		try {
			return this.takeObject(JMSFactory.FactoryQueueName.COMPONENT_HAND, null,
					this.configuration.getInt(JMSFactoryTransactionConfig.TAKE_TIMEOUT_COMPONENT) * 1000, true);
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}
	}

	@Override
	public MetalWristband takeMetalWristband() throws LogisticsTransactionException, InterruptedException,
			LogisticsTransactionTimeoutException {
		try {
			return this.takeObject(JMSFactory.FactoryQueueName.COMPONENT_METALWRISTBAND, null,
					this.configuration.getInt(JMSFactoryTransactionConfig.TAKE_TIMEOUT_COMPONENT) * 1000, true);
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}
	}

	@Override
	public LeatherWristband takeLeatherWristband() throws LogisticsTransactionException, InterruptedException,
			LogisticsTransactionTimeoutException {
		try {
			return this.takeObject(JMSFactory.FactoryQueueName.COMPONENT_LEATHERWRISTBAND, null,
					this.configuration.getInt(JMSFactoryTransactionConfig.TAKE_TIMEOUT_COMPONENT) * 1000, true);
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}
	}

	@Override
	public Watch takeWatch() throws LogisticsTransactionException, InterruptedException,
			LogisticsTransactionTimeoutException {
		try {
			return this.takeObject(JMSFactory.FactoryQueueName.ASSEMBLED);
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}
	}

	@Override
	public Watch takeWatch(int qualityRatingLowerBound, int qualityRatingUpperBound, int timeout)
			throws LogisticsTransactionException, InterruptedException, LogisticsTransactionTimeoutException {
		String messageSelector = String.format("%s >= %d AND %s <= %d", MessageSelectorWatch.QUALITY_RATING,
				qualityRatingLowerBound, MessageSelectorWatch.QUALITY_RATING, qualityRatingUpperBound);

		try {
			return this.takeObject(JMSFactory.FactoryQueueName.TESTED, messageSelector, timeout, false);
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}
	}

	@Override
	public DistributorRegistration takeDistributorRegistration(String distributorInfoId)
			throws LogisticsTransactionException, InterruptedException, LogisticsTransactionTimeoutException {

		String messageSelector = String.format("%s = '%s'", MessageSelectorDistributorRegistration.REGISTRATION_ID,
				distributorInfoId);

		try {
			return this.takeObject(JMSFactory.FactoryQueueName.DISTRIBUTOR_REGISTRATION, messageSelector, 0, false);
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}
	}

	@Override
	public DistributorRegistration takeNextDistributorRegistration(WatchType watchType)
			throws LogisticsTransactionException, InterruptedException, LogisticsTransactionTimeoutException {
		try {
			// read distributors whose stock is not full for the given watch
			// type
			String messageSelector = String.format("%s = false",
					MessageSelectorDistributorRegistration.getMessageSelectorDistributorRegistration(watchType));

			while (true) {
				List<DistributorRegistration> registrations = this.browseQueue(
						JMSFactory.FactoryQueueName.DISTRIBUTOR_REGISTRATION, messageSelector);

				Collections.sort(registrations);

				if (registrations.size() == 0) {
					// no distributor wants this watch type
					return null;
				}

				DistributorRegistration candidate = registrations.get(0);

				// try to take this distributor
				// this can fail because it's possible the distributor has been
				// taken after we browsed the queue
				messageSelector = String.format("%s = '%s'", MessageSelectorDistributorRegistration.REGISTRATION_ID,
						candidate);
				DistributorRegistration distributorRegistration = this.takeObject(
						JMSFactory.FactoryQueueName.DISTRIBUTOR_REGISTRATION, messageSelector, -1, false);

				if (distributorRegistration != null) {
					return distributorRegistration;
				} else {
					// the chosen distributor is no longer available
					// try again
					continue;
				}
			}

		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}
	}

	@Override
	public boolean isComponentAvailable(ComponentType c, int amount) throws LogisticsTransactionException, InterruptedException, LogisticsTransactionTimeoutException {
		int componentCount = 0;

		List<Item> items = new ArrayList<Item>();
		JMSFactory.FactoryQueueName queueName;

		try {
			switch (c) {
			case CASING:
				queueName = JMSFactory.FactoryQueueName.COMPONENT_CASING;
				break;
			case CLOCKWORK:
				queueName = JMSFactory.FactoryQueueName.COMPONENT_CLOCKWORK;
				break;
			case HAND:
				queueName = JMSFactory.FactoryQueueName.COMPONENT_HAND;
				break;
			case LEATHERWRISTBAND:
				queueName = JMSFactory.FactoryQueueName.COMPONENT_LEATHERWRISTBAND;
				break;
			case METALWRISTBAND:
				queueName = JMSFactory.FactoryQueueName.COMPONENT_METALWRISTBAND;
				break;
			default:
				throw new IllegalStateException(String.format("Unexpected compontent type %s encountered", c));
			}

			for (int i = 0; i < amount; i++) {
				Item item = this.takeObject(queueName, null, -1, false);
				if (item != null) {
					componentCount++;
					items.add(item);
				}
			}

			for (Item item : items) {
				this.putObject(queueName, item);
			}

		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}

		return componentCount >= amount;
	}

	@Override
	public Item takeAccountManagerWorkerItem() throws LogisticsTransactionException, InterruptedException,
			LogisticsTransactionTimeoutException {
		try {
			return this.takeObject(JMSFactory.FactoryQueueName.ACCOUNT_MANAGER_WORKER_ITEM);
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}
	}

	@Override
	public Watch takeDeliveryWatch(String watchId) throws InterruptedException, LogisticsTransactionTimeoutException,
			LogisticsTransactionException {
		String messageSelector = String.format("%s = '%s'", MessageSelectorWatch.WATCH_ID, watchId);
		try {
			return this.takeObject(JMSFactory.FactoryQueueName.DELIVERY, messageSelector, -1, false);
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}
	}

	@Override
	public Watch takeNextDeliveryWatch(WatchType watchType) throws InterruptedException,
			LogisticsTransactionTimeoutException, LogisticsTransactionException {
		String messageSelector = String.format("%s = '%s'", MessageSelectorWatch.WATCH_TYPE, watchType);
		try {
			return this.takeObject(JMSFactory.FactoryQueueName.DELIVERY, messageSelector, -1, false);
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}
	}

	@Override
	public void put(OrderWorkItem workItem) throws LogisticsTransactionException,
			LogisticsTransactionTimeoutException {
		try {
			Message message = this.session.createObjectMessage(workItem);
			message.setStringProperty(MessageSelectorOrderWorkItem.ORDER_ID.toString(), workItem.getOrderId());
			message.setIntProperty(MessageSelectorOrderWorkItem.PRIORITY.toString(), workItem.getPriority());
			message.setStringProperty(MessageSelectorOrderWorkItem.WATCH_TYPE.toString(), workItem.getWatchtype().toString());
			this.putMessage(JMSFactory.FactoryQueueName.ORDER_WORK_ITEM, message);
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}
	}

	@Override
	public OrderWorkItem takeOrderWorkItem(Set<WatchType> candidates, boolean prevOrder) throws InterruptedException, LogisticsTransactionTimeoutException, LogisticsTransactionException {
		class MessageSelector {
			public String getSelector(int priority, Set<WatchType> candidates) {

				StringBuilder messageSelector = new StringBuilder();

				// filter on priority
				messageSelector.append(String.format("%s = %d", MessageSelectorOrder.PRIORITY, priority));

				// filter on
				String predicate = candidates.stream()
						.map(e -> String.format("%s = '%s'", MessageSelectorOrderWorkItem.WATCH_TYPE, e))
						.collect(Collectors.joining(" OR "));

				if (predicate.length() > 0) {
					messageSelector.append(String.format(" AND (%s)", predicate));
				}

				return messageSelector.toString();
			}
		};

		MessageSelector messageSelector = new MessageSelector();

		try {
			for (int priority : new int[] { Order.PRIORITY_HIGH, Order.PRIORITY_NORMAL, Order.PRIORITY_LOW }) {
				OrderWorkItem orderWorkItem = this.takeObject(JMSFactory.FactoryQueueName.ORDER_WORK_ITEM,
						messageSelector.getSelector(priority, candidates), -1, false);

				if (orderWorkItem != null) {
					return orderWorkItem;
				}
			}
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}

		return null;
	}

	@Override
	public Order takeOrder(String orderId) throws InterruptedException, LogisticsTransactionTimeoutException, LogisticsTransactionException {
		String messageSelector = String.format("%s = '%s'", MessageSelectorOrder.ORDER_ID, orderId);
		try {
			return this.takeObject(JMSFactory.FactoryQueueName.ITEM_ORDER, messageSelector, 0, false);
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}
	}
}
