package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.jms;

public class QueueReferer {
	
	AbstractJMSProvider.QueueName queueName;
	String messageSelector;
	
	public QueueReferer(AbstractJMSProvider.QueueName queueName, String messageSelector) {
		super();
		this.queueName = queueName;
		this.messageSelector = messageSelector;
	}

	public AbstractJMSProvider.QueueName getQueueName() {
		return queueName;
	}

	public String getMessageSelector() {
		return messageSelector;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((messageSelector == null) ? 0 : messageSelector.hashCode());
		result = prime * result + ((queueName == null) ? 0 : queueName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QueueReferer other = (QueueReferer) obj;
		if (messageSelector == null) {
			if (other.messageSelector != null)
				return false;
		} else if (!messageSelector.equals(other.messageSelector))
			return false;
		if (queueName == null) {
			if (other.queueName != null)
				return false;
		} else if (!queueName.equals(other.queueName))
			return false;
		return true;
	}	
}
