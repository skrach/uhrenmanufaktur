package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.jms;

import java.io.InterruptedIOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.jms.Topic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionTimeoutException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.IdPrefix;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.IdType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.jms.JMSFactory.FactoryQueueName;

public class AbstractJMSTransactionProvider {

	protected Session session;
	protected final Map<AbstractJMSProvider.QueueName, Queue> queues = new HashMap<AbstractJMSProvider.QueueName, Queue>();
	protected final Map<Queue, MessageProducer> producers = new HashMap<Queue, MessageProducer>();
	private final Map<QueueReferer, MessageConsumer> consumers = new HashMap<QueueReferer, MessageConsumer>();
	protected final Map<QueueReferer, QueueBrowser> browsers = new HashMap<QueueReferer, QueueBrowser>();
	protected MessageProducer topicQueueUpdateProducer;
	public static final String PREFIX_SEQUENCE = "SEQUENCE_";
	private static final Logger log = LogManager.getLogger(AbstractJMSTransactionProvider.class);

	public AbstractJMSTransactionProvider() {
		super();
	}

	protected void initialize() throws JMSException {
		Topic topic = this.session.createTopic(AbstractJMSProvider.TOPIC_QUEUE_UPDATES);
		this.topicQueueUpdateProducer = this.session.createProducer(topic);
	}

	protected AbstractJMSTransactionProvider(Session session) throws JMSException {
		this.session = session;
		this.initialize();
	}

	private MessageProducer getProducer(AbstractJMSProvider.QueueName queueName) {
		return this.producers.get(this.queues.get(queueName));
	}

	private MessageConsumer getConsumer(AbstractJMSProvider.QueueName queueName, String messageSelector) throws JMSException {
		QueueReferer queueReferer = new QueueReferer(queueName, messageSelector);
		MessageConsumer consumer = this.consumers.get(queueReferer);

		if (consumer == null) {
			consumer = this.session.createConsumer(this.queues.get(queueName), messageSelector);
			this.consumers.put(queueReferer, consumer);
		}

		return consumer;
	}

	private QueueBrowser getBrowser(AbstractJMSProvider.QueueName queueName, String messageSelector) throws JMSException {
		QueueReferer queueReferer = new QueueReferer(queueName, messageSelector);
		QueueBrowser browser = this.browsers.get(queueReferer);
		
		if (browser == null) {
			browser = this.session.createBrowser(this.queues.get(queueName), messageSelector);
			this.browsers.put(queueReferer, browser);
		}
		
		return browser;
	}

	void putMessage(AbstractJMSProvider.QueueName queueName, Message message) throws JMSException {
		if (message instanceof ObjectMessage) {
			log.debug(String.format("Putting object %s to queue %s", ((ObjectMessage) message).getObject(), queueName));	
		} else {
			log.debug(String.format("Putting message %s to queue %s", message, queueName));	
		}
		
		MessageProducer producer = this.getProducer(queueName);
		producer.send(message);

		// publish queue change
		this.publishQueueChange(queueName, AbstractJMSProvider.QueueOperation.PUT, ((ObjectMessage) message).getObject());
	}

	protected void putObject(AbstractJMSProvider.QueueName queueName, Serializable object) throws JMSException {
		this.putMessage(queueName, this.session.createObjectMessage(object));
	}

	@SuppressWarnings("unchecked")
	<T> T takeObject(AbstractJMSProvider.QueueName queueName, String messageSelector, int timeout, boolean throwOnTimeout) throws JMSException,
			InterruptedException, LogisticsTransactionTimeoutException, LogisticsTransactionException {
		try {
			log.debug(String.format("Taking object from queue %s (selector: %s) with timeout %d", queueName, messageSelector, timeout));
			MessageConsumer consumer = this.getConsumer(queueName, messageSelector);
			ObjectMessage message;
			

			if (timeout > 0) {
				message = (ObjectMessage) consumer.receive(timeout);
				if (message == null && throwOnTimeout) {
					this.rollback();
					throw new LogisticsTransactionTimeoutException("JMS Timeout exceeded");
				}
			} else if (timeout == 0) {
				message = (ObjectMessage) consumer.receive();
			} else if (timeout < 0) {
				message = (ObjectMessage) consumer.receiveNoWait();
			} else {
				throw new IllegalStateException();
			}

			if (message == null) {
				log.debug("No object of requested utype available in queue");
				return null;
			}

			T object = (T) message.getObject();
			log.debug(String.format("Will return object %s", object));

			// publish queue change
			this.publishQueueChange(queueName, AbstractJMSProvider.QueueOperation.TAKE, (Serializable) object);
			
			return object;
		} catch (JMSException e) {
			if (e.getCause() instanceof InterruptedException || e.getCause() instanceof InterruptedIOException) {
				throw new InterruptedException();
			} else {
				throw e;
			}
		}
	}

	<T> T takeObject(AbstractJMSProvider.QueueName queueName) throws JMSException, InterruptedException, LogisticsTransactionTimeoutException, LogisticsTransactionException {
		return this.takeObject(queueName, null, 0, false);
	}
	
	protected <T> List<T> browseQueue(AbstractJMSProvider.QueueName queueName) throws JMSException {
		return browseQueue(queueName, null);
	}

	@SuppressWarnings("unchecked")
	protected <T> List<T> browseQueue(AbstractJMSProvider.QueueName queueName, String messageSelector) throws JMSException {
		log.debug(String.format("Browsing queue %s (selector: %s)", queueName, messageSelector));
		QueueBrowser browser = this.getBrowser(queueName, messageSelector);
		Enumeration<T> enumeration = browser.getEnumeration();
		List<T> elements = new ArrayList<T>();
		while (enumeration.hasMoreElements()) {
			ObjectMessage message = (ObjectMessage) enumeration.nextElement();

			if (message != null) {
				T element = (T) message.getObject();
				elements.add(element);
			}
		}

		return elements;
	}

	private void publishQueueChange(AbstractJMSProvider.QueueName queueName, AbstractJMSProvider.QueueOperation operation,
			Serializable object) throws JMSException {
		log.debug(String.format("Publishing %s operation on queue %s", queueName, operation));
		Message message = this.session.createObjectMessage(object);
		message.setStringProperty(AbstractJMSProvider.TOPIC_QUEUE_UPDATES_PROP_QUEUE_NAME, queueName.toString());
		message.setStringProperty(AbstractJMSProvider.TOPIC_QUEUE_UPDATES_PROP_QUEUE_OP, operation.toString());
		this.topicQueueUpdateProducer.send(message);
	}

	public String getNewId(IdType type) throws LogisticsTransactionException, InterruptedException, LogisticsTransactionTimeoutException {
		try {
			FactoryQueueName queueName = JMSFactory.FactoryQueueName.valueOf(PREFIX_SEQUENCE + type.toString());
			Integer id = this.takeObject(queueName);
			id++;
			this.putObject(queueName, id);
			this.commit();
			return IdPrefix.getPrefixedId(id, type);
		} catch (IllegalArgumentException e) {
			throw new LogisticsTransactionException(String.format("No queue defined for type %s", type.toString()), e);
		} catch (JMSException | LogisticsTransactionException e) {
			throw new LogisticsTransactionException(e);
		}
	}

	public void commit() throws LogisticsTransactionException, InterruptedException, LogisticsTransactionTimeoutException {
		try {
			this.session.commit();
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}

	}

	public void rollback() throws LogisticsTransactionException {
		try {
			this.session.rollback();
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}
	}
}