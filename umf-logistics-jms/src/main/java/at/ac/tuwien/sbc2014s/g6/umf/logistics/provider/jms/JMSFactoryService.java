package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.jms;

import java.io.IOException;

import javax.jms.JMSException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsServiceException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionTimeoutException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.FactoryService;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.jms.AbstractJMSProvider.QueueName;

public class JMSFactoryService extends AbstractJMSService implements FactoryService {

	private static Logger log = LogManager.getLogger(JMSFactoryService.class);
	private JMSFactory factory;

	/**
	 * Start JMS Broker Service.
	 *
	 * @param args
	 *            Startup options. E.g. tcp://localhost:61616
	 * @throws Exception
	 *             Thrown if errors encountered during startup
	 */
	public static void main(String[] args) {
		JMSFactoryService service = new JMSFactoryService();

		try {
			if (args.length < 1) {
				service.startService(null);
			} else {
				service.startService(args[0]);
			}

			log.info("Press ENTER key to stop Broker");
			System.in.read();
			log.info("Stopping JMS Broker...");
			service.stopService();
		} catch (LogisticsServiceException | IOException e) {
			throw new RuntimeException(e);
		}
		System.exit(0);
	}

	@Override
	public void afterStartup() throws LogisticsServiceException {
		super.afterStartup();

		try {
			log.info("Populating sequence queues");
			this.factory = new JMSFactory("vm://localhost");
			JMSFactoryTransaction transaction = this.factory.begin();

			for (QueueName queueName : this.factory.getQueueNames()) {
				if (queueName.toString().startsWith(JMSFactoryTransaction.PREFIX_SEQUENCE)) {
					transaction.putObject(queueName, new Integer(0));
				}
			}

			transaction.commit();
		} catch (LogisticsTransactionException | InterruptedException | JMSException | LogisticsTransactionTimeoutException e) {
			throw new LogisticsServiceException(e);
		}
	}

	@Override
	public void startService(String parameter) throws LogisticsServiceException {
		this.startJMSService(parameter);
	}
}
