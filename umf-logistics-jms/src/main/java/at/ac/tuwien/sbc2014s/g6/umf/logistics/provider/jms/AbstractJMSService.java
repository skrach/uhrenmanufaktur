package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.jms;

import org.apache.activemq.broker.BrokerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsServiceException;
import at.ac.tuwien.sbc2014s.g6.umf.util.AvailablePortFinder;


public abstract class AbstractJMSService {

	private static Logger log = LogManager.getLogger(AbstractJMSService.class);
	private BrokerService broker;
	private String bindAddress;

	protected void afterStartup() throws LogisticsServiceException {
	}

	protected void beforeShutdown() throws LogisticsServiceException {

	}

	public void startJMSService(String bindAddress) throws LogisticsServiceException {
		this.broker = new BrokerService();
		this.broker.setPersistent(false);
		this.broker.setUseJmx(false);

		try {
			if (bindAddress == null) {
				int port = AvailablePortFinder.getNextAvailable();
				this.bindAddress = String.format("tcp://localhost:%s", port);
			} else {
				this.bindAddress = bindAddress;
			}

			this.broker.addConnector(String.format("%s", this.bindAddress));

			log.info("Starting JMS Broker...");
			this.broker.start();
			this.broker.waitUntilStarted();

			log.info("Started JMS Broker");
			this.afterStartup();
		} catch (Exception e) {
			throw new LogisticsServiceException( "Exception occurred while trying to start service.", e );
		}
	}

	public void stopService() throws LogisticsServiceException {
		try {
			this.beforeShutdown();
			log.info("Stopping JMS Broker...");
			this.broker.stop();
			this.broker.waitUntilStopped();
			log.info("Stopped");
		} catch (Exception e) {
			throw new LogisticsServiceException( "Exception occurred while trying to stop service.", e );
		}
	}

	protected String getBindAddress() {
		return this.bindAddress;
	}
}
