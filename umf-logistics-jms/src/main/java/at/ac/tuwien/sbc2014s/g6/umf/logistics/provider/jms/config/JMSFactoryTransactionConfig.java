package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.jms.config;

import org.apache.commons.configuration.Configuration;

import at.ac.tuwien.sbc2014s.g6.umf.util.Config;

public class JMSFactoryTransactionConfig {
	
	public static final String TAKE_TIMEOUT_COMPONENT = "TakeTimeout.Component";

	public static Configuration getConfiguration() {
		return Config.getConfig("jmsfactorytransaction.properties");
	}
}
