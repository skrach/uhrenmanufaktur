package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.jms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ActiveMQPrefetchPolicy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.ItemUpdateCallback;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionTimeoutException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.IdType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Item;

public abstract class AbstractJMSProvider implements MessageListener {

	public interface QueueName {
	};

	public static enum QueueOperation {
		PUT, TAKE
	}

	public static final String TOPIC_QUEUE_UPDATES = "TOPIC_QUEUE_UPDATES";
	public static final String TOPIC_QUEUE_UPDATES_PROP_QUEUE_NAME = "TOPIC_QUEUE_UPDATES_PROP_QUEUE_NAME";
	public static final String TOPIC_QUEUE_UPDATES_PROP_QUEUE_OP = "TOPIC_QUEUE_UPDATES_PROP_QUEUE_OP";
	protected ActiveMQConnection connection;
	private Session internalSession;
	private AbstractJMSTransactionProvider internalTransaction;
	private String brokerUrl;

	protected final Map<QueueName, ItemUpdateCallback> queueChangeCallBacks = new HashMap<QueueName, ItemUpdateCallback>();;

	private static Logger log = LogManager.getLogger(AbstractJMSProvider.class);
	private MessageConsumer topicConsumer;

	public abstract QueueName getQueueName(String value);

	public abstract QueueName[] getQueueNames();

	public abstract AbstractJMSTransactionProvider begin() throws LogisticsTransactionException, InterruptedException;

	public AbstractJMSProvider(String brokerUrl) {
		this.brokerUrl = brokerUrl;
		// connect to JMS Server
		ActiveMQConnectionFactory connection = new ActiveMQConnectionFactory(brokerUrl);
		ActiveMQPrefetchPolicy prefetchPolicy = new ActiveMQPrefetchPolicy();
		prefetchPolicy.setQueuePrefetch(0);
		prefetchPolicy.setQueueBrowserPrefetch(0);
		connection.setPrefetchPolicy(prefetchPolicy);
		try {
			log.debug(String.format("Connecting to JMS Provider with broker url %s", brokerUrl));
			this.connection = (ActiveMQConnection) connection.createConnection();
			this.internalSession = this.connection.createSession(true, Session.SESSION_TRANSACTED);
			this.internalTransaction = begin();
		} catch (JMSException e) {
			throw new RuntimeException(e);
		} catch (LogisticsTransactionException e) {
			throw new RuntimeException(e);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	public AbstractJMSProvider() {
		this("tcp://localhost:61616?broker.persistent=false");
	}

	protected List<Item> readQueueItems(QueueName... queues) throws JMSException {
		// disable topic consumer message listener as ActiveMQ doesn't allow
		// browsing
		// if _another_ consumer (in the same session?) is listening
		// topicConsumer.setMessageListener(null);
		List<Item> items = new ArrayList<Item>();
		for (QueueName queue : queues) {
			items.addAll(this.internalTransaction.browseQueue(queue));
		}

		// reenable message listener
		// topicConsumer.setMessageListener(this);
		return items;
	}

	public String getNewId(IdType type) throws LogisticsException, InterruptedException {
		try {
			return this.internalTransaction.getNewId(type);
		} catch (LogisticsTransactionException | LogisticsTransactionTimeoutException e) {
			throw new LogisticsException(e);
		}
	}

	public void start() throws LogisticsException {
		try {
			if (this.queueChangeCallBacks != null && this.queueChangeCallBacks.size() > 0) {
				Topic topic = this.internalSession.createTopic(TOPIC_QUEUE_UPDATES);
				topicConsumer = this.internalSession.createConsumer(topic);
				topicConsumer.setMessageListener(this);
			}

			// start message delivery
			this.connection.start();
		} catch (JMSException e) {
			throw new LogisticsException(e);
		}
	}

	public void stop() throws LogisticsException {
		try {
			if (this.connection.isStarted()) {
				this.connection.stop();
			}

			this.queueChangeCallBacks.clear();

			if (!this.connection.isClosing() && !this.connection.isClosed()) {
				this.connection.close();
			}
		} catch (JMSException e) {
			throw new LogisticsException(e);
		}
	}

	@Override
	public void onMessage(Message message) {
		try {
			if (message == null) {
				log.debug("Received null message");
				return;
			}
			
			QueueName queueName = getQueueName(message.getStringProperty(TOPIC_QUEUE_UPDATES_PROP_QUEUE_NAME));
			QueueOperation operation = QueueOperation.valueOf(message.getStringProperty(TOPIC_QUEUE_UPDATES_PROP_QUEUE_OP));
			Object object = ((ObjectMessage) message).getObject();

			// only item messages are processed
			if (!(object instanceof Item)) {
				return;
			}

			ItemUpdateCallback callback = this.queueChangeCallBacks.get(queueName);

			List<Item> items = new ArrayList<Item>(1);
			items.add((Item) object);
			
			if (callback == null) {
				return;
			}

			switch (operation) {
			case PUT:
				callback.notifyPut(items);
				break;
			case TAKE:
				callback.notifyTake(items);
				break;
			default:
				log.error(String.format("Unexpected queue operation type %s encountered", operation.toString()));
				break;

			}
		} catch (JMSException e) {
			log.error("Failed to process topic message", e);
		}
	}

	protected String getBrokerUrl() {
		return brokerUrl;
	}

	protected AbstractJMSTransactionProvider getInternalTransaction() {
		return internalTransaction;
	}
	
	public boolean isTransactionReusable() {
		return true;
	}	
}
