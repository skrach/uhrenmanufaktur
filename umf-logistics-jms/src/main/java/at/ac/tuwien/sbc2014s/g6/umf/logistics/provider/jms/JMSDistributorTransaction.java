package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.jms;

import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.Session;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.WatchType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionTimeoutException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.distributor.DistributorTransaction;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Watch;

public class JMSDistributorTransaction extends AbstractJMSTransactionProvider implements DistributorTransaction {

	JMSDistributor jmsDistributor;

	JMSDistributorTransaction(Session session, JMSDistributor distributor) throws LogisticsTransactionException,
			InterruptedException, JMSException {
		super(session);
	}
	
	@Override
	protected void initialize() throws JMSException {
		super.initialize();
		
		for (AbstractJMSProvider.QueueName queueName : JMSDistributor.DistributorQueueName.values()) {
			Queue queue = this.session.createQueue(queueName.toString());
			this.queues.put(queueName, this.session.createQueue(queueName.toString()));
			this.producers.put(queue, this.session.createProducer(queue));
		}
	}

	@Override
	public WatchType sellWatch() throws InterruptedException, LogisticsTransactionException, LogisticsTransactionTimeoutException {
		try {
			Watch watch = this.takeObject(JMSDistributor.DistributorQueueName.STOCK);
			this.putObject(JMSDistributor.DistributorQueueName.SOLD, watch);
			
			// update stock info
			return WatchType.getWatchType(watch);
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}

	}

	@Override
	public void put(Watch watch) throws LogisticsTransactionException, InterruptedException, LogisticsTransactionTimeoutException {
		try {
			this.putObject(JMSDistributor.DistributorQueueName.STOCK, watch);
		} catch (JMSException e) {
			throw new LogisticsTransactionException(e);
		}
	}
}
