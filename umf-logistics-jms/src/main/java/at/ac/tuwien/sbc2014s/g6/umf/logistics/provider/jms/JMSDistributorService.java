package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.jms;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LoadingProviderFailedException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsServiceException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.distributor.Distributor;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.distributor.DistributorService;

public class JMSDistributorService extends AbstractJMSService implements DistributorService {

	private static Logger log = LogManager.getLogger(JMSDistributorService.class);
	private Distributor distributor;

	/**
	 * Start JMS Distributor Service.
	 *
	 * @param args
	 *            Startup options. E.g. tcp://localhost:61616
	 * @throws Exception
	 *             Thrown if errors encountered during startup
	 */
	public static void main(String[] args) {
		JMSDistributorService service = new JMSDistributorService();

		// support starting this without arguments
		try {
			if (args.length < 1) {
				service.startService(null);
			} else {
					service.startService(args[0]);
			}
		} catch (LogisticsServiceException e) {
			throw new RuntimeException(e);
		}

		try {
			log.info("Press ENTER key to stop Broker");
			System.in.read();
			log.info("Stopping JMS Broker...");
			service.stopService();
		} catch (IOException | LogisticsServiceException e) {
			throw new RuntimeException(e);
		}
		System.exit(0);
	}

	@Override
	protected void afterStartup() throws LogisticsServiceException {
		super.afterStartup();

		try {
			// generate psn
			String distributorPsn = String.format("%s|%s", JMSDistributor.class.getName(), this.getBindAddress());
			this.distributor = Distributor.createDistributor(distributorPsn);
		} catch (LoadingProviderFailedException e) {
			throw new LogisticsServiceException(e);
		}
	}

	@Override
	public Distributor startService(String parameter) throws LogisticsServiceException {
		this.startJMSService(parameter);
		return this.distributor;
	}
}
