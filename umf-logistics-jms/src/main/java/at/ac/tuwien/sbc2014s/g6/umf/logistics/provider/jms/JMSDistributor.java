package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.jms;

import java.io.InterruptedIOException;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.Session;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.ItemUpdateCallback;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.distributor.Distributor;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Item;

public class JMSDistributor extends AbstractJMSProvider implements Distributor {
	private static Logger log = LogManager.getLogger(JMSDistributor.class);
	
	public static enum DistributorQueueName implements AbstractJMSProvider.QueueName {
		STOCK, SOLD
	}
	
	public JMSDistributor(String brokerUrl) {
		super(brokerUrl);
	}

	@Override
	public QueueName getQueueName(String value) {
		return DistributorQueueName.valueOf(value);
	}

	@Override
	public QueueName[] getQueueNames() {
		return DistributorQueueName.values();
	}

	@Override
	public JMSDistributorTransaction begin() throws LogisticsTransactionException, InterruptedException {
		try {
			return new JMSDistributorTransaction(this.connection.createSession(true, Session.SESSION_TRANSACTED), this);
		} catch (JMSException e) {
			if (e.getCause() instanceof InterruptedException || e.getCause() instanceof InterruptedIOException) {
				throw new InterruptedException();
			} else {
				throw new LogisticsTransactionException(e);
			}
		}
	}

	@Override
	public List<? extends Item> readDepotStock() throws LogisticsException {
		try {
			return readQueueItems(DistributorQueueName.STOCK);
		} catch (JMSException e) {
			throw new LogisticsException(e);
		}
	}

	@Override
	public List<? extends Item> readDepotSold() throws LogisticsException {
		try {
			return readQueueItems(DistributorQueueName.SOLD);
		} catch (JMSException e) {
			throw new LogisticsException(e);
		}
	}

	@Override
	public void subscribeToDepotStock(ItemUpdateCallback callback) {
		this.queueChangeCallBacks.put(DistributorQueueName.STOCK, callback);
	}

	@Override
	public void subscribeToDepotSold(ItemUpdateCallback callback) {
		this.queueChangeCallBacks.put(DistributorQueueName.SOLD, callback);
	}

	@Override
	public String getDistributorPsn() {
		return String.format("%s|%s", JMSDistributor.class.getName(), this.getBrokerUrl());
	}
	
	@Override
	public void start() throws LogisticsException {
		super.start();
		log.info("Started JMSDistributor instance " + this.getDistributorPsn());
		
	}
}
