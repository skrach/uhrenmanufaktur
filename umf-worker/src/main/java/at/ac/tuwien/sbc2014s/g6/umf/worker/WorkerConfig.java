package at.ac.tuwien.sbc2014s.g6.umf.worker;

import org.apache.commons.configuration.Configuration;

import at.ac.tuwien.sbc2014s.g6.umf.util.Config;

public class WorkerConfig {
	public static final String SUPPLIER_PROCESSTIME_LOWERBOUND = "Supplier.ProcessTime.LowerBound";
	public static final String SUPPLIER_PROCESSTIME_UPPERBOUND = "Supplier.ProcessTime.UpperBound";

	public static final String ASSEMBLY_PROCESSTIME_LOWERBOUND = "Assembly.ProcessTime.LowerBound";
	public static final String ASSEMBLY_PROCESSTIME_UPPERBOUND = "Assembly.ProcessTime.UpperBound";

	public static final String QA_PROCESSTIME_LOWERBOUND = "Qa.ProcessTime.LowerBound";
	public static final String QA_PROCESSTIME_UPPERBOUND = "Qa.ProcessTime.UpperBound";
	public static final String QA_QUALITYRATING_LOWERBOUND = "Qa.QualityRating.LowerBound";
	public static final String QA_QUALITYRATING_UPPERBOUND = "Qa.QualityRating.UpperBound";

	public static final String FACTORY_PROCESSTIME_DELIVER_LOWERBOUND = "Factory.ProcessTime.Deliver.LowerBound";
	public static final String FACTORY_PROCESSTIME_DELIVER_UPPERBOUND = "Factory.ProcessTime.Deliver.UpperBound";
	public static final String FACTORY_PROCESSTIME_RECYCLE_LOWERBOUND = "Factory.ProcessTime.Recycle.LowerBound";
	public static final String FACTORY_PROCESSTIME_RECYCLE_UPPERBOUND = "Factory.ProcessTime.Recycle.UpperBound";
	public static final String FACTORY_WATCH_TIMEOUT = "Factory.Watch.Timeout";
	public static final String QUALITYRATINGCLASS_A_LOWERBOUND = "QualityRatingClass.A.LowerBound";
	public static final String QUALITYRATINGCLASS_A_UPPERBOUND = "QualityRatingClass.A.UpperBound";
	public static final String QUALITYRATINGCLASS_B_LOWERBOUND = "QualityRatingClass.B.LowerBound";
	public static final String QUALITYRATINGCLASS_B_UPPERBOUND = "QualityRatingClass.B.UpperBound";
	public static final String QUALITYRATINGCLASS_RECYCLE_LOWERBOUND = "QualityRatingClass.Recycle.LowerBound";
	public static final String QUALITYRATINGCLASS_RECYCLE_UPPERBOUND = "QualityRatingClass.Recycle.UpperBound";

	public static final String DISTRIBUTOR_PROCESSTIME_SELL_LOWERBOUND = "Distributor.Processtime.Sell.Lowerbound";
	public static final String DISTRIBUTOR_PROCESSTIME_SELL_UPPERBOUND = "Distributor.Processtime.Sell.Upperbound";

	public static final String WORKER_BENCHMARKMODE = "Worker.BenchmarkMode";
	public static final String SUPPLIER_BULKINSERT = "Supplier.BulkInsert";

	public static Configuration getWorkerConfiguration() {
		return Config.getConfig("worker.properties");
	}
}
