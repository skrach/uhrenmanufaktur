package at.ac.tuwien.sbc2014s.g6.umf.worker.logistics;

import org.apache.commons.configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionTimeoutException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.Factory;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.FactoryTransaction;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.IdType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Component;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Order;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.OrderWorkItem;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.RecycleableComponent;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Watch;
import at.ac.tuwien.sbc2014s.g6.umf.util.RandomGenerator;
import at.ac.tuwien.sbc2014s.g6.umf.worker.AbstractWorker;
import at.ac.tuwien.sbc2014s.g6.umf.worker.WorkerConfig;

public class LogisticsWorker extends AbstractWorker {

	public enum QaType {
		A, B
	}

	private static enum CurrentQuality {
		PREFERED, RECYCLE
	};

	private static final Logger log = LogManager.getLogger(LogisticsWorker.class);

	private final QaType qaType;

	public LogisticsWorker(String psn, QaType qaType) {
		super(log, psn);
		this.qaType = qaType;
	}

	public static void main(String[] args) {
		if (args.length != 2) {
			System.err.println("Arguments: PSN QaType");
			return;
		}

		LogisticsWorker worker = new LogisticsWorker(args[0], QaType.valueOf(args[1].toUpperCase()));

		try {
			worker.run();
		} catch (RuntimeException e) {
			log.fatal("An error occured.", e);
			System.exit(1);
		}

		System.exit(0);
	}

	@Override
	protected void process() {
		boolean interrupted = false;
		try {
			Factory factory = this.getFactory();
			FactoryTransaction transaction = null;
			try {
				factory.start();
				this.setWorkerId(factory.getNewId(IdType.WORKER_LOGISTICS));
			} catch (LogisticsException e) {
				this.infoFailure("Error getting worker id. Shutting down.", e);
				this.stop(null, e);
				return;
			}

			if (WorkerConfig.getWorkerConfiguration().getInt(WorkerConfig.WORKER_BENCHMARKMODE) > 0) {
				this.infoMessage("Waiting for benchmark start signal ...");
				factory.waitForBenchmarkSignal();
			}

			Configuration config = this.getConfig();
			int minD = config.getInt(WorkerConfig.FACTORY_PROCESSTIME_DELIVER_LOWERBOUND) * 1000;
			int maxD = config.getInt(WorkerConfig.FACTORY_PROCESSTIME_DELIVER_UPPERBOUND) * 1000;
			int minR = config.getInt(WorkerConfig.FACTORY_PROCESSTIME_RECYCLE_LOWERBOUND) * 1000;
			int maxR = config.getInt(WorkerConfig.FACTORY_PROCESSTIME_RECYCLE_UPPERBOUND) * 1000;
			int prefMinQuality;
			int prefMaxQuality;
			switch (this.qaType) {
			case A:
				prefMinQuality = config.getInt(WorkerConfig.QUALITYRATINGCLASS_A_LOWERBOUND);
				prefMaxQuality = config.getInt(WorkerConfig.QUALITYRATINGCLASS_A_UPPERBOUND);
				break;
			case B:
				prefMinQuality = config.getInt(WorkerConfig.QUALITYRATINGCLASS_B_LOWERBOUND);
				prefMaxQuality = config.getInt(WorkerConfig.QUALITYRATINGCLASS_B_UPPERBOUND);
				break;
			default:
				throw new IllegalArgumentException("Invalid quality class provided.");
			}

			int recycleMinQuality = config.getInt(WorkerConfig.QUALITYRATINGCLASS_RECYCLE_LOWERBOUND);
			int recycleMaxQuality = config.getInt(WorkerConfig.QUALITYRATINGCLASS_RECYCLE_UPPERBOUND);
			int timeout = config.getInt(WorkerConfig.FACTORY_WATCH_TIMEOUT);

			CurrentQuality currentQuality = CurrentQuality.PREFERED;

			while (!(interrupted = Thread.interrupted())) {
				this.infoProgress(0, 2);

				try {
					if (transaction == null || !factory.isTransactionReusable()) {
						transaction = factory.begin();
					}
				} catch (LogisticsTransactionException e) {
					this.infoFailure("An error occured while beginning transaction. Shutting down.", e);
					this.stop(transaction, e);
					return;
				}

				try {
					try {
						// take a watch
						Watch watch = null;

						int maxSteps = 2;
						while (watch == null) {
							switch (currentQuality) {
							case PREFERED:
								this.infoMessage(String.format("Waiting for class %s watch ...", this.qaType.name()));
								watch = transaction.takeWatch(prefMinQuality, prefMaxQuality, timeout);
								break;
							case RECYCLE:
								this.infoMessage("Waiting for watch to recycle ...");
								watch = transaction.takeWatch(recycleMinQuality, recycleMaxQuality, timeout);
								if (watch != null) {
									maxSteps += watch.getComponents().size() - 1;
								}
								break;
							default:
								throw new IllegalStateException();
							}

							if (watch == null) {
								if (currentQuality == CurrentQuality.PREFERED) {
									currentQuality = CurrentQuality.RECYCLE;
								} else {
									currentQuality = CurrentQuality.PREFERED;
								}
							}
						}

						watch.setModificationDate();
						watch.setLogisticsWorkerId(this.getWorkerId());
						this.infoProgress(1, maxSteps);

						// delivering or recycling watch after a random delay
						int randomTime;
						OrderWorkItem orderWorkItem = watch.getOrderWorkItem();
						switch (currentQuality) {
						case PREFERED:
							randomTime = RandomGenerator.rand(minD, maxD);
							this.infoMessage(String.format("Delivering %s for %.3f seconds ...", watch.getId(),
									(float) randomTime / 1000));
							Thread.sleep(randomTime);

							if (orderWorkItem == null) {
								transaction.dispatch(watch);
								this.infoMessage(String.format("Watch %s was put into delivery.", watch.getId()));
							} else {
								Order order = transaction.takeOrder(orderWorkItem.getOrderId());
								order.addWatch(watch);
								order.setModificationDate();
								transaction.put(order);
								this.infoMessage(String.format("Watch %s was put into order %s.", watch.getId(),
										orderWorkItem.getOrderId()));
							}

							break;
						case RECYCLE:
							randomTime = RandomGenerator.rand(minR, maxR);
							this.infoMessage(String.format("Recycling %s for %.3f seconds ...", watch.getId(),
									(float) randomTime / 1000));
							Thread.sleep(randomTime);

							if (orderWorkItem != null) {
								// requeue order workitem
								transaction.put(orderWorkItem);
								this.infoMessage(String.format("Requeued order work item for order %s and watch type %s.",
										orderWorkItem.getOrderId(), orderWorkItem.getWatchtype().name()));
							}

							transaction.recycle(watch);

							int counter = 1;
							for (Component c : watch.getComponents()) {
								if (c instanceof RecycleableComponent) {
									RecycleableComponent rc = (RecycleableComponent) c;
									this.infoMessage(String.format("Extracting %s ...", rc.getId()));
									rc.setRecycleCount(rc.getRecycleCount() + 1);
									transaction.put(rc);
									this.infoMessage(String.format("Recycled %s.", rc.getId()));
									this.infoProgress(++counter, maxSteps);
								}
							}
							break;
						default:
							throw new IllegalStateException();
						}

						transaction.commit();
						if (transaction == null || !factory.isTransactionReusable()) {
							transaction = null;
						}
						this.infoMessage(String.format("Put %s.", watch.getId()));
						this.infoProgress(maxSteps, maxSteps);
					} catch (LogisticsTransactionException e) {
						this.infoFailure("An error occured while delivering or recycling a watch.", e);
						this.stop(transaction, e);
						return;
					}
				} catch (LogisticsTransactionTimeoutException e) {
					this.infoMessage("Transaction timed out. Trying again ...");
					transaction = null;
					continue;
				}
			}
		} catch (InterruptedException e) {
			interrupted = true;
		}

		if (interrupted) {
			this.infoSuccess("Worker was interrupted. Shutting down.");
		} else {
			this.infoSuccess("Worker has finished. Shutting down.");
		}
		this.stop();
	}
}
