package at.ac.tuwien.sbc2014s.g6.umf.worker.accountmanager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.WatchType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LoadingProviderFailedException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionTimeoutException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.distributor.Distributor;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.distributor.DistributorTransaction;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.Factory;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.FactoryTransaction;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.IdType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.DistributorRegistration;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Item;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Watch;
import at.ac.tuwien.sbc2014s.g6.umf.worker.AbstractWorker;
import at.ac.tuwien.sbc2014s.g6.umf.worker.WorkerConfig;

public class AccountManagerWorker extends AbstractWorker {

	private static final Logger log = LogManager.getLogger(AccountManagerWorker.class);

	public AccountManagerWorker(String psn) {
		super(log, psn);
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			log.error("Arguments: PSN");
			return;
		}

		AccountManagerWorker worker = new AccountManagerWorker(args[0]);

		try {
			worker.run();
		} catch (RuntimeException e) {
			log.fatal("An error occured.", e);
			System.exit(1);
		}

		System.exit(0);
	}

	@Override
	protected void process() {
		boolean interrupted = false;
		try {
			Factory factory = this.getFactory();
			FactoryTransaction factoryTransaction = null;
			try {
				factory.start();
				this.setWorkerId(factory.getNewId(IdType.WORKER_ACCOUNTMANAGER));
			} catch (LogisticsException e) {
				this.infoFailure("Error getting worker id. Shutting down.", e);
				this.stop(null, e);
				return;
			}

			if (WorkerConfig.getWorkerConfiguration().getInt(WorkerConfig.WORKER_BENCHMARKMODE) > 0) {
				this.infoMessage("Waiting for benchmark start signal ...");
				factory.waitForBenchmarkSignal();
			}

			DistributorRegistration dReg = null;
			DistributorRegistration dRegItem = null;
			DistributorTransaction distributorTransaction = null;
			while (!(interrupted = Thread.interrupted())) {
				this.infoProgress(0, 6);

				try {
					if (factoryTransaction == null || !factory.isTransactionReusable()) {
						factoryTransaction = factory.begin();
					}
				} catch (LogisticsTransactionException e) {
					this.infoFailure("An error occured while beginning transaction. Shutting down.", e);
					this.stop(factoryTransaction, e);
					return;
				}

				try {
					try {
						this.infoMessage("Waiting for next work item ...");
						Item workerItem = factoryTransaction.takeAccountManagerWorkerItem();
						if (workerItem instanceof Watch) {
							Watch watch = (Watch) workerItem;
							this.infoMessage(String.format("Processing worker item watch %s", watch.getId()));
							WatchType watchType = WatchType.getWatchType(watch);
							// check if the watch can be delivered to a
							// distributor
							dReg = factoryTransaction.takeNextDistributorRegistration(watchType);

							if (dReg == null) {
								this.infoMessage(String.format("Found no distributor needing %s", watchType.name()));
								// No distributor is interested in this watch
								// we can just skip processing as we didn't take
								// any objects (beside the worker item)
								factoryTransaction.commit();
								continue;
							}

							// get watch
							watch = factoryTransaction.takeDeliveryWatch(watch.getId());
							
							if(watch == null) {
								this.infoMessage(String.format("Watch not available. Skipping."));
								continue;
							}

							this.infoMessage(String.format("Connecting to distributor %s...", dReg.getDistributorPsn()));
							Distributor distributor = Distributor.createDistributor(dReg.getDistributorPsn());
							distributor.start();

							// put watch to distributor
							this.infoMessage(String.format("Putting %s into distributor stock depot ...", watch.getId()));
							distributorTransaction = distributor.begin();
							distributorTransaction.put(watch);
							distributorTransaction.commit();
							distributor.stop();
							this.infoMessage(String.format("Put %s into distributor stock depot.", watch.getId()));

							this.infoMessage("Updating distributor registration in factory ...");
							// put updated distributor registration
							dReg.setStockCount(watchType, dReg.getStockInfo(watchType).getStockCount() + 1);
							factoryTransaction.put(dReg);
							factoryTransaction.commit();
						} else if (workerItem instanceof DistributorRegistration) {
							dRegItem = (DistributorRegistration) workerItem;

							this.infoMessage(String.format("Checking if distributor %s needs watches",
									dRegItem.getDistributorPsn()));

							this.infoMessage(String.format("Connecting to distributor %s...", dRegItem.getDistributorPsn()));
							Distributor distributor = Distributor.createDistributor(dRegItem.getDistributorPsn());
							dReg = factoryTransaction.takeDistributorRegistration(dRegItem.getDistributorPsn());
							distributor.start();
							distributorTransaction = distributor.begin();

							for (WatchType watchType : WatchType.values()) {
								// check if the distributor needs watches of the
								// given type
								while (!dReg.getStockInfo(watchType).isStockFull()) {
									this.infoMessage(String.format("Check if watches of type %s can be provided", watchType));
									// check if we have watches for the
									// distributor
									Watch watch = factoryTransaction.takeNextDeliveryWatch(watchType);
									if (watch == null) {
										this.infoMessage(String.format("No watches of type %s available.", watchType.name()));
										break;
									}

									// put watch to distributor
									this.infoMessage(String.format("Putting %s into distributor stock depot ...",
											watch.getId()));
									watch.setDistributorId(dReg.getWorkerId());
									distributorTransaction.put(watch);
									factoryTransaction.put(watch);
									this.infoMessage(String.format("Put %s into distributor stock depot.", watch.getId()));

									// put updated distributor registration
									dReg.setStockCount(watchType, dReg.getStockInfo(watchType).getStockCount() + 1);
								}
							}

							distributorTransaction.commit();
							distributor.stop();

							// put updated distributor registration
							factoryTransaction.put(dReg);
							factoryTransaction.commit();
						} else {
							throw new IllegalStateException("Unknown worker item type encountered");
						}

					} catch (LogisticsTransactionException | LogisticsException e) {
						this.infoFailure("An error occured while collecting components. Shutting down.", e);
						this.stop(factoryTransaction, e);
						return;
					} catch (LoadingProviderFailedException e) {
						this.infoFailure(String.format(
								"An error occured while connecting to the distributor (%s). Shutting down.",
								dReg.getDistributorPsn()), e);
						this.stop(factoryTransaction, e);
						return;
					}
				} catch (LogisticsTransactionTimeoutException e) {
					this.infoMessage("Transaction timed out. Trying again ...");
					factoryTransaction = null;
					continue;
				}
			}
		} catch (InterruptedException e) {
			interrupted = true;
		}

		if (interrupted) {
			this.infoSuccess("Worker was interrupted. Shutting down.");
		} else {
			this.infoSuccess("Worker has finished. Shutting down.");
		}
		this.stop();
	}

}
