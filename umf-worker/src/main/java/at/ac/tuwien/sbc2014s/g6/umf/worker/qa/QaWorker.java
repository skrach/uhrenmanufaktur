package at.ac.tuwien.sbc2014s.g6.umf.worker.qa;

import org.apache.commons.configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionTimeoutException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.Factory;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.FactoryTransaction;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.IdType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Watch;
import at.ac.tuwien.sbc2014s.g6.umf.util.RandomGenerator;
import at.ac.tuwien.sbc2014s.g6.umf.worker.AbstractWorker;
import at.ac.tuwien.sbc2014s.g6.umf.worker.WorkerConfig;

public class QaWorker extends AbstractWorker {

	private static final Logger log = LogManager.getLogger(QaWorker.class);

	public QaWorker(String psn) {
		super(log, psn);
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			log.error("Arguments: PSN");
			return;
		}

		QaWorker worker = new QaWorker(args[0]);

		try {
			worker.run();
		} catch (RuntimeException e) {
			log.fatal("An error occured.", e);
			System.exit(1);
		}

		System.exit(0);
	}

	@Override
	protected void process() {
		boolean interrupted = false;
		try {
			Factory factory = this.getFactory();
			try {
				factory.start();
				this.setWorkerId(factory.getNewId(IdType.WORKER_QA));
			} catch (LogisticsException e) {
				this.infoFailure("Error getting worker id. Shutting down.", e);
				this.stop(null, e);
				return;
			}

			if (WorkerConfig.getWorkerConfiguration().getInt(WorkerConfig.WORKER_BENCHMARKMODE) > 0) {
				this.infoMessage("Waiting for benchmark start signal ...");
				factory.waitForBenchmarkSignal();
			}

			Configuration config = this.getConfig();
			int min = config.getInt(WorkerConfig.QA_PROCESSTIME_LOWERBOUND) * 1000;
			int max = config.getInt(WorkerConfig.QA_PROCESSTIME_UPPERBOUND) * 1000;
			int minQuality = config.getInt(WorkerConfig.QA_QUALITYRATING_LOWERBOUND);
			int maxQuality = config.getInt(WorkerConfig.QA_QUALITYRATING_UPPERBOUND);

			while (!(interrupted = Thread.interrupted())) {
				this.infoProgress(0, 2);

				FactoryTransaction transaction = null;
				try {
					transaction = factory.begin();
				} catch (LogisticsTransactionException e) {
					this.infoFailure("An error occured while beginning transaction. Shutting down.", e);
					this.stop(transaction, e);
					return;
				}

				try {
					// take a watch
					this.infoMessage("Waiting for assembled watch ...");
					Watch watch = transaction.takeWatch();
					watch.setModificationDate();
					watch.setQaWorkerId(this.getWorkerId());
					watch.setQualityRating(RandomGenerator.rand(minQuality, maxQuality));
					this.infoProgress(1, 2);

					int randomTime = RandomGenerator.rand(min, max);
					this.infoMessage(String.format("Testing %s for %.3f seconds ...", watch.getId(),
							(float) randomTime / 1000));
					Thread.sleep(randomTime);

					// put tested watch
					this.infoMessage(String.format("Putting %s ...", watch.getId()));
					transaction.put(watch);
					transaction.commit();
					if (transaction == null || !factory.isTransactionReusable()) {
						transaction = null;
					}
					this.infoMessage(String.format("Put %s.", watch.getId()));
					this.infoProgress(2, 2);

				} catch (LogisticsTransactionException e) {
					this.infoFailure("An error occured while testing a watch.", e);
					this.stop(transaction, e);
					return;
				} catch (LogisticsTransactionTimeoutException e) {
					this.infoMessage("Transaction timed out. Trying again ...");
					transaction = null;
					continue;
				}
			}
		} catch (InterruptedException e) {
			interrupted = true;
		}

		if (interrupted) {
			this.infoSuccess("Worker was interrupted. Shutting down.");
		} else {
			this.infoSuccess("Worker has finished. Shutting down.");
		}
		this.stop();
	}
}
