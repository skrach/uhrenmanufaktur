package at.ac.tuwien.sbc2014s.g6.umf.worker.supplier;

import org.apache.commons.configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionTimeoutException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.Factory;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.FactoryTransaction;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.IdType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Casing;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Clockwork;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Component;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Hand;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.LeatherWristband;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.MetalWristband;
import at.ac.tuwien.sbc2014s.g6.umf.util.RandomGenerator;
import at.ac.tuwien.sbc2014s.g6.umf.worker.AbstractWorker;
import at.ac.tuwien.sbc2014s.g6.umf.worker.WorkerConfig;

public class SupplierWorker extends AbstractWorker {

	private static final Logger log = LogManager.getLogger(SupplierWorker.class);

	private final ComponentType componentType;
	private final int amount;

	public SupplierWorker(String psn, ComponentType componentType, int amount) {
		super(log, psn);
		this.componentType = componentType;
		this.amount = amount;
	}

	public static void main(String[] args) {
		if (args.length != 3) {
			log.error("Arguments: PSN ComponentType Amount");
			return;
		}

		SupplierWorker worker = new SupplierWorker(args[0], ComponentType.valueOf(args[1].toUpperCase()),
				Integer.parseInt(args[2]));

		try {
			worker.run();
		} catch (RuntimeException e) {
			log.fatal("An error occured.", e);
			System.exit(1);
		}

		System.exit(0);
	}

	private final int BULK_INTERVAL = 50;

	@Override
	protected void process() {
		boolean interrupted = false;
		try {
			Factory factory = this.getFactory();
			FactoryTransaction transaction = null;
			try {
				factory.start();
				this.setWorkerId(factory.getNewId(IdType.WORKER_SUPPLIER));
			} catch (LogisticsException e) {
				this.infoFailure("Error getting worker id. Shutting down.", e);
				this.stop(null, e);
				return;
			}

			Configuration config = WorkerConfig.getWorkerConfiguration();
			int min = config.getInt(WorkerConfig.SUPPLIER_PROCESSTIME_LOWERBOUND) * 1000;
			int max = config.getInt(WorkerConfig.SUPPLIER_PROCESSTIME_UPPERBOUND) * 1000;
			int counter = 0;

			this.infoProgress(counter, this.amount);
			Component component = null;

			while (counter++ < this.amount && !(interrupted = Thread.interrupted())) {
				if (component == null) {
					try {
						switch (this.componentType) {
						case CASING:
							component = new Casing(factory.getNewId(IdType.ITEM_CASING), this.getWorkerId());
							break;
						case CLOCKWORK:
							component = new Clockwork(factory.getNewId(IdType.ITEM_CLOCKWORK), this.getWorkerId());
							break;
						case HAND:
							component = new Hand(factory.getNewId(IdType.ITEM_HAND), this.getWorkerId());
							break;
						case LEATHER_WRISTBAND:
							component = new LeatherWristband(factory.getNewId(IdType.ITEM_LEATHER_WRISTBAND),
									this.getWorkerId());
							break;
						case METAL_WRISTBAND:
							component = new MetalWristband(factory.getNewId(IdType.ITEM_METAL_WRISTBAND), this.getWorkerId());
							break;
						default:
							throw new IllegalStateException("Unexpected item type encountered");
						}
					} catch (LogisticsException e) {
						this.infoFailure(String.format("An error occured while getting new id of type %s. Shutting down.",
								this.componentType.name()), e);
						this.stop(null, e);
						return;
					}
				} else {
					log.info(String.format("Reusing component %s due to transaction timeout.", component.getId()));
				}

				// put component after a random delay
				int randomTime = RandomGenerator.rand(min, max);
				this.infoMessage(String.format("Supplying %s for %.3f seconds ...", component.getId(),
						(float) randomTime / 1000));
				Thread.sleep(randomTime);

				this.infoMessage(String.format("Putting %s ...", component.getId()));

				try {
					if (transaction == null
							|| (!factory.isTransactionReusable() && config.getInt(WorkerConfig.SUPPLIER_BULKINSERT) == 0)
							|| (!factory.isTransactionReusable() && config.getInt(WorkerConfig.SUPPLIER_BULKINSERT) > 0 && (counter - 1)
									% this.BULK_INTERVAL == 0)) {
						transaction = factory.begin();
						// log.warn("Beginning new transaction. " + counter);
					}
				} catch (LogisticsTransactionException e) {
					this.infoFailure("An error occured while beginning transaction. Shutting down.", e);
					this.stop(transaction, e);
					return;
				}

				// log.warn(transaction + "c" + counter);

				try {
					transaction.put(component);
					if (config.getInt("Supplier.BulkInsert") == 0
							|| (config.getInt("Supplier.BulkInsert") > 0 && (counter % this.BULK_INTERVAL == 0 || counter == this.amount))) {
						transaction.commit();
						// log.warn("Commited transaction. " + counter);
					}

					// if (!factory.isTransactionReusable()) {
					// transaction = null;
					// }
					this.infoMessage(String.format("Put %s.", component.getId()));
					this.infoProgress(counter, this.amount);
					component = null;
				} catch (LogisticsTransactionException e) {
					this.infoFailure(String.format("An error occured while putting %s. Shutting down.", component.getId()),
							e);
					this.stop(transaction, e);
					return;
				} catch (LogisticsTransactionTimeoutException e) {
					if (config.getInt("Supplier.BulkInsert") > 0) {
						String msg = String
								.format("Transaction retry not supported with Supplier.BulkInsert > 0. Increase transaction timeout or decrease BULK_INTERVAL. (counter %% %d was %d)",
										this.BULK_INTERVAL, counter);
						log.fatal(msg);
						throw new RuntimeException(msg);
					}
					this.infoMessage("Transaction timed out. Trying again ...");
					counter--;
					continue;
				}
			}
		} catch (InterruptedException e) {
			interrupted = true;
		}

		if (interrupted) {
			this.infoSuccess("Worker was interrupted. Shutting down.");
		} else {
			this.infoSuccess("Worker has finished. Shutting down.");
		}
		this.stop();
	}
}
