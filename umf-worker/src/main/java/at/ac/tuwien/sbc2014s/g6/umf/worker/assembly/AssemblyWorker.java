package at.ac.tuwien.sbc2014s.g6.umf.worker.assembly;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.WatchType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionTimeoutException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.ComponentType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.Factory;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.FactoryTransaction;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.IdType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.ClassicWatch;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Component;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.OrderWorkItem;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.SportWatch;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.SportWatchTZS;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Watch;
import at.ac.tuwien.sbc2014s.g6.umf.util.RandomGenerator;
import at.ac.tuwien.sbc2014s.g6.umf.worker.AbstractWorker;
import at.ac.tuwien.sbc2014s.g6.umf.worker.WorkerConfig;

public class AssemblyWorker extends AbstractWorker {

	private static final Logger log = LogManager.getLogger(AssemblyWorker.class);

	public AssemblyWorker(String psn) {
		super(log, psn);
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			log.error("Arguments: PSN");
			return;
		}

		AssemblyWorker worker = new AssemblyWorker(args[0]);

		try {
			worker.run();
		} catch (RuntimeException e) {
			log.fatal("An error occured.", e);
			System.exit(1);
		}

		System.exit(0);
	}

	private List<Component> takeComponents(FactoryTransaction transaction, ComponentType... componentTypes)
			throws LogisticsTransactionException, InterruptedException, LogisticsTransactionTimeoutException {
		List<Component> components = new ArrayList<Component>();
		int cnt = 0;
		for (ComponentType componentType : componentTypes) {
			if (cnt == 0) {
				this.infoMessage(String.format("Waiting for %s ...", componentType));
			}

			Component c;

			switch (componentType) {
			case CASING:
				c = transaction.takeCasing();
				break;
			case CLOCKWORK:
				c = transaction.takeClockwork();
				break;
			case HAND:
				c = transaction.takeHand();
				break;
			case LEATHERWRISTBAND:
				c = transaction.takeLeatherWristband();
				break;
			case METALWRISTBAND:
				c = transaction.takeMetalWristband();
				break;
			default:
				throw new IllegalStateException("Unknown component type encountered");
			}

			if (c == null) {
				throw new IllegalStateException("Take method returned null, but an exception should be thrown.");
			}

			components.add(c);
			if (++cnt < componentTypes.length) {
				this.infoMessage(String.format("Collected %s. Waiting for %s ...", c.getId(), componentTypes[cnt]));
			} else {
				this.infoMessage(String.format("Collected %s.", c.getId()));
			}
			this.infoProgress(2 + cnt, 3 + componentTypes.length);
		}

		return components;
	}

	@Override
	protected void process() {
		boolean interrupted = false;
		FactoryTransaction transaction = null;
		try {
			Factory factory = this.getFactory();

			try {
				factory.start();
				this.setWorkerId(factory.getNewId(IdType.WORKER_ASSEMBLY));
			} catch (LogisticsException e) {
				this.infoFailure("Error getting worker id. Shutting down.", e);
				this.stop(null, e);
				return;
			}

			if (WorkerConfig.getWorkerConfiguration().getInt(WorkerConfig.WORKER_BENCHMARKMODE) > 0) {
				this.infoMessage("Waiting for benchmark start signal ...");
				factory.waitForBenchmarkSignal();
			}

			Configuration config = WorkerConfig.getWorkerConfiguration();
			int min = config.getInt(WorkerConfig.ASSEMBLY_PROCESSTIME_LOWERBOUND) * 1000;
			int max = config.getInt(WorkerConfig.ASSEMBLY_PROCESSTIME_UPPERBOUND) * 1000;

			boolean prevOrder = false;
			OrderWorkItem orderWorkItem = null;
			Watch watch = null;
			Set<WatchType> candidates = new HashSet<>();
			while (!(interrupted = Thread.interrupted())) {
				this.infoProgress(0, 9);

				// start or reuse transaction
				try {
					if (transaction == null || !factory.isTransactionReusable()) {
						transaction = factory.begin();
					}
				} catch (LogisticsTransactionException e) {
					this.infoFailure("An error occured while beginning transaction. Shutting down.", e);
					this.stop(transaction, e);
					return;
				}

				// collect required components
				List<Component> components = new ArrayList<Component>();
				try {
					try {
						this.infoMessage("Checking available components ...");
						// first check what watches can be produced (heuristic)
						candidates.clear();

						this.infoMessage("Checking for available leather wristband ...");
						if (transaction.isComponentAvailable(ComponentType.LEATHERWRISTBAND, 1)) {
							candidates.add(WatchType.CLASSIC_WATCH);
						}

						this.infoMessage("Checking for available metal wristband ...");
						if (transaction.isComponentAvailable(ComponentType.METALWRISTBAND, 1)) {
							candidates.add(WatchType.SPORT_WATCH);
						}

						this.infoMessage("Checking for available hands ...");
						if (transaction.isComponentAvailable(ComponentType.HAND, 3)) {
							candidates.add(WatchType.SPORT_WATCH_TZS);
						}

						if (candidates.isEmpty()) {
							// The worker can't produce anything -> A random
							// watch type is added
							this.infoMessage("Adding random watch as only candidate.");
							candidates.add(WatchType.values()[RandomGenerator.rand(0, WatchType.values().length - 1)]);
						}

						this.infoProgress(1, 9);
						this.infoMessage(String.format("The following watches can be produced: %s", candidates));

						this.infoMessage("Checking whether an order can be fullfilled ...");
						orderWorkItem = transaction.takeOrderWorkItem(candidates, prevOrder);
						WatchType watchType;
						if (orderWorkItem == null) {
							// choose random watch type from candidates
							WatchType[] candidateArray = candidates.toArray(new WatchType[0]);
							watchType = candidateArray[RandomGenerator.rand(0, candidateArray.length - 1)];
							this.infoMessage(String.format("Chose random candidate %s as no order can be fullfilled.",
									watchType.name()));
						} else {
							watchType = orderWorkItem.getWatchtype();
							this.infoMessage(String.format("Watch of type %s will be produced for order %s.",
									orderWorkItem.getWatchtype().name(), orderWorkItem.getOrderId()));
						}

						this.infoProgress(2, 9);
						switch (watchType) {
						case CLASSIC_WATCH:
							components = this.takeComponents(transaction, ComponentType.LEATHERWRISTBAND,
									ComponentType.HAND, ComponentType.HAND, ComponentType.CLOCKWORK, ComponentType.CASING);
							watch = new ClassicWatch(transaction.getNewId(IdType.ITEM_WATCH), this.getWorkerId(), components);
							break;
						case SPORT_WATCH:
							components = this.takeComponents(transaction, ComponentType.METALWRISTBAND, ComponentType.HAND,
									ComponentType.HAND, ComponentType.CLOCKWORK, ComponentType.CASING);
							watch = new SportWatch(transaction.getNewId(IdType.ITEM_WATCH), this.getWorkerId(), components);
							break;
						case SPORT_WATCH_TZS:
							components = this.takeComponents(transaction, ComponentType.METALWRISTBAND, ComponentType.HAND,
									ComponentType.HAND, ComponentType.HAND, ComponentType.CLOCKWORK, ComponentType.CASING);
							watch = new SportWatchTZS(transaction.getNewId(IdType.ITEM_WATCH), this.getWorkerId(),
									components);
							break;
						default:
							throw new IllegalStateException("Unknown watch type encountered");
						}
					} catch (LogisticsTransactionException e) {
						this.infoFailure("An error occured while collecting components. Shutting down.", e);
						this.stop(transaction, e);
						return;
					}

					this.infoProgress(8, 9);
					int randomTime = RandomGenerator.rand(min, max);
					this.infoMessage(String.format("Creating %s for %.3f seconds ...", watch.getId(),
							(float) randomTime / 1000));
					Thread.sleep(randomTime);

					try {
						// handle order if appropriate
						if (orderWorkItem != null) {
							watch.setOrderWorkItem(orderWorkItem);
//							Order order = transaction.takeOrder(orderWorkItem.getOrderId());
//							order.addWatch(watch); --> handled by LogisticsWorker, when watch is not recycled
//							transaction.put(order);
						}

						// put new watch
						this.infoMessage(String.format("Putting %s ...", watch.getId()));
						transaction.put(watch);
						transaction.commit();
						if (transaction == null || !factory.isTransactionReusable()) {
							transaction = null;
						}
						this.infoMessage(String.format("Put %s.", watch.getId()));
						this.infoProgress(9, 9);

						// toggle prevOrder
						prevOrder = !prevOrder;
					} catch (LogisticsTransactionException e) {
						this.infoFailure(String.format("An error occured while putting %s. Shutting down.", watch.getId()),
								e);
						this.stop(transaction, e);
						return;
					}
				} catch (LogisticsTransactionTimeoutException e) {
					this.infoMessage("Transaction timed out. Trying again ...");
					transaction = null;
					continue;
				}
			}
		} catch (InterruptedException e) {
			interrupted = true;
		}

		if (interrupted) {
			this.infoSuccess("Worker was interrupted. Shutting down.");
		} else {
			this.infoSuccess("Worker has finished. Shutting down.");
		}
		this.stop();
	}
}
