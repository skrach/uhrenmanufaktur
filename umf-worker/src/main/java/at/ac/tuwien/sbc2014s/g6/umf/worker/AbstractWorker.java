package at.ac.tuwien.sbc2014s.g6.umf.worker;

import org.apache.commons.configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LoadingProviderFailedException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.Factory;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.FactoryTransaction;

public abstract class AbstractWorker implements Runnable {

	private Factory factory;

	protected Factory getFactory() {
		return this.factory;
	}

	private Configuration config;

	protected Configuration getConfig() {
		return this.config;
	}

	private final Logger log;
	private final String psn;

	public AbstractWorker(Logger log, String psn) {
		this.log = log;
		this.psn = psn;
	}

	private void initialize() {
		try {
			this.log.info(String.format("Getting factory instance with arguments \"%s\".", this.psn));
			this.factory = Factory.createFactory(this.psn);
		} catch (LoadingProviderFailedException e) {
			throw new RuntimeException(e);
		}

		this.config = WorkerConfig.getWorkerConfiguration();
	}

	/**
	 * Override and do actual work here. Call <tt>updateProgress</tt> and/or
	 * <tt>updateMessage</tt> to indicate progress.
	 */
	protected abstract void process();

	/**
	 * Optionally override to track progress.
	 *
	 * @param progress
	 *            Current amount of processed work units
	 * @param all
	 *            Amount of work units to process or null if worker is intended
	 *            to work indefinitely
	 */
	protected void infoProgress(int progress, Integer all) {
		this.log.info(String.format("%s Progress: %d of %d.",
				(this.getWorkerId() == null ? "Worker (has no id yet)" : this.getWorkerId()), progress, all));
	}

	/**
	 * Optionally override to track progress.
	 */
	protected void infoMessage(String msg) {
		this.log.info(String.format("%s Message: %s",
				(this.getWorkerId() == null ? "Worker (has no id yet)" : this.getWorkerId()), msg));
	}

	/**
	 * Optionally override to track progress.
	 */
	protected void infoSuccess(String msg) {
		this.log.info(String.format("%s Message: %s",
				(this.getWorkerId() == null ? "Worker (has no id yet)" : this.getWorkerId()), msg));
	}

	/**
	 * Optionally override to track progress.
	 */
	protected void infoFailure(String msg, Throwable e) {
		this.log.fatal(String.format("%s Message: %s",
				(this.getWorkerId() == null ? "Worker (has no id yet)" : this.getWorkerId()), msg), e);
	}

	private String workerId;

	public String getWorkerId() {
		return this.workerId;
	}

	public void setWorkerId(String workerId) {
		this.workerId = workerId;
		this.infoMessage(String.format("Obtained worker id \"%s\".", this.workerId));
	}

	@Override
	public final void run() {
		this.initialize();
		this.process();
	}

	public void stop() {
		this.stop(null, null);
	}

	public void stop(FactoryTransaction transaction, Throwable th) {
		this.log.info("Stopping worker ...");
		if (transaction != null) {
			try {
				this.log.info("Rolling back transaction ...");
				transaction.rollback();
				this.log.info("Rolled back transaction.");
			} catch (LogisticsTransactionException e) {
				this.log.fatal("Failed rolling back transaction.", e);
			}
		}

		if (this.factory != null) {
			try {
				this.log.info("Stopping factory interface ...");
				this.factory.stop();
				this.log.info("Stopped factory interface.");
			} catch (LogisticsException e) {
				this.log.fatal("Failed stopping factory interface.", e);
			}
		}

		if (th != null) {
			throw new RuntimeException("Exception ocurred in worker.", th);
		}
	}

	public static void main(String[] args) {
		final Logger log = LogManager.getLogger(AbstractWorker.class);

		if (args.length != 1) {
			log.error("Arguments: PSN");
			return;
		}

		try {
			Factory factory = Factory.createFactory(args[0]);
			factory.start();
			factory.signalBenchmarkStart();
			log.info("Benchmark start signalled.");
			factory.stop();
		} catch (LoadingProviderFailedException | LogisticsException e) {
			e.printStackTrace();
			System.exit(1);
		}

		System.exit(0);
	}
}
