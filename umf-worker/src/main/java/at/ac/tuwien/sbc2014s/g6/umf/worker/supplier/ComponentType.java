package at.ac.tuwien.sbc2014s.g6.umf.worker.supplier;

/**
 * Type identifier used for supply item type
 */
public enum ComponentType {
	CASING, CLOCKWORK, HAND, LEATHER_WRISTBAND, METAL_WRISTBAND
}
