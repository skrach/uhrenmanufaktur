package at.ac.tuwien.sbc2014s.g6.umf.worker.distributor;

import org.apache.commons.configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.WatchType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LoadingProviderFailedException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsServiceException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionTimeoutException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.distributor.Distributor;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.distributor.DistributorService;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.distributor.DistributorTransaction;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.Factory;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.FactoryTransaction;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.IdType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.DistributorRegistration;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.DistributorRegistration.StockInfo;
import at.ac.tuwien.sbc2014s.g6.umf.util.RandomGenerator;
import at.ac.tuwien.sbc2014s.g6.umf.worker.AbstractWorker;
import at.ac.tuwien.sbc2014s.g6.umf.worker.WorkerConfig;

public class DistributorWorker extends AbstractWorker {

	private static final Logger log = LogManager.getLogger(DistributorWorker.class);
	private String distributorPsn;
	private final String distributorClass;
	private Distributor distributor;
	private DistributorService distributorService;
	private final int stockClassic;
	private final int stockSports;
	private final int stockSportsTZ;

	public DistributorWorker(String factoryPsn, String distributorServiceClass, int stockClassic, int stockSports,
			int stockSportsTZ) {
		super(log, factoryPsn);
		this.distributorClass = distributorServiceClass;
		this.stockClassic = stockClassic;
		this.stockSports = stockSports;
		this.stockSportsTZ = stockSportsTZ;
	}

	@Override
	protected void process() {
		// connect to factory
		boolean interrupted = false;
		Factory factory = this.getFactory();
		FactoryTransaction factoryTransaction = null;
		DistributorTransaction distributorTransaction = null;
		int randomTime;

		Configuration config = this.getConfig();
		int minD = config.getInt(WorkerConfig.DISTRIBUTOR_PROCESSTIME_SELL_LOWERBOUND) * 1000;
		int maxD = config.getInt(WorkerConfig.DISTRIBUTOR_PROCESSTIME_SELL_UPPERBOUND) * 1000;

		// start distributor distributorService
		try {
			factory.start();
			this.distributorService = DistributorService.createDistributorService(this.distributorClass);
			this.distributor = this.distributorService.startService(null);
			this.distributor.start();
			this.setWorkerId(factory.getNewId(IdType.WORKER_DISTRIBUTOR));
			factoryTransaction = factory.begin();

			// register distributor and get id
			try {
				this.infoMessage(String.format("Registering distributor \"%s\"...", this.distributor.getDistributorPsn()));
				DistributorRegistration dReg = new DistributorRegistration(this.distributor.getDistributorPsn());
				dReg.setStockCapacity(WatchType.CLASSIC_WATCH, this.stockClassic);
				dReg.setStockCapacity(WatchType.SPORT_WATCH, this.stockSports);
				dReg.setStockCapacity(WatchType.SPORT_WATCH_TZS, this.stockSportsTZ);
				dReg.setWorkerId(this.getWorkerId());
				this.setDistributorPsn(this.distributor.getDistributorPsn());
				factoryTransaction.update(dReg);
				factoryTransaction.commit();
			} catch (LogisticsTransactionTimeoutException e) {
				this.infoFailure("Transaction timed out while registering distributor. Shutting down.", e);
				return;
			}

			while (!(interrupted = Thread.interrupted())) {
				try {
					this.infoProgress(1, 2);

					distributorTransaction = this.distributor.begin();
					WatchType watchType = distributorTransaction.sellWatch();

					randomTime = RandomGenerator.rand(minD, maxD);
					this.infoMessage(String.format("Selling watch in %.3f seconds ...", (float) randomTime / 1000));
					Thread.sleep(randomTime);

					// update stock info
					this.infoMessage(String.format("Getting registration from factory ..."));
					if ( !factory.isTransactionReusable() ) {
						factoryTransaction = factory.begin();
					}
					DistributorRegistration distributorRegistration = factoryTransaction
							.takeDistributorRegistration(this.distributor.getDistributorPsn());

					this.infoMessage("Updating distributor stock...");
					distributorTransaction.commit();
					this.infoProgress(2, 3);

					this.infoMessage("Updating factory registration...");
					StockInfo stockInfo = distributorRegistration.getStockInfo(watchType);
					distributorRegistration.setStockCount(watchType, stockInfo.getStockCount() - 1);
					factoryTransaction.update(distributorRegistration);
					factoryTransaction.commit();
					this.infoProgress(3, 3);
					distributorTransaction = null;
				} catch (LogisticsTransactionTimeoutException e) {
					this.infoMessage("Transaction timed out. Trying again ...");
					factoryTransaction = null;
					continue;
				}
			}
		} catch (LoadingProviderFailedException e) {
			this.infoFailure("An error occured while starting the distributor distributorService", e);
			return;
		} catch (LogisticsTransactionException | LogisticsServiceException e) {
			this.infoFailure("An error occured on the factory transaction. Shutting down.", e);
			this.stop(factoryTransaction, distributorTransaction, e);
			return;
		} catch (InterruptedException e) {
			interrupted = true;
		} catch (LogisticsException e) {
			this.infoFailure("An error occured. Shutting down.", e);
			this.stop(factoryTransaction, distributorTransaction, e);
			return;
		}

		if (interrupted) {
			this.infoSuccess("Worker was interrupted. Shutting down.");
		} else {
			this.infoSuccess("Worker has finished. Shutting down.");
		}
		this.stop(factoryTransaction, distributorTransaction, null);
	}

	private void stop(FactoryTransaction factoryTransaction, DistributorTransaction distributorTransaction,
			Throwable throwable) {
		System.out.println("dsxdysölgfjdhsökgfdhjsgöfhdoölsgfkZGHHGFKFGHKG");
		if (distributorTransaction != null) {
			try {
				log.info("Rolling back distributor transaction ...");
				distributorTransaction.rollback();
				log.info("Rolled back distributor transaction.");
			} catch (LogisticsTransactionException e) {
				log.fatal("Failed rolling back distributor transaction.", e);
			}
		}

		if (this.distributor != null) {
			try {
				if (this.distributor != null) {
					log.info("Deregistering distributor");

					if (factoryTransaction == null || !this.getFactory().isTransactionReusable()) {
						factoryTransaction = this.getFactory().begin();
					}

					factoryTransaction.takeDistributorRegistration(this.distributor.getDistributorPsn());
					factoryTransaction.commit();
				}

				log.info("Stopping distributor interface ...");
				this.distributor.stop();

				log.info("Deregistering distributor service");
			} catch (LogisticsException | LogisticsTransactionException | InterruptedException
					| LogisticsTransactionTimeoutException e) {
				log.fatal("Failed stopping or deregistering distributor interface.", e);
			}
		}

		if (this.distributorService != null) {
			try {
				log.info("Stopping distributor service...");
				this.distributorService.stopService();
				log.info("Stopped distributor service");
			} catch (LogisticsServiceException e) {
				log.fatal("Failed stopping distributor service.", e);
			}
		}

		super.stop(factoryTransaction, throwable);
	}

	public String getDistributorPsn() {
		return this.distributorPsn;
	}

	public void setDistributorPsn(String distributorPsn) {
		this.distributorPsn = distributorPsn;
	}
}
