export JAVA_HOME=/usr/lib/jvm/java-8-oracle
export _JAVA_OPTIONS="-Djava.net.preferIPv4Stack=true"

mvn clean install

sbclva-2-1	 1	 192.168.24.191
sbclva-2-2	 2	 192.168.24.192
sbclva-2-4	 4	 192.168.24.194
sbclva-2-8	 8	 192.168.24.198

Angabe:
1500 Casings
1500 Clockworks
750 Leather Wristbands
750 Metal Wristbands
3750 Hands
Abarbeitungszeit 60 Sekunden


MZS		2
---------------------------------
availableCasingCount    1298
availableClockworkCount 1257
availableHandCount      3279
availableLeatherWristbandCount  679
availableMetalWristbandCount    619
availableAssembledWatchCount    0
availableTestedWatchCount       0
deliveryWatchCount      202
deliveredWatchCount     0
deliveryClassAWatchCount        77
deliveryClassBWatchCount        125
recycledWatchCount      41
orderCount      0
distributorCount        0
percentClassAWatches    31.69 %
percentClassBWatches    51.44 %
percentRecycledWatches  16.87 %
highestComponentRecycleCount    0
earliestCreateDate      2014-05-16 08:05:29
latestModificationDate  2014-06-16 08:06:30


MZS		4
---------------------------------
availableCasingCount    1300
availableClockworkCount 1257
availableHandCount      3284
availableLeatherWristbandCount  687
availableMetalWristbandCount    613
availableAssembledWatchCount    0
availableTestedWatchCount       2
deliveryWatchCount      198
deliveryClassAWatchCount        71
deliveryClassBWatchCount        127
percentClassAWatches    29.46 %
percentClassBWatches    52.70 %
percentRecycledWatches  17.84 %
recycledWatchCount      43
highestComponentRecycleCount    0
deliveredWatchCount     0
orderCount      0
distributorCount        0
earliestCreateDate      2014-06-16T20:46:43.629Z
latestModificationDate  2014-06-16T20:47:44.225Z
deliveryWatchesPerSecond        4.017


MZS		8
---------------------------------
availableCasingCount    1267
availableClockworkCount 1214
availableHandCount      3204
availableLeatherWristbandCount  679
availableMetalWristbandCount    588
availableAssembledWatchCount    0
availableTestedWatchCount       0
deliveryWatchCount      233
deliveryClassAWatchCount        102
deliveryClassBWatchCount        131
percentClassAWatches    35.66 %
percentClassBWatches    45.80 %
percentRecycledWatches  18.53 %
recycledWatchCount      53
highestComponentRecycleCount    0
deliveredWatchCount     0
orderCount      0
distributorCount        0
earliestCreateDate      2014-06-16T20:31:53.107Z
latestModificationDate  2014-06-16T20:33:03.985Z
deliveryWatchesPerSecond        4.086


MZS		NATHAN
---------------------------------
availableCasingCount	366
availableClockworkCount	72
availableHandCount	1128
availableLeatherWristbandCount	366
availableMetalWristbandCount	0
availableAssembledWatchCount	0
availableTestedWatchCount	0
deliveryWatchCount	1134
deliveryClassAWatchCount	422
deliveryClassBWatchCount	712
percentClassAWatches	29.55 %
percentClassBWatches	49.86 %
percentRecycledWatches	20.59 %
recycledWatchCount	294
highestComponentRecycleCount	3
deliveredWatchCount	0
orderCount	0
distributorCount	0
earliestCreateDate	2014-06-16T20:32:38.590Z
latestModificationDate	2014-06-16T20:32:52.944Z
deliveryWatchesPerSecond	102.000


MZS		LUCY
---------------------------------
availableCasingCount 372
availableClockworkCount 109
availableHandCount 1099
availableLeatherWristbandCount 372
availableMetalWristbandCount 0
availableAssembledWatchCount 0
availableTestedWatchCount 0
deliveryWatchCount 1128
deliveryClassAWatchCount 420
deliveryClassBWatchCount 708
percentClassAWatches 30.19 %
percentClassBWatches 50.90 %
percentRecycledWatches 18.91 %
recycledWatchCount 263
highestComponentRecycleCount 3
deliveredWatchCount 0
orderCount 0
distributorCount 0
earliestCreateDate 2014-06-16T22:03:39.038Z
latestModificationDate 2014-06-16T22:04:27.206Z
deliveryWatchesPerSecond 28.979


MZS		KENNETH
---------------------------------
availableCasingCount	356
availableClockworkCount	122
availableHandCount	1089
availableLeatherWristbandCount	356
availableMetalWristbandCount	0
availableAssembledWatchCount	0
availableTestedWatchCount	0
deliveryWatchCount	1144
deliveryClassAWatchCount	426
deliveryClassBWatchCount	718
percentClassAWatches	30.91 %
percentClassBWatches	52.10 %
percentRecycledWatches	16.98 %
recycledWatchCount	234
highestComponentRecycleCount	3
deliveredWatchCount	0
orderCount	0
distributorCount	0
earliestCreateDate	2014-06-23T23:31:17.371Z
latestModificationDate	2014-06-23T23:31:57.270Z
deliveryWatchesPerSecond	35.333


JMS		2
---------------------------------
availableCasingCount    364
availableClockworkCount 200
availableHandCount      1088
availableLeatherWristbandCount  364
availableMetalWristbandCount    0
availableAssembledWatchCount    447
availableTestedWatchCount       69
deliveryWatchCount      620
deliveryClassAWatchCount        258
deliveryClassBWatchCount        362
percentClassAWatches    32.91 %
percentClassBWatches    46.17 %
percentRecycledWatches  20.92 %
recycledWatchCount      164
highestComponentRecycleCount    1
deliveredWatchCount     0
orderCount      0
distributorCount        0
earliestCreateDate      2014-06-16T21:05:17.403Z
latestModificationDate  2014-06-16T21:06:16.660Z
deliveryWatchesPerSecond        13.288


JMS		4
---------------------------------
availableCasingCount    345
availableClockworkCount 35
availableHandCount      1075
availableLeatherWristbandCount  345
availableMetalWristbandCount    0
availableAssembledWatchCount    0
availableTestedWatchCount       1
deliveryWatchCount      1154
deliveryClassAWatchCount        439
deliveryClassBWatchCount        715
percentClassAWatches    29.99 %
percentClassBWatches    48.84 %
percentRecycledWatches  21.17 %
recycledWatchCount      310
highestComponentRecycleCount    4
deliveredWatchCount     0
orderCount      0
distributorCount        0
earliestCreateDate      2014-06-16T20:57:48.636Z
latestModificationDate  2014-06-16T20:58:47.453Z
deliveryWatchesPerSecond        25.241


JMS		8
---------------------------------
availableCasingCount    387
availableClockworkCount 101
availableHandCount      1140
availableLeatherWristbandCount  386
availableMetalWristbandCount    1
availableAssembledWatchCount    0
availableTestedWatchCount       0
deliveryWatchCount      1113
deliveryClassAWatchCount        421
deliveryClassBWatchCount        692
percentClassAWatches    30.09 %
percentClassBWatches    49.46 %
percentRecycledWatches  20.44 %
recycledWatchCount      286
highestComponentRecycleCount    4
deliveredWatchCount     0
orderCount      0
distributorCount        0
earliestCreateDate      2014-06-16T20:54:12.366Z
latestModificationDate  2014-06-16T20:55:11.844Z
deliveryWatchesPerSecond        23.712


JMS		NATHAN
---------------------------------
availableCasingCount	300
availableClockworkCount	0
availableHandCount	919
availableLeatherWristbandCount	288
availableMetalWristbandCount	12
availableAssembledWatchCount	0
availableTestedWatchCount	0
deliveryWatchCount	1200
deliveryClassAWatchCount	445
deliveryClassBWatchCount	755
percentClassAWatches	29.67 %
percentClassBWatches	50.33 %
percentRecycledWatches	20.00 %
recycledWatchCount	300
highestComponentRecycleCount	2
deliveredWatchCount	0
orderCount	0
distributorCount	0
earliestCreateDate	2014-06-16T20:36:07.770Z
latestModificationDate	2014-06-16T20:36:35.597Z
deliveryWatchesPerSecond	55.556


JMS		LUCY
---------------------------------
availableCasingCount 332
availableClockworkCount 63
availableHandCount 988
availableLeatherWristbandCount 331
availableMetalWristbandCount 1
availableAssembledWatchCount 0
availableTestedWatchCount 0
deliveryWatchCount 1168
deliveryClassAWatchCount 433
deliveryClassBWatchCount 735
percentClassAWatches 30.13 %
percentClassBWatches 51.15 %
percentRecycledWatches 18.72 %
recycledWatchCount 269
highestComponentRecycleCount 3
deliveredWatchCount 0
orderCount 0
distributorCount 0
earliestCreateDate 2014-06-16T22:18:36.562Z
latestModificationDate 2014-06-16T22:19:42.693Z
deliveryWatchesPerSecond 21.773


JMS		KENNETH
---------------------------------
availableCasingCount	332
availableClockworkCount	24
availableHandCount	1013
availableLeatherWristbandCount	332
availableMetalWristbandCount	0
availableAssembledWatchCount	0
availableTestedWatchCount	0
deliveryWatchCount	1168
deliveryClassAWatchCount	458
deliveryClassBWatchCount	710
percentClassAWatches	31.03 %
percentClassBWatches	48.10 %
percentRecycledWatches	20.87 %
recycledWatchCount	308
highestComponentRecycleCount	5
deliveredWatchCount	0
orderCount	0
distributorCount	0
earliestCreateDate	2014-06-23T23:37:47.086Z
latestModificationDate	2014-06-23T23:38:28.881Z
deliveryWatchesPerSecond	36.000