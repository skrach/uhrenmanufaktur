package at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item;

public class MetalWristband extends RecycleableComponent {
	private static final long serialVersionUID = 1L;

	public MetalWristband(String id, String supplierId) {
		super(id, supplierId);
	}

	@Override
	public String toString() {
		return "Metal wristband [id=" + this.getId() + "]";
	}
}
