package at.ac.tuwien.sbc2014s.g6.umf.logistics.factory;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsServiceException;

public interface FactoryService {

	/**
	 * Call this method to start a service
	 *
	 * @param param
	 *            Optional. If no parameter is supplied, a port is chosen.
	 * @throws LogisticsServiceException
	 * @return A distributor instance
	 */
	public abstract void startService(String parameter) throws LogisticsServiceException;

	/**
	 * Called to stop a service
	 *
	 * @throws LogisticsServiceException
	 */
	public abstract void stopService() throws LogisticsServiceException;
}