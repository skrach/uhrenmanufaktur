package at.ac.tuwien.sbc2014s.g6.umf.logistics.common;

import java.util.List;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Item;

public interface ItemUpdateCallback {

	public void notifyPut(List<? extends Item> items);

	public void notifyTake(List<? extends Item> items);

}
