package at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item;

import java.util.List;

public class SportWatch extends Watch {
	private static final long serialVersionUID = 1L;

	public SportWatch(String id, String assemblyWorkerId, List<Component> components) {
		super(id, assemblyWorkerId, components);
	}

	@Override
	public String toString() {
		return "Sports watch [id=" + this.getId() + "]";
	}
}
