package at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item;


public abstract class Component extends Item {
	private static final long serialVersionUID = 1L;

	private final String supplierId;

	public String getSupplierId() {
		return this.supplierId;
	}

	public Component(String id, String supplierId) {
		super(id);
		this.supplierId = supplierId;
	}
}
