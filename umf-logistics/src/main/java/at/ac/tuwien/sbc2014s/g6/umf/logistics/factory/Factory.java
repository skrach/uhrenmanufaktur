package at.ac.tuwien.sbc2014s.g6.umf.logistics.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.ItemUpdateCallback;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LoadingProviderFailedException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Item;

public interface Factory {

	/**
	 * Tries to load and instatiate class specified by provider source name
	 * (PSN). PSN format: "JavaClassname[|[Argument]]" If "Argument" is present
	 * in PSN, the constructor taking one String argument is called, else the
	 * nullary constructor will be called.
	 *
	 * @param providerSourceName
	 * @return Class implementing Factory interface
	 * @throws LoadingProviderFailedException
	 */
	public static Factory createFactory(String providerSourceName) throws LoadingProviderFailedException {
		final Logger log = LogManager.getLogger(Factory.class);

		String[] psnParts = providerSourceName.split("\\|", 2);

		if (psnParts.length < 1) {
			String msg = String.format("Provider source name \"%s\" format invalid", providerSourceName);
			log.info(msg);
			throw new LoadingProviderFailedException(msg);
		}

		Class<?> c;
		try {
			c = Class.forName(psnParts[0]);
		} catch (ClassNotFoundException e) {
			log.info(String.format("Failed loading provider class \"%s\" due to %s: %s", psnParts[0],
					e.getClass().getName(), e.getMessage()));
			throw new LoadingProviderFailedException(e);
		}

		if (!Factory.class.isAssignableFrom(c)) {
			String msg = String.format(
					"Class \"%s\" cannot be used as logistics provider, because it does not implement interface \"%s\"",
					c.getName(), Factory.class.getName());
			log.info(msg);
			throw new LoadingProviderFailedException(msg);
		}

		Constructor<?> ctor;
		try {
			if (psnParts.length > 1) {
				ctor = c.getDeclaredConstructor(String.class);
				log.info(String.format(
						"Successfully loaded provider class for source name \"%s\" and will use constructor with argument.",
						providerSourceName));
				return (Factory) ctor.newInstance(psnParts[1]);
			} else {
				ctor = c.getDeclaredConstructor();
				log.info(String.format(
						"Successfully loaded provider class for source name \"%s\" and will use nullary constructor.",
						providerSourceName));
				return (Factory) ctor.newInstance();
			}
		} catch (NoSuchMethodException | SecurityException e) { // getDeclaredConstructor
			log.error(String.format("Failed loading provider class for source name \"%s\" due to %s: %s",
					providerSourceName, e.getClass().getName(), e.getMessage()));
			throw new LoadingProviderFailedException(e);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) { // newInstance
			log.error(String.format("Failed instantiating provider class for source name \"%s\" due to %s: %s",
					providerSourceName, e.getClass().getName(), e.getMessage()));
			throw new LoadingProviderFailedException(e);
		}
	}

	/**
	 * Block until benchmark start signal is issued.
	 */
	public void waitForBenchmarkSignal();

	/**
	 * Signals benchmark start.
	 */
	public void signalBenchmarkStart();

	/**
	 * Returns a new implementation of FactoryTransaction interface.
	 *
	 * @return Class implementing FactoryTransaction interface
	 * @throws LogisticsTransactionException
	 */
	public FactoryTransaction begin() throws LogisticsTransactionException, InterruptedException;

	/**
	 * Reads the current contents of the depot "Component". (non-taking)
	 *
	 * @return List of items in depot
	 * @throws LogisticsException
	 */
	public List<? extends Item> readDepotComponent() throws LogisticsException;

	/**
	 * Reads the current contents of the depot "Assembled". (non-taking)
	 *
	 * @return List of items in depot
	 * @throws LogisticsException
	 */
	public List<? extends Item> readDepotAssembled() throws LogisticsException;

	/**
	 * Reads the current contents of the depot "Tested". (non-taking)
	 *
	 * @return List of items in depot
	 * @throws LogisticsException
	 */
	public List<? extends Item> readDepotTested() throws LogisticsException;

	/**
	 * Reads the current contents of the depot "Delivery". (non-taking)
	 *
	 * @return List of items in depot
	 * @throws LogisticsException
	 */
	public List<? extends Item> readDepotDelivery() throws LogisticsException;

	/**
	 * Reads the current contents of the depot "Delivered". (non-taking)
	 *
	 * @return List of items in depot
	 * @throws LogisticsException
	 */
	public List<? extends Item> readDepotDelivered() throws LogisticsException;

	/**
	 * Reads the current contents of the depot "Recycled". (non-taking)
	 *
	 * @return List of items in depot
	 * @throws LogisticsException
	 */
	public List<? extends Item> readDepotRecycled() throws LogisticsException;

	/**
	 * Reads the current contents of the depot "Order". (non-taking)
	 *
	 * @return List of items in depot
	 * @throws LogisticsException
	 */
	public List<? extends Item> readDepotOrder() throws LogisticsException;

	/**
	 * Reads the current contents of the list of "DistributorRegistrations". (non-taking)
	 *
	 * @return List of items in distributorregistration
	 * @throws LogisticsException
	 */
	public List<? extends Item> readListDistributorRegistrations() throws LogisticsException;

	/**
	 * Subscribe to put or take events of the depot "Component".
	 *
	 * @param callback
	 *            Implementation of ItemUpdateCallback
	 */
	public void subscribeToDepotComponent(ItemUpdateCallback callback);

	/**
	 * Subscribe to put or take events of the depot "Assembled".
	 *
	 * @param callback
	 *            Implementation of ItemUpdateCallback
	 */
	public void subscribeToDepotAssembled(ItemUpdateCallback callback);

	/**
	 * Subscribe to put or take events of the depot "Tested".
	 *
	 * @param callback
	 *            Implementation of ItemUpdateCallback
	 */
	public void subscribeToDepotTested(ItemUpdateCallback callback);

	/**
	 * Subscribe to put or take events of the depot "Delivery".
	 *
	 * @param callback
	 *            Implementation of ItemUpdateCallback
	 */
	public void subscribeToDepotDelivery(ItemUpdateCallback callback);

	/**
	 * Subscribe to put or take events of the depot "Delivered".
	 *
	 * @param callback
	 *            Implementation of ItemUpdateCallback
	 */
	public void subscribeToDepotDelivered(ItemUpdateCallback callback);

	/**
	 * Subscribe to put or take events of the depot "Recycled".
	 *
	 * @param callback
	 *            Implementation of ItemUpdateCallback
	 */
	public void subscribeToDepotRecycled(ItemUpdateCallback callback);

	/**
	 * Subscribe to put or take events of the depot "Recycled".
	 *
	 * @param callback
	 *            Implementation of ItemUpdateCallback
	 */
	public void subscribeToDepotOrder(ItemUpdateCallback callback);

	/**
	 * Subscribe to put or take events of the list of "DistributorRegistrations".
	 *
	 * @param callback
	 *            Implementation of ItemUpdateCallback
	 */
	public void subscribeToListDistributorRegistrations(ItemUpdateCallback callback);

	/**
	 * Start subscriptions. This method must be called after subscribing to
	 * events and before creating a transaction (including implicit ones).
	 *
	 * @throws LogisticsException
	 */
	public void start() throws LogisticsException;

	/**
	 * Clear all subscriptions and tear down provider specific resources.
	 *
	 * @throws LogisticsException
	 */
	public void stop() throws LogisticsException;

	/**
	 * Returns a new id (unique within given type)
	 *
	 * @param type
	 *            IdType enum, specifying the scope for uniqueness
	 * @return Unique id string
	 * @throws LogisticsException
	 *             on failure
	 * @throws InterruptedException
	 *             when current thread was interrupted
	 */
	public String getNewId(IdType type) throws LogisticsException, InterruptedException;
	
	/**
	 * Specify if the transaction implementation supports recycling of transactions.
	 * @return true if recycling is supported and false otherwise
	 */
	public boolean isTransactionReusable();
}
