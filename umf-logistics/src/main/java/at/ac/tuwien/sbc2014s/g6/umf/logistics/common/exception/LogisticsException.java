package at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception;

public class LogisticsException extends Exception {

	private static final long serialVersionUID = 1L;

	public LogisticsException() {
		super();
	}

	public LogisticsException(String message) {
		super(message);
	}

	public LogisticsException(Throwable throwable) {
		super(throwable);
	}

	public LogisticsException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
