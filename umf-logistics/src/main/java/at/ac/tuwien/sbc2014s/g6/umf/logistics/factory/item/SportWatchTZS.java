package at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item;

import java.util.List;

public class SportWatchTZS extends Watch {
	private static final long serialVersionUID = 1L;

	public SportWatchTZS(String id, String assemblyWorkerId, List<Component> components) {
		super(id, assemblyWorkerId, components);
	}

	@Override
	public String toString() {
		return "SportsTZ watch [id=" + this.getId() + "]";
	}
}
