package at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item;

public class LeatherWristband extends RecycleableComponent {
	private static final long serialVersionUID = 1L;

	public LeatherWristband(String id, String supplierId) {
		super(id, supplierId);
	}

	@Override
	public String toString() {
		return "Leather wristband [id=" + this.getId() + "]";
	}
}
