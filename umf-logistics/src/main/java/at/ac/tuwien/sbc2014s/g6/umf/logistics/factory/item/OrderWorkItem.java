package at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.WatchType;

public class OrderWorkItem extends Item {
	private static final long serialVersionUID = 1L;
	
	private String orderId;
	private int priority;
	private WatchType watchtype;
	
	public OrderWorkItem(String orderId, int priority, WatchType watchtype) {
		super(String.format("%s-%d%s", orderId, priority, watchtype));
		this.orderId = orderId;
		this.priority = priority;
		this.watchtype = watchtype;
	}

	public String getOrderId() {
		return orderId;
	}

	public int getPriority() {
		return priority;
	}

	public WatchType getWatchtype() {
		return watchtype;
	}
}
