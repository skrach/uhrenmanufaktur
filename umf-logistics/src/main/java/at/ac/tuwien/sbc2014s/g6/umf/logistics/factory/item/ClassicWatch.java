package at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item;

import java.util.List;

public class ClassicWatch extends Watch {
	private static final long serialVersionUID = 1L;

	public ClassicWatch(String id, String assemblyWorkerId, List<Component> components) {
		super(id, assemblyWorkerId, components);
	}

	@Override
	public String toString() {
		return "Classic watch [id=" + this.getId() + "]";
	}

}
