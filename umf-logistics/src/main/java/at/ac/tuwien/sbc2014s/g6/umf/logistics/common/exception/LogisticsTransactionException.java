package at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception;

public class LogisticsTransactionException extends Exception {

	private static final long serialVersionUID = 1L;

	public LogisticsTransactionException() {
		super();
	}

	public LogisticsTransactionException(String message) {
		super(message);
	}

	public LogisticsTransactionException(Throwable throwable) {
		super(throwable);
	}

	public LogisticsTransactionException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
