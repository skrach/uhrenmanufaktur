package at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception;

public class LoadingProviderFailedException extends Exception {

	private static final long serialVersionUID = 1L;

	public LoadingProviderFailedException() {
		super();
	}

	public LoadingProviderFailedException(String message) {
		super(message);
	}

	public LoadingProviderFailedException(Throwable throwable) {
		super(throwable);
	}

	public LoadingProviderFailedException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
