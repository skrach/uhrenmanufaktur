package at.ac.tuwien.sbc2014s.g6.umf.logistics.distributor;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LoadingProviderFailedException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsServiceException;


public interface DistributorService {

	/**
	 * Call this method to start a service
	 * @param param Optional. If no parameter is supplied, a port is chosen.
	 * @throws LogisticsServiceException
	 * @return A distributor instance
	 */
	public abstract Distributor startService(String parameter) throws LogisticsServiceException;

	/**
	 * Called to stop a service
	 * @throws LogisticsServiceException
	 */
	public abstract void stopService() throws LogisticsServiceException;

	/**
	 * Tries to load and instatiate class specified by provider source name
	 * (PSN). PSN format: "JavaClassname[|[Argument]]" If "Argument" is present
	 * in PSN, the constructor taking one String argument is called, else the
	 * nullary constructor will be called.
	 *
	 * @param providerSourceName
	 * @return Class implementing DistributorService interface
	 * @throws LoadingProviderFailedException
	 */
	public static DistributorService createDistributorService(String providerSourceName) throws LoadingProviderFailedException {
		final Logger log = LogManager.getLogger(DistributorService.class);

		String[] psnParts = providerSourceName.split("\\|", 2);

		if (psnParts.length < 1) {
			String msg = String.format("Provider source name \"%s\" format invalid", providerSourceName);
			log.info(msg);
			throw new LoadingProviderFailedException(msg);
		}

		Class<?> c;
		try {
			c = Class.forName(psnParts[0]);
		} catch (ClassNotFoundException e) {
			log.info(String.format("Failed loading provider class \"%s\" due to %s: %s", psnParts[0],
					e.getClass().getName(), e.getMessage()));
			throw new LoadingProviderFailedException(e);
		}

		if (!DistributorService.class.isAssignableFrom(c)) {
			String msg = String.format(
					"Class \"%s\" cannot be used as logistics provider, because it does not implement interface \"%s\"",
					c.getName(), DistributorService.class.getName());
			log.info(msg);
			throw new LoadingProviderFailedException(msg);
		}

		Constructor<?> ctor;
		try {
			if (psnParts.length > 1) {
				ctor = c.getDeclaredConstructor(String.class);
				log.info(String.format(
						"Successfully loaded provider class for source name \"%s\" and will use constructor with argument.",
						providerSourceName));
				return (DistributorService) ctor.newInstance(psnParts[1]);
			} else {
				ctor = c.getDeclaredConstructor();
				log.info(String.format(
						"Successfully loaded provider class for source name \"%s\" and will use nullary constructor.",
						providerSourceName));
				return (DistributorService) ctor.newInstance();
			}
		} catch (NoSuchMethodException | SecurityException e) { // getDeclaredConstructor
			log.error(String.format("Failed loading provider class for source name \"%s\" due to %s: %s",
					providerSourceName, e.getClass().getName(), e.getMessage()));
			throw new LoadingProviderFailedException(e);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) { // newInstance
			log.error(String.format("Failed instantiating provider class for source name \"%s\" due to %s: %s",
					providerSourceName, e.getClass().getName(), e.getMessage()));
			throw new LoadingProviderFailedException(e);
		}
	}
}