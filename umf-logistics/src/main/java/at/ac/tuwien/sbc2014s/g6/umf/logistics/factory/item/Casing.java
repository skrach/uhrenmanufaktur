package at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item;

public class Casing extends RecycleableComponent {
	private static final long serialVersionUID = 1L;

	public Casing(String id, String supplierId) {
		super(id, supplierId);
	}

	@Override
	public String toString() {
		return "Casing [id=" + getId() + "]";
	}
}
