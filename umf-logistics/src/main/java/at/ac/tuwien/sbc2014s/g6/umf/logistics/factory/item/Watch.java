package at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public abstract class Watch extends Item {
	private static final long serialVersionUID = 1L;
	private final String assemblyWorkerId;
	private String logisticsWorkerId;
	private String qaWorkerId;
	private Integer qualityRating;
	private boolean recycled;
	private final List<Component> components;
	private Instant modificationDate;
	private OrderWorkItem orderWorkItem;
	private String distributorId;
	private String accountManagerId;

	public Watch(String id, String assemblyWorkerId, List<Component> components) {
		super(id);
		this.assemblyWorkerId = assemblyWorkerId;
		this.components = new ArrayList<Component>();
		this.components.addAll(components);
	}

	public String getAccountManagerId() {
		return this.accountManagerId;
	}

	public String getAssemblyWorkerId() {
		return this.assemblyWorkerId;
	}

	public List<Component> getComponents() {
		ArrayList<Component> result = new ArrayList<Component>();
		result.addAll(this.components);
		return result;
	}

	public String getDistributorId() {
		return this.distributorId;
	}

	public String getLogisticsWorkerId() {
		return this.logisticsWorkerId;
	}

	public Instant getModificationDate() {
		return this.modificationDate;
	}

	public String getQaWorkerId() {
		return this.qaWorkerId;
	}

	public Integer getQualityRating() {
		return this.qualityRating;
	}

	public boolean isRecycled() {
		return this.recycled;
	}

	public void setAccountManagerId(String accountManagerId) {
		this.accountManagerId = accountManagerId;
	}

	public void setDistributorId(String distributorId) {
		this.distributorId = distributorId;
	}

	public void setLogisticsWorkerId(String logisticsWorkerId) {
		this.logisticsWorkerId = logisticsWorkerId;
		this.setModificationDate();
	}

	public void setModificationDate() {
		this.modificationDate = Instant.now();
	}

	public String getOrderId() {
		return (this.orderWorkItem == null ? null : this.orderWorkItem.getOrderId());
	}

	public OrderWorkItem getOrderWorkItem() {
		return this.orderWorkItem;
	}

	public void setOrderWorkItem(OrderWorkItem orderWorkItem) {
		this.orderWorkItem = orderWorkItem;
	}

	public void setQaWorkerId(String qaWorkerId) {
		this.qaWorkerId = qaWorkerId;
		this.setModificationDate();
	}

	public void setQualityRating(int qualityRating) {
		this.qualityRating = qualityRating;
		this.setModificationDate();
	}

	public void setRecycled() {
		this.recycled = true;
		this.setModificationDate();
	}

	@Override
	public String toString() {
		return "Watch [id=" + this.getId() + "]";
	}

}
