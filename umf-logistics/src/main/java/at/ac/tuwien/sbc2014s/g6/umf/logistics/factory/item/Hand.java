package at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item;

public class Hand extends RecycleableComponent {
	private static final long serialVersionUID = 1L;

	public Hand(String id, String supplierId) {
		super(id, supplierId);
	}

	@Override
	public String toString() {
		return "Hand [id=" + this.getId() + "]";
	}
}
