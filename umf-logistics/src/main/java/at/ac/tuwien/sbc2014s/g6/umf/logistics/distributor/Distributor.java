package at.ac.tuwien.sbc2014s.g6.umf.logistics.distributor;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.ItemUpdateCallback;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LoadingProviderFailedException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Item;

public interface Distributor {

	/**
	 * Tries to load and instatiate class specified by provider source name
	 * (PSN). PSN format: "JavaClassname[|[Argument]]" If "Argument" is present
	 * in PSN, the constructor taking one String argument is called, else the
	 * nullary constructor will be called.
	 *
	 * @param providerSourceName
	 * @return Class implementing Factory interface
	 * @throws LoadingProviderFailedException
	 */
	public static Distributor createDistributor(String providerSourceName) throws LoadingProviderFailedException {
		final Logger log = LogManager.getLogger(Distributor.class);

		String[] psnParts = providerSourceName.split("\\|", 2);

		if (psnParts.length < 1) {
			String msg = String.format("Provider source name \"%s\" format invalid", providerSourceName);
			log.info(msg);
			throw new LoadingProviderFailedException(msg);
		}

		Class<?> c;
		try {
			c = Class.forName(psnParts[0]);
		} catch (ClassNotFoundException e) {
			log.info(String.format("Failed loading provider class \"%s\" due to %s: %s", psnParts[0],
					e.getClass().getName(), e.getMessage()));
			throw new LoadingProviderFailedException(e);
		}

		if (!Distributor.class.isAssignableFrom(c)) {
			String msg = String.format(
					"Class \"%s\" cannot be used as logistics provider, because it does not implement interface \"%s\"",
					c.getName(), Distributor.class.getName());
			log.info(msg);
			throw new LoadingProviderFailedException(msg);
		}

		Constructor<?> ctor;
		try {
			if (psnParts.length > 1) {
				ctor = c.getDeclaredConstructor(String.class);
				log.info(String.format(
						"Successfully loaded provider class for source name \"%s\" and will use constructor with argument.",
						providerSourceName));
				return (Distributor) ctor.newInstance(psnParts[1]);
			} else {
				ctor = c.getDeclaredConstructor();
				log.info(String.format(
						"Successfully loaded provider class for source name \"%s\" and will use nullary constructor.",
						providerSourceName));
				return (Distributor) ctor.newInstance();
			}
		} catch (NoSuchMethodException | SecurityException e) { // getDeclaredConstructor
			log.error(String.format("Failed loading provider class for source name \"%s\" due to %s: %s",
					providerSourceName, e.getClass().getName(), e.getMessage()));
			throw new LoadingProviderFailedException(e);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) { // newInstance
			log.error(String.format("Failed instantiating provider class for source name \"%s\" due to %s: %s",
					providerSourceName, e.getClass().getName(), e.getMessage()));
			throw new LoadingProviderFailedException(e);
		}
	}

    /**
     * Reads the current contents of the depot "Stock". (non-taking)
     *
     * @return List of items in depot
     * @throws DistributorException
     */
    public List<? extends Item> readDepotStock() throws LogisticsException;

    /**
     * Reads the current contents of the depot "Sold". (non-taking)
     *
     * @return List of items in depot
     * @throws DistributorException
     */
    public List<? extends Item> readDepotSold() throws LogisticsException;

    /**
     * Subscribe to put or take events of the depot "Stock".
     *
     * @param callback
     *            Implementation of ItemUpdateCallback
     */
    public void subscribeToDepotStock(ItemUpdateCallback callback);

    /**
     * Subscribe to put or take events of the depot "Sold".
     *
     * @param callback
     *            Implementation of ItemUpdateCallback
     */
    public void subscribeToDepotSold(ItemUpdateCallback callback);

	/**
	 * Returns a new implementation of FactoryTransaction interface.
	 *
	 * @return Class implementing FactoryTransaction interface
	 * @throws LogisticsTransactionException
	 */
	public DistributorTransaction begin() throws LogisticsTransactionException, InterruptedException;

	/**
	 * Start subscriptions. This method must be called after subscribing to
	 * events and before creating a transaction (including implicit ones).
	 *
	 * @throws LogisticsException
	 */
	public void start() throws LogisticsException;

	/**
	 * Clear all subscriptions and tear down provider specific resources.
	 *
	 * @throws LogisticsException
	 */
	public void stop() throws LogisticsException;

	/**
	 * Returns the distributor psn of this distributor
	 * @return the distributor psn
	 */
	public String getDistributorPsn();
}
