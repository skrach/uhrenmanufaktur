package at.ac.tuwien.sbc2014s.g6.umf.logistics.factory;

import java.util.HashMap;
import java.util.Map;

public class IdPrefix {

	private static final Map<IdType, String> idPrefixes = new HashMap<>();

	static {
		idPrefixes.put(IdType.ITEM_CASING, "ICA");
		idPrefixes.put(IdType.ITEM_CLOCKWORK, "ICL");
		idPrefixes.put(IdType.ITEM_HAND, "IHA");
		idPrefixes.put(IdType.ITEM_WATCH, "IWA");
		idPrefixes.put(IdType.ITEM_LEATHER_WRISTBAND, "ILWR");
		idPrefixes.put(IdType.ITEM_METAL_WRISTBAND, "IMWR");
		idPrefixes.put(IdType.WORKER_ASSEMBLY, "WA");
		idPrefixes.put(IdType.WORKER_LOGISTICS, "WL");
		idPrefixes.put(IdType.WORKER_QA, "WQ");
		idPrefixes.put(IdType.WORKER_SUPPLIER, "WS");
		idPrefixes.put(IdType.WORKER_ACCOUNTMANAGER, "WM");
		idPrefixes.put(IdType.WORKER_DISTRIBUTOR, "WD");
		idPrefixes.put(IdType.ITEM_ORDER, "IOR");
	}

	public static String getPrefixedId(int id, IdType idType) {
		return String.format("%s%d", idPrefixes.get(idType), id);
	}
}
