package at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.WatchType;

public class Order extends Item {
	public static final int PRIORITY_HIGH = 0;
	public static final int PRIORITY_NORMAL = 1;
	public static final int PRIORITY_LOW = 2;

	private Instant modificationDate;
	private final int priority;

	// openorderCount fields are calculated
	private int openOrderCountClassicWatch;
	private int openOrderCountSportWatch;
	private int openOrderCountSportWatchTZS;

	private final int orderCountClassicWatch;
	private final int orderCountSportWatch;
	private final int orderCountSportWatchTZS;

	private static final long serialVersionUID = 1L;
	private final Map<WatchType, List<Watch>> orderedWatches = new HashMap<WatchType, List<Watch>>();

	public Order(String id, int priority, int orderCountClassicWatch, int orderCountSportWatch, int orderCountSportWatchTZS) {
		super(id);
		this.priority = priority;

		// intialize empty orders
		for (WatchType type : WatchType.values()) {
			this.orderedWatches.put(type, new ArrayList<Watch>());
		}

		// set order count
		this.orderCountClassicWatch = orderCountClassicWatch;
		this.orderCountSportWatch = orderCountSportWatch;
		this.orderCountSportWatchTZS = orderCountSportWatchTZS;
		this.calculateOpenOrderCount();
	}

	private void calculateOpenOrderCount() {
		this.openOrderCountClassicWatch = this.orderCountClassicWatch
				- this.orderedWatches.get(WatchType.CLASSIC_WATCH).size();
		this.openOrderCountSportWatch = this.orderCountSportWatch - this.orderedWatches.get(WatchType.SPORT_WATCH).size();
		this.openOrderCountSportWatchTZS = this.orderCountSportWatchTZS
				- this.orderedWatches.get(WatchType.SPORT_WATCH_TZS).size();
	}

	public void addWatch(Watch watch) {
		int precond;
		if (watch instanceof ClassicWatch) {
			precond = this.openOrderCountClassicWatch;
		} else if (watch instanceof SportWatch) {
			precond = this.openOrderCountSportWatch;
		} else if (watch instanceof SportWatchTZS) {
			precond = this.openOrderCountSportWatchTZS;
		} else {
			throw new IllegalStateException(String.format("Unexpected instance type %s encountered", watch));
		}

		if (precond > 0) {
			if (watch instanceof ClassicWatch) {
				this.orderedWatches.get(WatchType.CLASSIC_WATCH).add(watch);
			} else if (watch instanceof SportWatch) {
				this.orderedWatches.get(WatchType.SPORT_WATCH).add(watch);
			} else if (watch instanceof SportWatchTZS) {
				this.orderedWatches.get(WatchType.SPORT_WATCH_TZS).add(watch);
			} else {
				throw new IllegalStateException(String.format("Unexpected instance type %s encountered", watch));
			}

			this.calculateOpenOrderCount();

		} else {
			throw new IllegalArgumentException(String.format("Can not add watch %s as it is not needed for order %d", watch,
					this));
		}
	}

	public int getOpenOrderCount(WatchType watchType) {
		switch (watchType) {
		case CLASSIC_WATCH:
			return this.getOpenOrderCountClassicWatch();
		case SPORT_WATCH:
			return this.getOpenOrderCountSportWatch();
		case SPORT_WATCH_TZS:
			return this.getOpenOrderCountSportWatchTZS();
		default:
			throw new IllegalArgumentException("Unknown watch type encountered");
		}
	}

	public int getOpenOrderCountClassicWatch() {
		return this.openOrderCountClassicWatch;
	}

	public int getOpenOrderCountSportWatch() {
		return this.openOrderCountSportWatch;
	}

	public int getOpenOrderCountSportWatchTZS() {
		return this.openOrderCountSportWatchTZS;
	}

	public WatchType getHighestOpenOrderCountWatchType(List<WatchType> candidates) {
		// get highest open order count of candidate watch types
		Integer highestOrderCount = candidates.stream().map(e -> this.getOpenOrderCount(e)).max(Integer::max).orElse(0);

		// get watch type of the highest order count
		if (highestOrderCount > 0) {
			List<WatchType> winners = candidates.stream().filter(e -> this.getOpenOrderCount(e) == highestOrderCount)
					.collect(Collectors.toList());
			return winners.get(0);
		}

		for (WatchType watchType : WatchType.values()) {
			if (this.getOpenOrderCount(watchType) == highestOrderCount) {
				return watchType;
			}
		}

		throw new IllegalStateException("Internal error encountered");
	}

	public int getOrderCountClassicWatch() {
		return this.orderCountClassicWatch;
	}

	public int getOrderCountSportWatch() {
		return this.orderCountSportWatch;
	}

	public int getOrderCountSportWatchTZS() {
		return this.orderCountSportWatchTZS;
	}

	public Map<WatchType, List<Watch>> getOrderedWatches() {
		return this.orderedWatches;
	}

	public int getPriority() {
		return this.priority;
	}

	public Instant getModificationDate() {
		return this.modificationDate;
	}

	public void setModificationDate() {
		this.modificationDate = Instant.now();
	}

	@Override
	public String toString() {
		return "Order [id=" + this.getId() + "]";
	}
}
