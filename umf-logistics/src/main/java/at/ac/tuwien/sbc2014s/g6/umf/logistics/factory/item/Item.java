package at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item;

import java.io.Serializable;
import java.time.Instant;

public abstract class Item implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String id;

	public String getId() {
		return this.id;
	}

	public Item(String id) {
		this.id = id;
		this.createDate = Instant.now();
	}

	private final Instant createDate;

	public Instant getCreateDate() {
		return this.createDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Item))
			return false;
		Item other = (Item) obj;
		if (this.id == null) {
			if (other.id != null)
				return false;
		} else if (!this.id.equals(other.id))
			return false;
		return true;
	}

}
