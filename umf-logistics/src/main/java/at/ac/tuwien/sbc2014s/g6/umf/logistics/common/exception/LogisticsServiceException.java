package at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception;

public class LogisticsServiceException extends Exception {

	private static final long serialVersionUID = 1L;

	public LogisticsServiceException() {
		super();
	}

	public LogisticsServiceException(String message) {
		super(message);
	}

	public LogisticsServiceException(Throwable throwable) {
		super(throwable);
	}

	public LogisticsServiceException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
