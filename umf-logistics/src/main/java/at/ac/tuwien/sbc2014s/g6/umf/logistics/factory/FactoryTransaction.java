package at.ac.tuwien.sbc2014s.g6.umf.logistics.factory;

import java.util.Set;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.WatchType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionTimeoutException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Casing;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Clockwork;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Component;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.DistributorRegistration;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Hand;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Item;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.LeatherWristband;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.MetalWristband;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Order;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.OrderWorkItem;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Watch;

public interface FactoryTransaction {

	public class Constants {
		public static final int TIMEOUT_INFINITE = 0;
		public static final int TIMEOUT_DEFAULT = -1;
	}

	/**
	 * Commits all changes within this transaction and renders this
	 * FactoryTransaction instance useless.
	 *
	 * @throws LogisticsTransactionException
	 *             on failure
	 * @throws LogisticsTransactionTimeoutException
	 *             on timeout
	 */
	public void commit() throws LogisticsTransactionException, LogisticsTransactionTimeoutException, InterruptedException;

	/**
	 * Rolls back all changes within this transaction and renders this
	 * FactoryTransaction instance useless.
	 *
	 * @throws LogisticsTransactionException
	 *             on failure
	 */
	public void rollback() throws LogisticsTransactionException;

	/**
	 * Puts DistributorRegistration into depot "DistributorRegistration".
	 *
	 * @param distributorRegistration
	 * @throws LogisticsTransactionException
	 * @throws LogisticsTransactionTimeoutException
	 */
	public void put(DistributorRegistration distributorRegistration) throws LogisticsTransactionException,
			LogisticsTransactionTimeoutException;

	/**
	 * Puts DistributorRegistration into depot "DistributorRegistration" and
	 * into queue "Account Manager Work Items".
	 *
	 * @param distributorRegistration
	 * @throws LogisticsTransactionException
	 * @throws LogisticsTransactionTimeoutException
	 */
	public void update(DistributorRegistration distributorRegistration) throws LogisticsTransactionException,
			LogisticsTransactionTimeoutException;

	/**
	 * Puts Order into depot "Order".
	 *
	 * @param distributorRegistration
	 * @throws LogisticsTransactionException
	 * @throws LogisticsTransactionTimeoutException
	 */
	public void put(Order order) throws LogisticsTransactionException, LogisticsTransactionTimeoutException;

	/**
	 * Puts OrderWorkItem into depot "OrderWorkItem".
	 *
	 * @param order
	 * @throws LogisticsTransactionException
	 * @throws LogisticsTransactionTimeoutException
	 */
	public void put(OrderWorkItem order) throws LogisticsTransactionException, LogisticsTransactionTimeoutException;

	/**
	 * Puts Order into depot "Order" and creates appropriate OrderWorkItems and
	 * puts them into Depot "OrderWorkItem". Default implementation should
	 * suffice.
	 *
	 * @param order
	 * @throws LogisticsTransactionException
	 * @throws LogisticsTransactionTimeoutException
	 */
	public default void putNewOrder(Order order) throws LogisticsTransactionException, LogisticsTransactionTimeoutException {
		this.put(order);
		for (WatchType watchType : WatchType.values()) {
			for (int i = 0; i < order.getOpenOrderCount(watchType); i++) {
				this.put(new OrderWorkItem(order.getId(), order.getPriority(), watchType));
			}
		}
	}

	/**
	 * Puts component into depot "Component".
	 *
	 * @param component
	 * @throws LogisticsTransactionException
	 *             on failure
	 * @throws LogisticsTransactionTimeoutException
	 *             on timeout
	 */
	public void put(Component component) throws LogisticsTransactionException, LogisticsTransactionTimeoutException;

	/**
	 * Put watch into one of the following depots: - depot "Assembled", when
	 * quality rating is not set and distributorId is null - depot "Tested" when
	 * quality rating is set and distributorId is null - depot "Delivered" when
	 * distributorId is set
	 *
	 * @param watch
	 * @throws LogisticsTransactionException
	 *             on failure
	 * @throws LogisticsTransactionTimeoutException
	 *             on timeout
	 */
	public void put(Watch watch) throws LogisticsTransactionException, LogisticsTransactionTimeoutException;

	/**
	 * Put watch into depot "Delivery" and into queue
	 * "Account Manager Work Items".
	 *
	 * @param watch
	 *            The watch to deliver
	 * @throws LogisticsTransactionException
	 *             on failure
	 * @throws LogisticsTransactionTimeoutException
	 *             on timeout
	 * @throws InterruptedException
	 */
	public void dispatch(Watch watch) throws LogisticsTransactionException, LogisticsTransactionTimeoutException,
			InterruptedException;

	/**
	 * Put watch into depot "Recycled".
	 *
	 * @param watch
	 * @throws LogisticsTransactionException
	 *             on failure
	 * @throws LogisticsTransactionTimeoutException
	 *             on timeout
	 */
	public void recycle(Watch watch) throws LogisticsTransactionException, LogisticsTransactionTimeoutException;

	/**
	 * Retrieves a Casing, removing it from the depot "Components". (blocking,
	 * no timeout) If the depot does not contain any Casing Objects, waits until
	 * one gets available.
	 *
	 * @return Casing
	 * @throws LogisticsTransactionException
	 *             on failure
	 * @throws LogisticsTransactionTimeoutException
	 *             on timeout
	 * @throws InterruptedException
	 *             when current thread was interrupted
	 */
	public Casing takeCasing() throws LogisticsTransactionException, InterruptedException,
			LogisticsTransactionTimeoutException;

	/**
	 * Retrieves a Clockwork, removing it from the depot "Components".
	 * (blocking, no timeout) If the depot does not contain any Clockwork
	 * Objects, waits until one gets available.
	 *
	 * @return Clockwork
	 * @throws LogisticsTransactionException
	 *             on failure
	 * @throws LogisticsTransactionTimeoutException
	 *             on timeout
	 * @throws InterruptedException
	 *             when current thread was interrupted
	 */
	public Clockwork takeClockwork() throws LogisticsTransactionException, InterruptedException,
			LogisticsTransactionTimeoutException;

	/**
	 * Retrieves a Hand, removing it from the depot "Components". (blocking, no
	 * timeout) If the depot does not contain any Hand Objects, waits until one
	 * gets available.
	 *
	 * @return Hand
	 * @throws LogisticsTransactionException
	 *             on failure
	 * @throws LogisticsTransactionTimeoutException
	 *             on timeout
	 * @throws InterruptedException
	 *             when current thread was interrupted
	 */
	public Hand takeHand() throws LogisticsTransactionException, InterruptedException, LogisticsTransactionTimeoutException;

	/**
	 * Retrieves a Wristband, removing it from the depot "Components".
	 * (blocking, no timeout) If the depot does not contain any Wristband
	 * Objects, waits until one gets available.
	 *
	 * @return Wristband
	 * @throws LogisticsTransactionException
	 *             on failure
	 * @throws LogisticsTransactionTimeoutException
	 *             on timeout
	 * @throws InterruptedException
	 *             when current thread was interrupted
	 */
	public LeatherWristband takeLeatherWristband() throws LogisticsTransactionException, InterruptedException,
			LogisticsTransactionTimeoutException;

	/**
	 * Retrieves a Wristband, removing it from the depot "Components".
	 * (blocking, no timeout) If the depot does not contain any Wristband
	 * Objects, waits until one gets available.
	 *
	 * @return Wristband
	 * @throws LogisticsTransactionException
	 *             on failure
	 * @throws LogisticsTransactionTimeoutException
	 *             on timeout
	 * @throws InterruptedException
	 *             when current thread was interrupted
	 */
	public MetalWristband takeMetalWristband() throws LogisticsTransactionException, InterruptedException,
			LogisticsTransactionTimeoutException;

	/**
	 * Retrieves a Watch, removing it from the depot "Assembled". (blocking, no
	 * timeout) If the depot does not contain any Watch Objects, waits until one
	 * gets available.
	 *
	 * @return Watch
	 * @throws LogisticsTransactionException
	 *             on failure
	 * @throws LogisticsTransactionTimeoutException
	 *             on timeout
	 * @throws InterruptedException
	 *             when current thread was interrupted
	 */
	public Watch takeWatch() throws LogisticsTransactionException, InterruptedException,
			LogisticsTransactionTimeoutException;

	/**
	 * Retrieves a Watch matching the given quality class, removing it from the
	 * depot "Tested". (blocking with timeout, FIFO) If the depot does not
	 * contain any Watch Objects, waits until one gets available or timeout is
	 * hit.
	 *
	 * @param qualityRatingLowerBound
	 *            Minimum quality rating of watch
	 * @param qualityRatingUpperBound
	 *            Maximum quality rating of watch
	 * @param timeout
	 *            Time in milliseconds after which this method return.
	 *            <tt>FactoryTransaction.Constants.TIMEOUT_*</tt> constants can
	 *            be used.
	 * @return Watch A watch object or null if no watch could be fetched until
	 *         the given timeout had been reached.
	 * @throws LogisticsTransactionException
	 *             on failure
	 * @throws LogisticsTransactionTimeoutException
	 *             on timeout
	 * @throws InterruptedException
	 *             when current thread was interrupted
	 */
	public Watch takeWatch(int qualityRatingLowerBound, int qualityRatingUpperBound, int timeout)
			throws LogisticsTransactionException, InterruptedException, LogisticsTransactionTimeoutException;

	/**
	 * Take a watch with the given watchId from the depot "Delivery". This
	 * method is non-blocking!
	 *
	 * @param watchId
	 * @return A watch or null if a watch with the given watchId could not be
	 *         found.
	 * @throws LogisticsTransactionException
	 * @throws LogisticsTransactionTimeoutException
	 * @throws InterruptedException
	 */
	public Watch takeDeliveryWatch(String watchId) throws InterruptedException, LogisticsTransactionTimeoutException,
			LogisticsTransactionException;

	/**
	 * Take a watch with the given watch type from the depot "Delivery". This
	 * method is non-blocking!
	 *
	 * @param watchType
	 * @return A watch or null if a watch with the given watchId could not be
	 *         found.
	 * @throws LogisticsTransactionException
	 * @throws LogisticsTransactionTimeoutException
	 * @throws InterruptedException
	 */
	public Watch takeNextDeliveryWatch(WatchType watchType) throws InterruptedException,
			LogisticsTransactionTimeoutException, LogisticsTransactionException;

	/**
	 * Take a DistributorRegistration with the given
	 * distributorProviderSourceName. Blocking mit default timeout.
	 *
	 * @param distributorProviderSourceName
	 * @throws LogisticsTransactionException
	 * @throws InterruptedException
	 * @throws LogisticsTransactionTimeoutException
	 * @return DistributorRegistration or null
	 */
	public DistributorRegistration takeDistributorRegistration(String distributorProviderSourceName)
			throws LogisticsTransactionException, InterruptedException, LogisticsTransactionTimeoutException;

	/**
	 * Take next DistributorRegistration requiring the given WatchType. This
	 * method is non blocking and returns null if no distributor could be found!
	 *
	 * Taken DistributorRegistration should be put back into List with put.
	 *
	 * @param watchType
	 * @return DistributorRegistration requiring given WatchType or null, if no
	 *         Distributor needs the given WatchType.
	 */
	public DistributorRegistration takeNextDistributorRegistration(WatchType watchType)
			throws LogisticsTransactionException, InterruptedException, LogisticsTransactionTimeoutException;

	/**
	 * This method returns a work item for an AccountManager. This Item can be a
	 * {@link Watch} or a {@link DistributorRegistration} object. The underlying
	 * container must be populated when a new Distributor registers or when a
	 * new Watch that doesn't belong to an order, is put into depot delivery.
	 *
	 * @return A {@link Watch} or {@link DistributorRegistration} item
	 * @throws LogisticsTransactionException
	 * @throws InterruptedException
	 * @throws LogisticsTransactionTimeoutException
	 */
	public Item takeAccountManagerWorkerItem() throws LogisticsTransactionException, InterruptedException,
			LogisticsTransactionTimeoutException;

	/**
	 * Take a new OrderWorkItem. Non blocking.
	 *
	 * If orders with priority high or normal exist, which need one of the
	 * candidate WatchTypes, then one of those is returned. If there are only
	 * Orders with priority low, then one of them will be returned, if parameter
	 * prevOrder is false. Otherwise no order will be returned. Orders created
	 * first are served first. This method is non blocking and returns null if
	 * no components are available.
	 *
	 * @param candidates
	 *            A set of watch types the worker can produce
	 * @param prevOrder
	 *            Indicate if the last watch produced by the worker was for an
	 *            order.
	 * @return An {@link OrderWorkItem} or null if no candidate matches the
	 *         watches required by orders.
	 * @throws LogisticsTransactionException
	 * @throws LogisticsTransactionTimeoutException
	 * @throws InterruptedException
	 */
	public OrderWorkItem takeOrderWorkItem(Set<WatchType> candidates, boolean prevOrder) throws InterruptedException,
			LogisticsTransactionTimeoutException, LogisticsTransactionException;

	/**
	 * Take order with given order id. Blocking.
	 *
	 * @param orderId
	 * @return
	 * @throws LogisticsTransactionException
	 * @throws LogisticsTransactionTimeoutException
	 * @throws InterruptedException
	 */
	public Order takeOrder(String orderId) throws InterruptedException, LogisticsTransactionTimeoutException,
			LogisticsTransactionException;

	/**
	 * Returns a new id (unique within given type).
	 *
	 * @param type
	 *            IdType enum, specifying the scope for uniqueness
	 * @return Unique id string
	 * @throws LogisticsTransactionException
	 *             on failure
	 * @throws LogisticsTransactionTimeoutException
	 *             on timeout
	 * @throws InterruptedException
	 *             when current thread was interrupted
	 */
	public String getNewId(IdType type) throws LogisticsTransactionException, InterruptedException,
			LogisticsTransactionTimeoutException;

	/**
	 * Checks whether the given amount of the given ComponentType is available
	 * to be taken. Non blocking.
	 *
	 * @param c
	 * @param amount
	 * @return True if given amount is available, false otherwise
	 * @throws LogisticsTransactionException
	 */
	public boolean isComponentAvailable(ComponentType c, int amount) throws LogisticsTransactionException,
			InterruptedException, LogisticsTransactionTimeoutException;

}
