package at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item;

public class RecycleableComponent extends Component {
	private static final long serialVersionUID = 1L;

	private int recycleCount;

	public int getRecycleCount() {
		return this.recycleCount;
	}

	public void setRecycleCount(int recycleCount) {
		this.recycleCount = recycleCount;
	}

	public RecycleableComponent(String id, String supplierId) {
		super(id, supplierId);
	}

}
