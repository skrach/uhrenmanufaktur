package at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception;

public class LogisticsTransactionTimeoutException extends Exception {

	private static final long serialVersionUID = 1L;

	public LogisticsTransactionTimeoutException() {
		super();
	}

	public LogisticsTransactionTimeoutException(String message) {
		super(message);
	}

	public LogisticsTransactionTimeoutException(Throwable throwable) {
		super(throwable);
	}

	public LogisticsTransactionTimeoutException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
