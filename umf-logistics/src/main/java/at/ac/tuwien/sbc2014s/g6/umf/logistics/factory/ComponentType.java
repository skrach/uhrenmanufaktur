package at.ac.tuwien.sbc2014s.g6.umf.logistics.factory;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Casing;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Clockwork;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Component;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Hand;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.LeatherWristband;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.MetalWristband;

/**
 * Type identifier used for id uniqueness scopes
 */
public enum ComponentType {
	CASING, CLOCKWORK, HAND, METALWRISTBAND, LEATHERWRISTBAND;

	public static ComponentType getComponentType(Component c) {
		if (c instanceof Casing) {
			return ComponentType.CASING;
		} else if (c instanceof Clockwork) {
			return ComponentType.CLOCKWORK;
		} else if (c instanceof Hand) {
			return ComponentType.HAND;
		} else if (c instanceof MetalWristband) {
			return ComponentType.METALWRISTBAND;
		} else if (c instanceof LeatherWristband) {
			return ComponentType.LEATHERWRISTBAND;
		} else {
			throw new IllegalStateException("Unexpected component type encountered");
		}
	}
}
