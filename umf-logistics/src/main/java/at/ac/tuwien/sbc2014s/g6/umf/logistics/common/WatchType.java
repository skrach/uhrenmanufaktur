package at.ac.tuwien.sbc2014s.g6.umf.logistics.common;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.ClassicWatch;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.SportWatch;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.SportWatchTZS;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Watch;

/**
 * Type identifier used for id uniqueness scopes
 */
public enum WatchType {
	CLASSIC_WATCH, SPORT_WATCH, SPORT_WATCH_TZS;
	
	public static WatchType getWatchType(Watch watch) {
		if (watch instanceof ClassicWatch) {
			return WatchType.CLASSIC_WATCH;
		} else if (watch instanceof SportWatch) {
			return WatchType.SPORT_WATCH;
		} else if (watch instanceof SportWatchTZS) {
			return WatchType.SPORT_WATCH_TZS;
		} else {
			throw new IllegalStateException("Unexpected watch type encountered");
		}
	}
}
