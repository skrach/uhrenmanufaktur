package at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.WatchType;

public class DistributorRegistration extends Item implements Comparable<DistributorRegistration> {

	public class StockInfo implements Serializable {
		private static final long serialVersionUID = 1L;
		private int stockCapacity;
		private int stockCount;

		public int getStockCapacity() {
			return this.stockCapacity;
		}

		public void setStockCapacity(int stockCapacity) {
			this.stockCapacity = stockCapacity;
		}

		public int getStockCount() {
			return this.stockCount;
		}

		public void setStockCount(int stockCount) {
			this.stockCount = stockCount;
		}

		public boolean isStockFull() {
			return this.stockCapacity <= this.stockCount;
		}
	}

	private String workerId;

	public String getWorkerId() {
		return this.workerId;
	}

	public void setWorkerId(String workerId) {
		this.workerId = workerId;
	}

	private boolean stockFullClassicWatch;

	private boolean stockFullSportWatch;

	private boolean stockFullSportWatchTZS;

	private Instant lastWatchReceived;

	private static final long serialVersionUID = 1L;

	private final Map<WatchType, StockInfo> watchStock = new HashMap<WatchType, DistributorRegistration.StockInfo>();

	public DistributorRegistration(String distributorPsn) {
		super(distributorPsn);

		// intialize empty stocks
		for (WatchType type : WatchType.values()) {
			StockInfo stockInfo = new StockInfo();
			stockInfo.setStockCapacity(0);
			stockInfo.setStockCount(0);
			this.watchStock.put(type, stockInfo);
		}

		this.stockFullClassicWatch = true;
		this.stockFullSportWatch = true;
		this.stockFullSportWatchTZS = true;

		// initialize date
		this.updateLastWatchReceived();
	}

	private void updateStockFull(WatchType watchType) {
		boolean stockFull = this.watchStock.get(watchType).isStockFull();

		switch (watchType) {
		case CLASSIC_WATCH:
			this.stockFullClassicWatch = stockFull;
			break;
		case SPORT_WATCH:
			this.stockFullSportWatch = stockFull;
			break;
		case SPORT_WATCH_TZS:
			this.stockFullSportWatchTZS = stockFull;
			break;
		default:
			throw new IllegalStateException(String.format("Illegal watch type %s encountered", watchType));
		}
	}

	/**
	 * Get stock information for a given watch type
	 *
	 * @param watchType
	 * @return Stock information like stock capacity and stock count
	 */
	public StockInfo getStockInfo(WatchType watchType) {
		return this.watchStock.get(watchType);
	}

	public void setStockCapacity(WatchType watchType, int stockCapacity) {
		this.watchStock.get(watchType).setStockCapacity(stockCapacity);
		this.updateStockFull(watchType);
	}

	public void setStockCount(WatchType watchType, int stockCount) {
		this.watchStock.get(watchType).setStockCount(stockCount);
		this.updateStockFull(watchType);
	}

	public String getDistributorPsn() {
		return this.getId();
	}

	public boolean isStockFullClassicWatch() {
		return this.stockFullClassicWatch;
	}

	public boolean isStockFullSportWatch() {
		return this.stockFullSportWatch;
	}

	public boolean isStockFullSportWatchTZS() {
		return this.stockFullSportWatchTZS;
	}

	@Override
	public int compareTo(DistributorRegistration o) {
		return this.getLastWatchReceived().compareTo(o.getLastWatchReceived());
	}

	public Instant getLastWatchReceived() {
		return this.lastWatchReceived;
	}

	public void updateLastWatchReceived() {
		this.lastWatchReceived = Instant.now();
	}
	
	@Override
	public String toString() {
		return this.getDistributorPsn();
	}
}
