package at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item;

public class Clockwork extends Component {
	private static final long serialVersionUID = 1L;

	public Clockwork(String id, String supplierId) {
		super(id, supplierId);
	}

	@Override
	public String toString() {
		return "Clockwork [id=" + this.getId() + "]";
	}
}
