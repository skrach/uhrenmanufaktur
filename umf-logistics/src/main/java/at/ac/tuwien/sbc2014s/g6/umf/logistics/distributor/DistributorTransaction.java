package at.ac.tuwien.sbc2014s.g6.umf.logistics.distributor;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.WatchType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionTimeoutException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Watch;

public interface DistributorTransaction {

	/**
	 * Commits all changes within this transaction and renders this
	 * FactoryTransaction instance useless.
	 *
	 * @throws LogisticsTransactionException
	 *             on failure
	 * @throws LogisticsTransactionTimeoutException
	 *             on timeout
	 */
	public void commit() throws LogisticsTransactionException, LogisticsTransactionTimeoutException, InterruptedException;

	/**
	 * Rolls back all changes within this transaction and renders this
	 * FactoryTransaction instance useless.
	 *
	 * @throws LogisticsTransactionException
	 *             on failure
	 */
	public void rollback() throws LogisticsTransactionException;

	/**
	 * Sell a random watch. This moves the watch from the stock to the sold. Blocking.
	 * depot.
	 * @return 
	 * @throws InterruptedException
	 * @throws LogisticsTransactionTimeoutException
	 */
	public WatchType sellWatch() throws LogisticsTransactionException, InterruptedException, LogisticsTransactionTimeoutException;

	/**
	 * Put watch to stock depot.
	 *
	 * @param watch
	 * @throws LogisticsTransactionException
	 * @throws LogisticsTransactionTimeoutException
	 * @throws InterruptedException
	 */
	public void put(Watch watch) throws LogisticsTransactionException, InterruptedException,
			LogisticsTransactionTimeoutException;

}
