package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs.item;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Item;

public class IdCounter extends Item {
	private static final long serialVersionUID = 1L;

	private int lastId;

	public int getNextId() {
		return this.lastId++;
	}

	public IdCounter(String id) {
		super(id);
	}
}
