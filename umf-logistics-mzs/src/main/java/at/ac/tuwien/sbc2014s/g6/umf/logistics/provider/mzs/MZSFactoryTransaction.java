package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mozartspaces.capi3.ComparableProperty;
import org.mozartspaces.capi3.CountNotMetException;
import org.mozartspaces.capi3.FifoCoordinator;
import org.mozartspaces.capi3.KeyCoordinator;
import org.mozartspaces.capi3.LabelCoordinator;
import org.mozartspaces.capi3.Query;
import org.mozartspaces.capi3.QueryCoordinator;
import org.mozartspaces.capi3.Selector;
import org.mozartspaces.capi3.TypeCoordinator;
import org.mozartspaces.core.Capi;
import org.mozartspaces.core.ContainerReference;
import org.mozartspaces.core.Entry;
import org.mozartspaces.core.MzsConstants;
import org.mozartspaces.core.MzsCoreException;
import org.mozartspaces.core.MzsCoreRuntimeException;
import org.mozartspaces.core.MzsTimeoutException;
import org.mozartspaces.core.TransactionException;
import org.mozartspaces.util.parser.sql.javacc.ParseException;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.WatchType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionTimeoutException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.ComponentType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.FactoryTransaction;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.IdPrefix;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.IdType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Casing;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Clockwork;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Component;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.DistributorRegistration;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Hand;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Item;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.LeatherWristband;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.MetalWristband;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Order;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.OrderWorkItem;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Watch;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs.config.MZSFactoryConfig;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs.config.MZSProviderConfigUtil;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs.item.IdCounter;

public class MZSFactoryTransaction extends AbstractMZSTransactionProvider implements FactoryTransaction {

	private final static Logger log = LogManager.getLogger(MZSFactoryTransaction.class);

	public MZSFactoryTransaction(Capi capi, URI spaceURI, Map<ContainerId, ContainerReference> containerRefs,
			Configuration configuration) throws LogisticsTransactionException {
		super(capi, spaceURI, containerRefs, configuration, MZSProviderConfigUtil
				.decodeTransactionTimeoutProperty(configuration.getString(MZSFactoryConfig.TRANSACTIONTIMEOUT)));
	}

	@Override
	public void put(DistributorRegistration d) throws LogisticsTransactionException, LogisticsTransactionTimeoutException {
		Entry e = new Entry(d);
		ContainerId cId = ContainerId.DISTRIBUTOR;
		this.put(e, cId, MzsConstants.RequestTimeout.DEFAULT);
	}

	@Override
	public void update(DistributorRegistration d) throws LogisticsTransactionException, LogisticsTransactionTimeoutException {
		this.put(d);

		// queue a workitem
		Entry e = new Entry(d);
		ContainerId cId = ContainerId.AMWORKITEM;
		this.put(e, cId, MzsConstants.RequestTimeout.DEFAULT);
	}

	@Override
	public void put(Order o) throws LogisticsTransactionException, LogisticsTransactionTimeoutException {
		Entry e = new Entry(o);
		ContainerId cId = ContainerId.ORDER;
		this.put(e, cId, MzsConstants.RequestTimeout.DEFAULT);
	}

	@Override
	public void put(Component c) throws LogisticsTransactionException, LogisticsTransactionTimeoutException {
		Entry e = new Entry(c, LabelCoordinator.newCoordinationData(ComponentType.getComponentType(c).name()));
		ContainerId cId = ContainerId.COMPONENT;
		this.put(e, cId, MzsConstants.RequestTimeout.DEFAULT);
	}

	@Override
	public void put(Watch w) throws LogisticsTransactionException, LogisticsTransactionTimeoutException {
		Entry e = new Entry(w);

		ContainerId cId;
		if (w.getDistributorId() != null) {
			cId = ContainerId.DELIVERED;
		} else if (w.getQualityRating() == null) {
			cId = ContainerId.ASSEMBLED;
		} else {
			cId = ContainerId.TESTED;
		}

		this.put(e, cId, MzsConstants.RequestTimeout.DEFAULT);
	}

	@Override
	public void put(OrderWorkItem o) throws LogisticsTransactionException, LogisticsTransactionTimeoutException {
		Entry e = new Entry(o, LabelCoordinator.newCoordinationData(o.getWatchtype().name()));
		ContainerId cId = ContainerId.ORDERWORKITEM;
		this.put(e, cId, MzsConstants.RequestTimeout.DEFAULT);
	}

	@Override
	public void dispatch(Watch w) throws LogisticsTransactionException, LogisticsTransactionTimeoutException {
		Entry e = new Entry(w, LabelCoordinator.newCoordinationData(WatchType.getWatchType(w).name()));
		ContainerId cId = ContainerId.DELIVERY;
		this.put(e, cId, MzsConstants.RequestTimeout.DEFAULT);

		e = new Entry(w);
		cId = ContainerId.AMWORKITEM;
		this.put(e, cId, MzsConstants.RequestTimeout.DEFAULT);
	}

	@Override
	public void recycle(Watch w) throws LogisticsTransactionException, LogisticsTransactionTimeoutException {
		Entry e = new Entry(w);
		ContainerId cId = ContainerId.RECYCLED;
		this.put(e, cId, MzsConstants.RequestTimeout.DEFAULT);
	}

	@Override
	public Casing takeCasing() throws LogisticsTransactionException, InterruptedException,
			LogisticsTransactionTimeoutException {
		ContainerId cId = ContainerId.COMPONENT;
		String label = ComponentType.CASING.name();
		List<Selector> selectors = new ArrayList<>();
		selectors.add(LabelCoordinator.newSelector(label, 1));

		log.debug(String.format("Trying to get %s from container \"%s\".", label, cId.name()));

		return this.takeOne(cId, selectors, MzsConstants.RequestTimeout.INFINITE);
	}

	@Override
	public Clockwork takeClockwork() throws LogisticsTransactionException, InterruptedException,
			LogisticsTransactionTimeoutException {
		ContainerId cId = ContainerId.COMPONENT;
		String label = ComponentType.CLOCKWORK.name();
		List<Selector> selectors = new ArrayList<>();
		selectors.add(LabelCoordinator.newSelector(label, 1));

		log.debug(String.format("Trying to get %s from container \"%s\".", label, cId.name()));

		return this.takeOne(cId, selectors, MzsConstants.RequestTimeout.INFINITE);
	}

	@Override
	public Hand takeHand() throws LogisticsTransactionException, InterruptedException, LogisticsTransactionTimeoutException {
		ContainerId cId = ContainerId.COMPONENT;
		String label = ComponentType.HAND.name();
		List<Selector> selectors = new ArrayList<>();
		selectors.add(LabelCoordinator.newSelector(label, 1));

		log.debug(String.format("Trying to get %s from container \"%s\".", label, cId.name()));

		return this.takeOne(cId, selectors, MzsConstants.RequestTimeout.INFINITE);
	}

	@Override
	public LeatherWristband takeLeatherWristband() throws LogisticsTransactionException, InterruptedException,
			LogisticsTransactionTimeoutException {
		ContainerId cId = ContainerId.COMPONENT;
		String label = ComponentType.LEATHERWRISTBAND.name();
		List<Selector> selectors = new ArrayList<>();
		selectors.add(LabelCoordinator.newSelector(label, 1));

		log.debug(String.format("Trying to get %s from container \"%s\".", label, cId.name()));

		return this.takeOne(cId, selectors, MzsConstants.RequestTimeout.INFINITE);
	}

	@Override
	public MetalWristband takeMetalWristband() throws LogisticsTransactionException, InterruptedException,
			LogisticsTransactionTimeoutException {
		ContainerId cId = ContainerId.COMPONENT;
		String label = ComponentType.METALWRISTBAND.name();
		List<Selector> selectors = new ArrayList<>();
		selectors.add(LabelCoordinator.newSelector(label, 1));

		log.debug(String.format("Trying to get %s from container \"%s\".", label, cId.name()));

		return this.takeOne(cId, selectors, MzsConstants.RequestTimeout.INFINITE);
	}

	@Override
	public Watch takeWatch() throws LogisticsTransactionException, InterruptedException,
			LogisticsTransactionTimeoutException {
		ContainerId cId = ContainerId.ASSEMBLED;
		List<Selector> selectors = new ArrayList<>();
		selectors.add(TypeCoordinator.newSelector(Watch.class, 1));

		log.debug(String.format("Trying to get %s from container \"%s\".", Watch.class.getName(), cId.name()));

		return this.takeOne(cId, selectors, MzsConstants.RequestTimeout.INFINITE);
	}

	@Override
	public Watch takeWatch(int qualityRatingLowerBound, int qualityRatingUpperBound, int timeout)
			throws LogisticsTransactionException, InterruptedException, LogisticsTransactionTimeoutException {
		ContainerId cId = ContainerId.TESTED;

		List<Selector> selectors = new ArrayList<>();
		try {
			selectors.add(QueryCoordinator.newSelector(new Query().sql(String.format(
					"qualityRating >= %d AND qualityRating <= %d", qualityRatingLowerBound, qualityRatingUpperBound)),
					MzsConstants.Selecting.COUNT_MAX));
		} catch (ParseException e) {
			log.fatal(e);
			throw new LogisticsTransactionException(e);
		}
		selectors.add(FifoCoordinator.newSelector(1));

		long requestTimeout;
		switch (timeout) {
		case Constants.TIMEOUT_DEFAULT:
			requestTimeout = MzsConstants.RequestTimeout.TRY_ONCE;
		case Constants.TIMEOUT_INFINITE:
			requestTimeout = MzsConstants.RequestTimeout.INFINITE;
		default:
			requestTimeout = timeout;
		}

		log.debug(String.format("Trying to get %s with quality rating between %d and %d from container \"%s\".",
				Watch.class.getName(), qualityRatingLowerBound, qualityRatingUpperBound, cId.name()));

		return this.takeOne(cId, selectors, requestTimeout);
	}

	@Override
	public Watch takeDeliveryWatch(String watchId) throws InterruptedException, LogisticsTransactionTimeoutException,
			LogisticsTransactionException {
		ContainerId cId = ContainerId.DELIVERY;

		List<Selector> selectors = new ArrayList<>();
		try {
			selectors.add(QueryCoordinator.newSelector(new Query().sql(String.format("id = '%s'", watchId)), 1));
		} catch (ParseException e) {
			log.fatal(e);
			throw new LogisticsTransactionException(e);
		}

		log.debug(String.format("Trying to get %s with id \"%s\" from container \"%s\".",
				DistributorRegistration.class.getName(), watchId, cId.name()));

		return this.takeOne(cId, selectors, MzsConstants.RequestTimeout.TRY_ONCE);
	}

	@Override
	public Watch takeNextDeliveryWatch(WatchType watchType) throws InterruptedException,
			LogisticsTransactionTimeoutException, LogisticsTransactionException {
		ContainerId cId = ContainerId.DELIVERY;
		String label = watchType.name();
		List<Selector> selectors = new ArrayList<>();
		selectors.add(LabelCoordinator.newSelector(label, 1));

		log.debug(String.format("Trying to get %s from container \"%s\"", label, cId.name()));

		return this.takeOne(cId, selectors, MzsConstants.RequestTimeout.INFINITE);
	}

	@Override
	public Order takeOrder(String orderId) throws InterruptedException, LogisticsTransactionTimeoutException,
			LogisticsTransactionException {
		ContainerId cId = ContainerId.ORDER;

		List<Selector> selectors = new ArrayList<>();
		try {
			selectors.add(QueryCoordinator.newSelector(new Query().sql(String.format("id = '%s'", orderId)), 1));
		} catch (ParseException e) {
			log.fatal(e);
			throw new LogisticsTransactionException(e);
		}

		log.debug(String.format("Trying to get %s with id \"%s\" from container \"%s\".",
				DistributorRegistration.class.getName(), orderId, cId.name()));

		return this.takeOne(cId, selectors, MzsConstants.RequestTimeout.INFINITE);
	}

	@Override
	public DistributorRegistration takeDistributorRegistration(String distributorProviderSourceName)
			throws LogisticsTransactionException, InterruptedException, LogisticsTransactionTimeoutException {
		ContainerId cId = ContainerId.DISTRIBUTOR;

		List<Selector> selectors = new ArrayList<>();
		try {
			selectors.add(QueryCoordinator.newSelector(
					new Query().sql(String.format("id = '%s'", distributorProviderSourceName)), 1));
		} catch (ParseException e) {
			log.fatal(e);
			throw new LogisticsTransactionException(e);
		}

		log.debug(String.format("Trying to get %s with distributor psn \"%s\" from container \"%s\"",
				DistributorRegistration.class.getName(), distributorProviderSourceName, cId.name()));

		return this.takeOne(cId, selectors, MzsConstants.RequestTimeout.TRY_ONCE);
	}

	@Override
	public DistributorRegistration takeNextDistributorRegistration(WatchType watchType)
			throws LogisticsTransactionException, InterruptedException, LogisticsTransactionTimeoutException {
		ContainerId cId = ContainerId.DISTRIBUTOR;

		String watchTypeCriteria = null;
		switch (watchType) {
		case CLASSIC_WATCH:
			watchTypeCriteria = "stockFullClassicWatch = FALSE";
			break;
		case SPORT_WATCH:
			watchTypeCriteria = "stockFullSportWatch = FALSE";
			break;
		case SPORT_WATCH_TZS:
			watchTypeCriteria = "stockFullSportWatchTZS = FALSE";
			break;
		default:
			throw new IllegalArgumentException("Watchtype not set.");
		}

		List<Selector> selectors = new ArrayList<>();
		try {
			selectors.add(QueryCoordinator.newSelector(new Query().sql(watchTypeCriteria)
					.sortup(ComparableProperty.forName("lastWatchReceived")).cnt(1)));
		} catch (ParseException e) {
			log.fatal(e);
			throw new LogisticsTransactionException(e);
		}

		log.debug(String.format("Trying to get longest not services %s in need of \"%s\" from container \"%s\".",
				DistributorRegistration.class.getName(), watchType.name(), cId.name()));

		return this.takeOne(cId, selectors, MzsConstants.RequestTimeout.TRY_ONCE);
	}

	@Override
	public Item takeAccountManagerWorkerItem() throws LogisticsTransactionException, InterruptedException,
			LogisticsTransactionTimeoutException {
		ContainerId cId = ContainerId.AMWORKITEM;

		List<Selector> selectors = new ArrayList<>();
		selectors.add(FifoCoordinator.newSelector(1));

		log.debug(String.format("Trying to get next %s from container \"%s\"", Item.class.getName(), cId.name()));

		return this.takeOne(cId, selectors, MzsConstants.RequestTimeout.INFINITE);
	}

	@Override
	public OrderWorkItem takeOrderWorkItem(Set<WatchType> candidates, boolean prevOrder)
			throws LogisticsTransactionException, InterruptedException, LogisticsTransactionTimeoutException {
		ContainerId cId = ContainerId.ORDERWORKITEM;
		OrderWorkItem fittingWorkItem = null;

		for (int priority : new int[] { Order.PRIORITY_HIGH, Order.PRIORITY_NORMAL, Order.PRIORITY_LOW }) {
			for (WatchType watchType : candidates) {
				List<Selector> selectors = new ArrayList<>();
				try {
					selectors.add(QueryCoordinator.newSelector(new Query().sql(String.format("priority = %d", priority)),
							MzsConstants.Selecting.COUNT_MAX));
				} catch (ParseException e) {
					log.fatal(e);
					throw new LogisticsTransactionException(e);
				}
				String label = watchType.name();
				selectors.add(LabelCoordinator.newSelector(label, MzsConstants.Selecting.COUNT_MAX));
				selectors.add(FifoCoordinator.newSelector(1));

				log.debug(String.format("Trying to get %s with priority %d and watch type \"%s\" from container \"%s\".",
						OrderWorkItem.class.getName(), priority, label, cId.name()));

				fittingWorkItem = this.takeOne(cId, selectors, MzsConstants.RequestTimeout.TRY_ONCE);

				if (fittingWorkItem != null) {
					return fittingWorkItem;
				}
			}
		}

		return null;
	}

	@Override
	public String getNewId(IdType type) throws LogisticsTransactionException, InterruptedException,
			LogisticsTransactionTimeoutException {
		ContainerId cId = ContainerId.IDCOUNTER;
		List<Selector> selectors = new ArrayList<>();
		selectors.add(KeyCoordinator.newSelector(type.name()));

		log.info(String.format("Trying to get id type %s from container %s.", type.name(), cId.name()));
		IdCounter idCounter = this.takeOne(cId, selectors, MzsConstants.RequestTimeout.INFINITE);

		int newId = idCounter.getNextId();

		Entry e = new Entry(idCounter, KeyCoordinator.newCoordinationData(type.name()));
		this.put(e, cId, MzsConstants.RequestTimeout.DEFAULT);
		return IdPrefix.getPrefixedId(newId, type);
	}

	@Override
	public boolean isComponentAvailable(ComponentType c, int amount) throws LogisticsTransactionException,
			InterruptedException, LogisticsTransactionTimeoutException {
		ContainerId cId = ContainerId.ORDERWORKITEM;
		List<Selector> selectors = new ArrayList<>();
		String label = c.name();
		selectors.add(LabelCoordinator.newSelector(label, amount));

		log.debug(String.format("Testing whether %d components of type \"%s\" are available in container \"%s\".", amount,
				label, cId.name()));

		long requestTimeout = MzsConstants.RequestTimeout.TRY_ONCE;

		// test
		ContainerReference cRef = this.getContainerRefs().get(cId);
		try {
			this.getCapi().test(cRef, selectors, requestTimeout, this.getTransactionRef());
			return true;
		} catch (CountNotMetException e) {
			return false;
		} catch (MzsTimeoutException e) {
			// only way to distinguish between request and transaction timeout
			// is checking for exception message :-<
			if (e.getMessage().equals("Request timed out")) {
				String msg = String.format("Request timeout hit while waiting for item from depot %s.", cId.name());
				log.warn(msg);
				throw new LogisticsTransactionTimeoutException(msg, e);
			} else {
				String msg = String.format("Transaction timeout hit while waiting for item from depot %s.", cId.name());
				log.warn(msg);
				throw new LogisticsTransactionTimeoutException(msg, e);
			}
		} catch (TransactionException e) {
			String msg = String.format("Failed testing items for container \"%s\". Transaction invalid.", cId.name());
			log.warn(msg, e);
			throw new LogisticsTransactionTimeoutException(msg, e);
		} catch (MzsCoreException e) {
			String msg = "Failed to test Item.";
			log.error(msg, e);
			throw new LogisticsTransactionException(msg, e);
		} catch (MzsCoreRuntimeException e) {
			if (e.getCause() instanceof InterruptedException) {
				throw (InterruptedException) e.getCause();
			}
			throw e;
		}
	}

}
