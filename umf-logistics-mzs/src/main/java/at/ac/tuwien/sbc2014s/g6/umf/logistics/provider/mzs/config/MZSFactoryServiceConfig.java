package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs.config;

import org.apache.commons.configuration.Configuration;

import at.ac.tuwien.sbc2014s.g6.umf.util.Config;

public class MZSFactoryServiceConfig {

	public static final String SERVICE_LISTENINGPORT = "Service.ListeningPort";

	public static final String CONTAINER_COMPONENT = "Container.Component";
	public static final String CONTAINER_ASSEMBLED = "Container.Assembled";
	public static final String CONTAINER_TESTED = "Container.Tested";
	public static final String CONTAINER_DELIVERY = "Container.Delivery";
	public static final String CONTAINER_DELIVERED = "Container.Delivered";
	public static final String CONTAINER_RECYCLED = "Container.Recycled";
	public static final String CONTAINER_ORDER = "Container.Order";
	public static final String CONTAINER_DISTRIBUTOR = "Container.Distributor";
	public static final String CONTAINER_IDCOUNTER = "Container.IdCounter";
	public static final String CONTAINER_AMWORKITEM = "Container.AMWorkItem";
	public static final String CONTAINER_ORDERWORKITEM = "Container.OrderWorkItem";
	public static final String CONTAINER_BENCHMARK = "Container.Benchmark";

	public static Configuration getConfiguration() {
		return Config.getConfig("mzsfactoryservice.properties");
	}
}
