package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs;

import java.net.URI;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mozartspaces.capi3.CountNotMetException;
import org.mozartspaces.capi3.Selector;
import org.mozartspaces.core.Capi;
import org.mozartspaces.core.ContainerReference;
import org.mozartspaces.core.Entry;
import org.mozartspaces.core.MzsCoreException;
import org.mozartspaces.core.MzsCoreRuntimeException;
import org.mozartspaces.core.MzsTimeoutException;
import org.mozartspaces.core.TransactionException;
import org.mozartspaces.core.TransactionReference;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionTimeoutException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Item;

public abstract class AbstractMZSTransactionProvider {

	private final static Logger log = LogManager.getLogger(AbstractMZSTransactionProvider.class);

	private final TransactionReference transactionRef;
	private final Capi capi;
	private final Map<ContainerId, ContainerReference> containerRefs;
	private final Configuration config;

	public TransactionReference getTransactionRef() {
		return this.transactionRef;
	}

	public Capi getCapi() {
		return this.capi;
	}

	public Map<ContainerId, ContainerReference> getContainerRefs() {
		return this.containerRefs;
	}

	public Configuration getConfig() {
		return this.config;
	}

	public AbstractMZSTransactionProvider(Capi capi, URI spaceURI, Map<ContainerId, ContainerReference> containerRefs,
			Configuration configuration, long timeout) throws LogisticsTransactionException {
		this.capi = capi;
		this.containerRefs = containerRefs;
		this.config = configuration;

		try {
			this.transactionRef = this.capi.createTransaction(timeout, spaceURI);
		} catch (MzsCoreException e) {
			String msg = "Failed creating transaction.";
			log.error(msg, e);
			throw new LogisticsTransactionException(msg, e);
		}
	}

	public void commit() throws LogisticsTransactionException, LogisticsTransactionTimeoutException, InterruptedException {
		try {
			this.capi.commitTransaction(this.transactionRef);
		} catch (MzsTimeoutException e) {
			String msg = "Failed commiting transaction. Transaction timed out.";
			log.warn(msg);
			throw new LogisticsTransactionTimeoutException(msg, e);
		} catch (TransactionException e) {
			String msg = String.format(
					"Failed commiting transaction %s. Transaction invalid and therefore propably already rolled back.",
					this.transactionRef);
			log.warn(msg, e);
			throw new LogisticsTransactionTimeoutException(msg, e);
		} catch (MzsCoreException e) {
			String msg = "Failed commiting transaction.";
			log.error(msg, e);
			throw new LogisticsTransactionException(msg, e);
		} catch (MzsCoreRuntimeException e) {
			if (e.getCause() instanceof InterruptedException) {
				throw (InterruptedException) e.getCause();
			}
			throw e;
		}
	}

	public void rollback() throws LogisticsTransactionException {
		try {
			this.capi.rollbackTransaction(this.transactionRef);
		} catch (TransactionException e) {
			log.warn(String.format(
					"Failed rolling back transaction %s. Transaction invalid and therefore propably already rolled back.",
					this.transactionRef), e);
		} catch (MzsTimeoutException e) {
			log.info(String.format("Transaction %s already timed out and therefore is already rolled back.",
					this.transactionRef));
		} catch (MzsCoreException e) {
			String msg = "Failed rolling back transaction.";
			log.error(msg, e);
			throw new LogisticsTransactionException(msg, e);
		}
	}

	protected void put(Entry entry, ContainerId cId, long requestTimeout) throws LogisticsTransactionException,
			LogisticsTransactionTimeoutException {
		try {
			this.capi.write(entry, this.containerRefs.get(cId), requestTimeout, this.transactionRef);
		} catch (TransactionException e) {
			String msg = String.format("Failed putting item into container \"%s\". Transaction invalid.", cId.name());
			log.warn(msg, e);
			throw new LogisticsTransactionTimeoutException(msg, e);
		} catch (MzsTimeoutException e) {
			String msg = String.format("Failed putting item into container \"%s\". Transaction timed out.", cId.name());
			log.warn(msg);
			throw new LogisticsTransactionTimeoutException(msg, e);
		} catch (MzsCoreException e) {
			String msg = String.format("Failed putting item into container \"%s\".", cId.name());
			log.error(msg, e);
			throw new LogisticsTransactionException(msg, e);
		}
	}

	protected <T extends Item> T takeOne(ContainerId cId, List<Selector> selectors, long requestTimeout)
			throws LogisticsTransactionException, InterruptedException, LogisticsTransactionTimeoutException {
		ContainerReference cRef = this.containerRefs.get(cId);
		try {
			List<T> entries = this.capi.take(cRef, selectors, requestTimeout, this.transactionRef);
			if (entries.size() != 1) {
				String msg = String.format("Failed to get exactly one item. Take result size = %d.", entries.size());
				log.warn(msg);
				throw new LogisticsTransactionException(msg);
			}

			return entries.get(0);
		} catch ( CountNotMetException e ) {
			return null;
		} catch (MzsTimeoutException e) {
			// only way to distinguish between request and transaction timeout
			// is checking for exception message :-<
			if (e.getMessage().equals("Request timed out")) {
				// log.info(String.format("Request timeout hit while waiting for item from depot %s.",
				// cId.name()));
				return null;
			} else {
				String msg = String.format("Transaction timeout hit while waiting for item from depot %s.", cId.name());
				log.warn(msg);
				throw new LogisticsTransactionTimeoutException(msg, e);
			}
		} catch (TransactionException e) {
			String msg = String.format("Failed taking item from container \"%s\". Transaction invalid.", cId.name());
			log.warn(msg, e);
			throw new LogisticsTransactionTimeoutException(msg, e);
		} catch (MzsCoreException e) {
			String msg = "Failed to get Item.";
			log.error(msg, e);
			throw new LogisticsTransactionException(msg, e);
		} catch (MzsCoreRuntimeException e) {
			if (e.getCause() instanceof InterruptedException) {
				throw (InterruptedException) e.getCause();
			}
			throw e;
		}
	}

}
