package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs;

import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mozartspaces.capi3.FifoCoordinator;
import org.mozartspaces.capi3.Selector;
import org.mozartspaces.core.Capi;
import org.mozartspaces.core.ContainerReference;
import org.mozartspaces.core.Entry;
import org.mozartspaces.core.MzsConstants;
import org.mozartspaces.core.MzsCore;
import org.mozartspaces.core.MzsCoreException;
import org.mozartspaces.notifications.Notification;
import org.mozartspaces.notifications.NotificationListener;
import org.mozartspaces.notifications.NotificationManager;
import org.mozartspaces.notifications.Operation;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.ItemUpdateCallback;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Item;

abstract public class AbstractMZSProvider {

	final static Logger log = LogManager.getLogger(AbstractMZSProvider.class);

	private Configuration config;
	private URI spaceUri;
	private MzsCore core;
	private Capi capi;
	private NotificationManager notificationManager;
	private boolean embedded = false;

	private final List<Subscription> subscriptions = new ArrayList<>();

	private Map<ContainerId, ContainerReference> containerRefs;

	private Map<ContainerId, String> cPropertyNames = new HashMap<>();

	public Configuration getConfig() {
		return this.config;
	}

	public URI getSpaceUri() {
		return this.spaceUri;
	}

	public MzsCore getCore() {
		return this.core;
	}

	public Capi getCapi() {
		return this.capi;
	}

	public NotificationManager getNotificationManager() {
		return this.notificationManager;
	}

	public boolean isEmbedded() {
		return this.embedded;
	}

	public void addSubscription( Subscription s ) {
		this.subscriptions.add(s);
	}

	public void initialize(MzsCore core, Configuration config, URI spaceUri, Map<ContainerId, String> containerConfigPropertyNames) {
		this.config = config;
		this.core = core;
		this.capi = new Capi(this.core);
		this.notificationManager = new NotificationManager(this.core);
		this.cPropertyNames = containerConfigPropertyNames;

		if ( spaceUri == null) {
			this.spaceUri = core.getConfig().getSpaceUri();
			this.embedded = true;
			log.debug(String.format("Initialized %s and determined space uri \"%s\"",
					this.getClass().getSimpleName(), this.spaceUri.toString()));
		} else {
			this.spaceUri = spaceUri;
			log.debug(String.format("Initialized %s and set space uri \"%s\"",
					this.getClass().getSimpleName(), this.spaceUri.toString()));
		}
	}

	/**
	 * Returns the map of container references to be used for container
	 * specification in space operations.
	 *
	 * @return Unmodifiable list of <tt>ContainerReference</tt> object.
	 * @throws LogisticsException
	 */
	protected Map<ContainerId, ContainerReference> getContainerRefs() throws LogisticsException {
		if (this.containerRefs == null) {
			Map<ContainerId, ContainerReference> cRefs = new HashMap<>();
			for (Map.Entry<ContainerId, String> item : this.cPropertyNames.entrySet()) {
				String containerName = this.config.getString(item.getValue());
				try {
					cRefs.put(item.getKey(), this.capi.lookupContainer(containerName, (this.embedded ? null : this.spaceUri),
							MzsConstants.RequestTimeout.DEFAULT, null));
				} catch (MzsCoreException e) {
					String msg = String.format("Failed to look up container \"%s\".", containerName);
					log.error(msg, e);
					throw new LogisticsException(msg, e);
				}
			}
			this.containerRefs = Collections.unmodifiableMap(cRefs);
		}
		return this.containerRefs;
	}

	protected List<? extends Item> readDepot(ContainerId cId) throws LogisticsException {
		log.debug(String.format("Reading depot \"%s\"...", cId.name()));

		ContainerReference cRef = this.getContainerRefs().get(cId);
		long requestTimeout = MzsConstants.RequestTimeout.DEFAULT;
		List<Selector> selectors = new ArrayList<>();
		selectors.add(FifoCoordinator.newSelector(MzsConstants.Selecting.COUNT_ALL));
		try {
			List<? extends Item> result = this.capi.read(cRef, selectors, requestTimeout, null);
			log.info(String.format("Read %d items from depot \"%s\".", result.size(), cId.name()));
			return result;
		} catch (MzsCoreException e) {
			String msg = String.format("Failed to get item list in depot \"%s\".", cId.name());
			log.error(msg, e);
			throw new LogisticsException(msg, e);
		}
	}

	private void subscribeToDepot(final ContainerId cId, final ItemUpdateCallback callback) throws LogisticsException {
		log.debug(String.format("Subscribing to changes of depot \"%s\"...", cId.name()));

		ContainerReference cRef = this.getContainerRefs().get(cId);
		if (cRef == null) {
			throw new LogisticsException(String.format("Container \"%s\" not found.", cId));
		}

		NotificationListener notificationListener;

		notificationListener = new NotificationListener() {
			@Override
			public void entryOperationFinished(final Notification notification, final Operation operation,
					final List<? extends Serializable> entries) {
				List<Item> items = entries.stream().map(e -> (Item) ((Entry) e).getValue()).collect(Collectors.toList());
				log.info(String.format("Notified about %s operation on %d items for depot \"%s\".", Operation.WRITE.name(),
						items.size(), cId.name()));
				callback.notifyPut(items);
			}
		};

		try {
			try {
				this.notificationManager.createNotification(cRef, notificationListener, Operation.WRITE);
				log.info(String.format("Subscribed to %s operation for depot \"%s\".", Operation.WRITE.name(), cId.name()));
			} catch (MzsCoreException e) {
				String msg = String.format("Failed to subscribe to %s operation in depot \"%s\".", Operation.WRITE.name(),
						cId.name());
				log.error(msg, e);
				throw new LogisticsException(msg, e);
			}

			notificationListener = new NotificationListener() {
				@Override
				public void entryOperationFinished(final Notification notification, final Operation operation,
						final List<? extends Serializable> entries) {
					List<Item> items = entries.stream().map(e -> (Item) e).collect(Collectors.toList());
					log.info(String.format("Notified about %s operation on %d items for depot \"%s\".",
							Operation.TAKE.name(), items.size(), cId.name()));
					callback.notifyTake(items);
				}
			};
			try {
				this.notificationManager.createNotification(cRef, notificationListener, Operation.TAKE);
				log.info(String.format("Subscribed to %s operation for depot \"%s\".", Operation.TAKE.name(), cId.name()));
			} catch (MzsCoreException e) {
				String msg = String.format("Failed to subscribe to %s operation in depot \"%s\".", Operation.TAKE.name(),
						cId.name());
				log.error(msg, e);
				throw new LogisticsException(msg, e);
			}
		} catch (InterruptedException e) {
			String msg = String.format("Failed to subscribe to %s or %s operation in depot \"%s\". Thread was interrupted.",
					Operation.TAKE.name(), Operation.WRITE.name(), cId.name());
			log.error(msg, e);

			// set interrupt flag, because of caught InterruptedException
			Thread.currentThread().interrupt();
			throw new LogisticsException(msg, e);
		}
	}

	public void start() throws LogisticsException {
		log.info(String.format("Starting subscriptions for %s.", this.getClass().getName()));
		for (Subscription s : this.subscriptions) {
			this.subscribeToDepot(s.getContainerId(), s.getCallback());
		}
	}

	public void stop() throws LogisticsException {
		log.info(String.format("Shutting down %s.", this.getClass().getName()));

		this.notificationManager.shutdown();
		this.core.shutdown(true);
	}

}
