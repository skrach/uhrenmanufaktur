package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.ItemUpdateCallback;

public class Subscription {
	private final ContainerId containerId;

	private final ItemUpdateCallback callback;

	public Subscription(ContainerId cId, ItemUpdateCallback callback) {
		super();
		this.containerId = cId;
		this.callback = callback;
	}

	public ContainerId getContainerId() {
		return this.containerId;
	}

	public ItemUpdateCallback getCallback() {
		return this.callback;
	}

}