package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs.config;

import org.mozartspaces.core.MzsConstants;

public class MZSProviderConfigUtil {

	public static long decodeTransactionTimeoutProperty(String value) {
		switch (value) {
		case "INFINITE":
			return MzsConstants.TransactionTimeout.INFINITE;
		default:
			return Long.parseLong(value);
		}
	}
}
