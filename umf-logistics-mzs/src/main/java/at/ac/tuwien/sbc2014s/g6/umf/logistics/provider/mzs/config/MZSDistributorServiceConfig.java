package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs.config;

import org.apache.commons.configuration.Configuration;

import at.ac.tuwien.sbc2014s.g6.umf.util.Config;

public class MZSDistributorServiceConfig {

	public static final String SERVICE_LISTENINGPORT = "Service.ListeningPort";

	public static final String CONTAINER_STOCK = "Container.Stock";
	public static final String CONTAINER_SOLD = "Container.Sold";

	public static Configuration getConfiguration() {
		return Config.getConfig("mzsdistributorservice.properties");
	}
}
