package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs;

import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mozartspaces.capi3.Coordinator;
import org.mozartspaces.capi3.FifoCoordinator;
import org.mozartspaces.capi3.RandomCoordinator;
import org.mozartspaces.capi3.TypeCoordinator;
import org.mozartspaces.core.Capi;
import org.mozartspaces.core.ContainerReference;
import org.mozartspaces.core.DefaultMzsCore;
import org.mozartspaces.core.MzsConstants;
import org.mozartspaces.core.MzsCore;
import org.mozartspaces.core.MzsCoreException;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsServiceException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.distributor.Distributor;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.distributor.DistributorService;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Watch;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs.config.MZSDistributorServiceConfig;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs.config.MZSFactoryServiceConfig;
import at.ac.tuwien.sbc2014s.g6.umf.util.AvailablePortFinder;

public class MZSDistributorService implements DistributorService {

	private static final Logger log = LogManager.getLogger(MZSFactoryService.class);

	private static MzsCore core;

	public static void main(String[] args) {
		if (args.length > 1) {
			log.fatal(String.format("Invalid number of arguments. Only one optional argument \"%s\" is expected.",
					MZSFactoryServiceConfig.SERVICE_LISTENINGPORT));
			System.exit(1);
		}

		String port = null;
		if (args.length == 1) {
			port = args[0];
		}

		try {
			(new MZSDistributorService()).startService(port);
		} catch (LogisticsServiceException e) {
		}
	}

	@Override
	public Distributor startService(String parameter) throws LogisticsServiceException {
		// get configuration
		Configuration config = MZSDistributorServiceConfig.getConfiguration();

		int port = AvailablePortFinder.getNextAvailable();
		if (parameter != null) {
			try {
				port = Integer.parseInt(parameter);
				if (port < 1 || port > 65535) {
					log.fatal(String.format("\"%s\" is not in valid tcp port range.", parameter));
					System.exit(3);
				}
			} catch (NumberFormatException e) {
				log.fatal(String.format("\"%s\" is no valid integer.", parameter));
				System.exit(2);
			}
		}

		// start server
		core = DefaultMzsCore.newInstance(port);
		URI spaceURI = core.getConfig().getSpaceUri();

		// initialize space
		Capi capi = new Capi(core);

		Map<String, Coordinator[]> containerSpecs = new HashMap<>();
		containerSpecs.put(MZSDistributorServiceConfig.CONTAINER_STOCK, new Coordinator[] { new FifoCoordinator(),
				new RandomCoordinator(), new TypeCoordinator(Arrays.asList(Watch.class)) });
		containerSpecs.put(MZSDistributorServiceConfig.CONTAINER_SOLD, new Coordinator[] { new FifoCoordinator(),
				new TypeCoordinator(Arrays.asList(Watch.class)) });

		Map<String, ContainerReference> containerRefs = new HashMap<>();
		containerSpecs.entrySet().forEach(
				item -> {
					String containerName = config.getString(item.getKey());
					try {
						containerRefs.put(
								item.getKey(),
								capi.createContainer(containerName, spaceURI, MzsConstants.Container.UNBOUNDED, null,
										item.getValue()));
						log.info(String.format("Created container \"%s\".", containerName));
					} catch (MzsCoreException e) {
						log.fatal(String.format("Failed to create container \"%s\".", containerName), e);
						System.exit(4);
					}
				});

		return new MZSDistributor(core);
	}

	@Override
	public void stopService() throws LogisticsServiceException {
		log.info("Service is shutting down ...");
		core.shutdown(true);
	}

	public void registerRuntimeShutDownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				try {
					MZSDistributorService.this.stopService();
				} catch (LogisticsServiceException e) {
					throw new RuntimeException(e);
				}
			}
		});
	}
}
