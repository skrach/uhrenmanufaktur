package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs;

import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mozartspaces.capi3.AnyCoordinator;
import org.mozartspaces.capi3.Coordinator;
import org.mozartspaces.capi3.FifoCoordinator;
import org.mozartspaces.capi3.KeyCoordinator;
import org.mozartspaces.capi3.LabelCoordinator;
import org.mozartspaces.capi3.QueryCoordinator;
import org.mozartspaces.capi3.TypeCoordinator;
import org.mozartspaces.core.Capi;
import org.mozartspaces.core.ContainerReference;
import org.mozartspaces.core.DefaultMzsCore;
import org.mozartspaces.core.Entry;
import org.mozartspaces.core.MzsConstants;
import org.mozartspaces.core.MzsCore;
import org.mozartspaces.core.MzsCoreException;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsServiceException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.FactoryService;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.IdType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Component;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.DistributorRegistration;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Order;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.OrderWorkItem;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Watch;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs.config.MZSFactoryServiceConfig;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs.item.IdCounter;

public class MZSFactoryService implements FactoryService {

	private static final Logger log = LogManager.getLogger(MZSFactoryService.class);

	private static MzsCore core;

	public static void main(String[] args) {
		if (args.length > 1) {
			log.fatal(String.format("Invalid number of arguments. Only one optional argument \"%s\" is expected.",
					MZSFactoryServiceConfig.SERVICE_LISTENINGPORT));
			System.exit(1);
		}

		String port = null;
		if (args.length == 1) {
			port = args[0];
		}

		try {
			(new MZSFactoryService()).startService(port);
		} catch (LogisticsServiceException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void startService(String parameter) throws LogisticsServiceException {
		// get configuration
		Configuration config = MZSFactoryServiceConfig.getConfiguration();

		int port = config.getInt(MZSFactoryServiceConfig.SERVICE_LISTENINGPORT);
		if (parameter != null) {
			try {
				port = Integer.parseInt(parameter);
				if (port < 1 || port > 65535) {
					log.fatal(String.format("\"%s\" is not in valid tcp port range.", parameter));
					System.exit(3);
				}
			} catch (NumberFormatException e) {
				log.fatal(String.format("\"%s\" is no valid integer.", parameter));
				System.exit(2);
			}
		}

		// start server
		core = DefaultMzsCore.newInstance(port);
		URI spaceURI = core.getConfig().getSpaceUri();

		// initialize space
		Capi capi = new Capi(core);

		Map<String, Coordinator[]> containerSpecs = new HashMap<>();
		containerSpecs.put(MZSFactoryServiceConfig.CONTAINER_COMPONENT, new Coordinator[] { new FifoCoordinator(),
				new TypeCoordinator(Arrays.asList(Component.class)), new LabelCoordinator() });
		containerSpecs.put(MZSFactoryServiceConfig.CONTAINER_ASSEMBLED, new Coordinator[] { new FifoCoordinator(),
				new TypeCoordinator(Arrays.asList(Watch.class)) });
		containerSpecs.put(MZSFactoryServiceConfig.CONTAINER_TESTED, new Coordinator[] { new FifoCoordinator(),
				new QueryCoordinator(), new TypeCoordinator(Arrays.asList(Watch.class)) });
		containerSpecs.put(MZSFactoryServiceConfig.CONTAINER_DELIVERY, new Coordinator[] { new FifoCoordinator(),
				new TypeCoordinator(Arrays.asList(Watch.class)), new LabelCoordinator() });
		containerSpecs.put(MZSFactoryServiceConfig.CONTAINER_DELIVERED, new Coordinator[] { new FifoCoordinator(),
				new TypeCoordinator(Arrays.asList(Watch.class)) });
		containerSpecs.put(MZSFactoryServiceConfig.CONTAINER_RECYCLED, new Coordinator[] { new FifoCoordinator(),
				new TypeCoordinator(Arrays.asList(Watch.class)) });
		containerSpecs.put(MZSFactoryServiceConfig.CONTAINER_ORDER, new Coordinator[] { new FifoCoordinator(),
				new QueryCoordinator(), new TypeCoordinator(Arrays.asList(Order.class)) });
		containerSpecs.put(MZSFactoryServiceConfig.CONTAINER_DISTRIBUTOR, new Coordinator[] { new FifoCoordinator(),
				new QueryCoordinator(), new TypeCoordinator(Arrays.asList(DistributorRegistration.class)) });
		containerSpecs.put(MZSFactoryServiceConfig.CONTAINER_IDCOUNTER, new Coordinator[] { new KeyCoordinator() });
		containerSpecs.put(MZSFactoryServiceConfig.CONTAINER_AMWORKITEM, new Coordinator[] { new FifoCoordinator(),
				new TypeCoordinator(Arrays.asList(DistributorRegistration.class, Watch.class)) });
		containerSpecs.put(MZSFactoryServiceConfig.CONTAINER_ORDERWORKITEM, new Coordinator[] { new FifoCoordinator(),
				new QueryCoordinator(), new TypeCoordinator(Arrays.asList(OrderWorkItem.class)), new LabelCoordinator() });
		containerSpecs.put(MZSFactoryServiceConfig.CONTAINER_BENCHMARK, new Coordinator[] {new AnyCoordinator()});

		Map<String, ContainerReference> containerRefs = new HashMap<>();
		containerSpecs.entrySet().forEach(
				item -> {
					String containerName = config.getString(item.getKey());
					try {
						containerRefs.put(
								item.getKey(),
								capi.createContainer(containerName, spaceURI, MzsConstants.Container.UNBOUNDED, null,
										item.getValue()));
						log.info(String.format("Created container \"%s\".", containerName));
					} catch (MzsCoreException e) {
						log.fatal(String.format("Failed to create container \"%s\".", containerName), e);
						System.exit(4);
					}
				});

		ContainerReference idCounterContainer = containerRefs.get(MZSFactoryServiceConfig.CONTAINER_IDCOUNTER);
		List<Entry> idCounterEntries = Stream.of(IdType.values())
				.map(e -> new Entry(new IdCounter(e.name()), KeyCoordinator.newCoordinationData(e.name())))
				.collect(Collectors.toList());

		try {
			capi.write(idCounterEntries, idCounterContainer, MzsConstants.RequestTimeout.TRY_ONCE, null);
			log.info(String.format("Created id counters \"%s\".",
					Stream.of(IdType.values()).map(IdType::name).collect(Collectors.joining(", "))));
		} catch (MzsCoreException e) {
			log.fatal("Failed initializing id counters.", e);
		}
	}

	@Override
	public void stopService() throws LogisticsServiceException {
		log.info("Service is shutting down ...");
		core.shutdown(true);
	}

	public void registerRuntimeShutDownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				try {
					MZSFactoryService.this.stopService();
				} catch (LogisticsServiceException e) {
					throw new RuntimeException(e);
				}
			}
		});
	}
}
