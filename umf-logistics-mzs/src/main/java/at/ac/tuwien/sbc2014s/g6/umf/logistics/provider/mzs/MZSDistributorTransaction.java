package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mozartspaces.capi3.RandomCoordinator;
import org.mozartspaces.capi3.Selector;
import org.mozartspaces.core.Capi;
import org.mozartspaces.core.ContainerReference;
import org.mozartspaces.core.Entry;
import org.mozartspaces.core.MzsConstants;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.WatchType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionTimeoutException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.distributor.DistributorTransaction;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Watch;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs.config.MZSDistributorConfig;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs.config.MZSProviderConfigUtil;

public class MZSDistributorTransaction extends AbstractMZSTransactionProvider implements DistributorTransaction {

	private final static Logger log = LogManager.getLogger(DistributorTransaction.class);

	public MZSDistributorTransaction(Capi capi, URI spaceURI, Map<ContainerId, ContainerReference> containerRefs,
			Configuration configuration) throws LogisticsTransactionException {
		super(capi, spaceURI, containerRefs, configuration, MZSProviderConfigUtil
				.decodeTransactionTimeoutProperty(configuration.getString(MZSDistributorConfig.TRANSACTIONTIMEOUT)));
	}

	@Override
	public void put(Watch w) throws LogisticsTransactionException, LogisticsTransactionTimeoutException {
		Entry e = new Entry(w);
		ContainerId cId = ContainerId.STOCK;
		this.put(e, cId, MzsConstants.RequestTimeout.DEFAULT);
	}

	@Override
	public WatchType sellWatch() throws LogisticsTransactionException, InterruptedException,
			LogisticsTransactionTimeoutException {
		ContainerId cId = ContainerId.STOCK;
		List<Selector> selectors = new ArrayList<>();
		selectors.add(RandomCoordinator.newSelector(1));

		log.debug(String.format("Trying to get random %s from container \"%s\".", Watch.class.getSimpleName(), cId.name()));

		Watch watch = this.takeOne(cId, selectors, MzsConstants.RequestTimeout.INFINITE);

		Entry e = new Entry(watch);
		cId = ContainerId.SOLD;
		this.put(e, cId, MzsConstants.RequestTimeout.DEFAULT);

		return WatchType.getWatchType(watch);
	}

}
