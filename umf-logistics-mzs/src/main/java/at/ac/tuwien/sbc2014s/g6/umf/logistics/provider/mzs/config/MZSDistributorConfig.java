package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs.config;

import org.apache.commons.configuration.Configuration;

import at.ac.tuwien.sbc2014s.g6.umf.util.Config;

public class MZSDistributorConfig {

	public static final String SERVICE_HOST = "Service.Host";
	public static final String SERVICE_PORT = "Service.Port";

	public static final String CONTAINER_STOCK = "Container.Stock";
	public static final String CONTAINER_SOLD = "Container.Sold";

	public static final String TRANSACTIONTIMEOUT = "TransactionTimeout";

	public static Configuration getConfiguration() {
		return Config.getConfig("mzsdistributor.properties");
	}

}
