package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs;

import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mozartspaces.core.DefaultMzsCore;
import org.mozartspaces.core.MzsCore;
import org.mozartspaces.core.MzsCoreRuntimeException;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.ItemUpdateCallback;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.distributor.Distributor;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.distributor.DistributorTransaction;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Item;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs.config.MZSDistributorConfig;

public class MZSDistributor extends AbstractMZSProvider implements Distributor {

	final static Logger log = LogManager.getLogger(MZSDistributorTransaction.class);

	final static Map<ContainerId, String> containerConfigProperties;

	static {
		Map<ContainerId, String> cPropertyNames = new HashMap<>();
		cPropertyNames.put(ContainerId.STOCK, MZSDistributorConfig.CONTAINER_STOCK);
		cPropertyNames.put(ContainerId.SOLD, MZSDistributorConfig.CONTAINER_SOLD);
		containerConfigProperties = Collections.unmodifiableMap(cPropertyNames);
	}

	public MZSDistributor(String args) {
		Configuration config = MZSDistributorConfig.getConfiguration();

		URI spaceUri;
		if (args == null || args.isEmpty()) {
			spaceUri = URI.create(String.format("xvsm://%s:%d", config.getString(MZSDistributorConfig.SERVICE_HOST),
					config.getInt(MZSDistributorConfig.SERVICE_PORT)));
		} else {
			spaceUri = URI.create(String.format("%s", args));
		}

		this.initialize(DefaultMzsCore.newInstanceWithoutSpace(), config, spaceUri, containerConfigProperties);
	}

	public MZSDistributor(MzsCore embeddedCore) {
		this.initialize(embeddedCore, MZSDistributorConfig.getConfiguration(), null, containerConfigProperties);
	}

	@Override
	public DistributorTransaction begin() throws LogisticsTransactionException, InterruptedException {
		try {
			return new MZSDistributorTransaction(this.getCapi(), (this.isEmbedded() ? null : this.getSpaceUri()),
					this.getContainerRefs(), this.getConfig());
		} catch (LogisticsException e) {
			String msg = "Failed to getting container references.";
			log.error(msg, e);
			throw new LogisticsTransactionException(msg, e);
		} catch (MzsCoreRuntimeException e) {
			if (e.getCause() instanceof InterruptedException) {
				throw (InterruptedException) e.getCause();
			}
			throw e;
		}
	}

	@Override
	public List<? extends Item> readDepotStock() throws LogisticsException {
		return this.readDepot(ContainerId.STOCK);
	}

	@Override
	public List<? extends Item> readDepotSold() throws LogisticsException {
		return this.readDepot(ContainerId.SOLD);
	}

	@Override
	public void subscribeToDepotStock(ItemUpdateCallback callback) {
		this.addSubscription(new Subscription(ContainerId.STOCK, callback));
	}

	@Override
	public void subscribeToDepotSold(ItemUpdateCallback callback) {
		this.addSubscription(new Subscription(ContainerId.SOLD, callback));
	}

	@Override
	public String getDistributorPsn() {
		return String.format("%s|%s", this.getClass().getName(), this.getSpaceUri());
	}

}
