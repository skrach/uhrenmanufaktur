package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs;

public enum ContainerId {
	/* Factory: */
	COMPONENT, ASSEMBLED, TESTED, DELIVERY, DELIVERED, RECYCLED, ORDER, DISTRIBUTOR, IDCOUNTER, AMWORKITEM, ORDERWORKITEM, BENCHMARK,
	/* Distributor: */
	STOCK, SOLD
}
