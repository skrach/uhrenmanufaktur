package at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mozartspaces.capi3.AnyCoordinator;
import org.mozartspaces.capi3.Selector;
import org.mozartspaces.core.ContainerReference;
import org.mozartspaces.core.DefaultMzsCore;
import org.mozartspaces.core.Entry;
import org.mozartspaces.core.MzsConstants;
import org.mozartspaces.core.MzsCoreException;
import org.mozartspaces.core.MzsCoreRuntimeException;
import org.mozartspaces.core.MzsTimeoutException;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.ItemUpdateCallback;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionTimeoutException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.Factory;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.FactoryTransaction;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.IdType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Casing;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Item;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.provider.mzs.config.MZSFactoryConfig;

public class MZSFactory extends AbstractMZSProvider implements Factory {

	final static Logger log = LogManager.getLogger(MZSFactoryTransaction.class);

	final static Map<ContainerId, String> containerConfigProperties;

	static {
		Map<ContainerId, String> cPropertyNames = new HashMap<>();
		cPropertyNames.put(ContainerId.COMPONENT, MZSFactoryConfig.CONTAINER_COMPONENT);
		cPropertyNames.put(ContainerId.ASSEMBLED, MZSFactoryConfig.CONTAINER_ASSEMBLED);
		cPropertyNames.put(ContainerId.TESTED, MZSFactoryConfig.CONTAINER_TESTED);
		cPropertyNames.put(ContainerId.DELIVERY, MZSFactoryConfig.CONTAINER_DELIVERY);
		cPropertyNames.put(ContainerId.DELIVERED, MZSFactoryConfig.CONTAINER_DELIVERED);
		cPropertyNames.put(ContainerId.RECYCLED, MZSFactoryConfig.CONTAINER_RECYCLED);
		cPropertyNames.put(ContainerId.ORDER, MZSFactoryConfig.CONTAINER_ORDER);
		cPropertyNames.put(ContainerId.DISTRIBUTOR, MZSFactoryConfig.CONTAINER_DISTRIBUTOR);
		cPropertyNames.put(ContainerId.IDCOUNTER, MZSFactoryConfig.CONTAINER_IDCOUNTER);
		cPropertyNames.put(ContainerId.AMWORKITEM, MZSFactoryConfig.CONTAINER_AMWORKITEM);
		cPropertyNames.put(ContainerId.ORDERWORKITEM, MZSFactoryConfig.CONTAINER_ORDERWORKITEM);
		cPropertyNames.put(ContainerId.BENCHMARK, MZSFactoryConfig.CONTAINER_BENCHMARK);
		containerConfigProperties = Collections.unmodifiableMap(cPropertyNames);
	}

	public MZSFactory(String args) {
		Configuration config = MZSFactoryConfig.getConfiguration();

		URI spaceUri;
		if (args == null || args.isEmpty()) {
			spaceUri = URI.create(String.format("xvsm://%s:%d", config.getString(MZSFactoryConfig.SERVICE_HOST),
					config.getInt(MZSFactoryConfig.SERVICE_PORT)));
		} else {
			spaceUri = URI.create(args);
		}

		this.initialize(DefaultMzsCore.newInstanceWithoutSpace(), config, spaceUri, containerConfigProperties);
	}

	@Override
	public FactoryTransaction begin() throws LogisticsTransactionException, InterruptedException {
		try {
			return new MZSFactoryTransaction(this.getCapi(), (this.isEmbedded() ? null : this.getSpaceUri()),
					this.getContainerRefs(), this.getConfig());
		} catch (LogisticsException e) {
			String msg = "Failed to getting container references.";
			log.error(msg, e);
			throw new LogisticsTransactionException(msg, e);
		} catch (MzsCoreRuntimeException e) {
			if (e.getCause() instanceof InterruptedException) {
				throw (InterruptedException) e.getCause();
			}
			throw e;
		}
	}

	@Override
	public List<? extends Item> readDepotComponent() throws LogisticsException {
		return this.readDepot(ContainerId.COMPONENT);
	}

	@Override
	public List<? extends Item> readDepotAssembled() throws LogisticsException {
		return this.readDepot(ContainerId.ASSEMBLED);
	}

	@Override
	public List<? extends Item> readDepotTested() throws LogisticsException {
		return this.readDepot(ContainerId.TESTED);
	}

	@Override
	public List<? extends Item> readDepotDelivery() throws LogisticsException {
		return this.readDepot(ContainerId.DELIVERY);
	}

	@Override
	public List<? extends Item> readDepotDelivered() throws LogisticsException {
		return this.readDepot(ContainerId.DELIVERED);
	}

	@Override
	public List<? extends Item> readDepotRecycled() throws LogisticsException {
		return this.readDepot(ContainerId.RECYCLED);
	}

	@Override
	public List<? extends Item> readDepotOrder() throws LogisticsException {
		return this.readDepot(ContainerId.ORDER);
	}

	@Override
	public List<? extends Item> readListDistributorRegistrations() throws LogisticsException {
		return this.readDepot(ContainerId.DISTRIBUTOR);
	}

	@Override
	public void subscribeToDepotComponent(ItemUpdateCallback callback) {
		this.addSubscription(new Subscription(ContainerId.COMPONENT, callback));
	}

	@Override
	public void subscribeToDepotAssembled(ItemUpdateCallback callback) {
		this.addSubscription(new Subscription(ContainerId.ASSEMBLED, callback));
	}

	@Override
	public void subscribeToDepotTested(ItemUpdateCallback callback) {
		this.addSubscription(new Subscription(ContainerId.TESTED, callback));
	}

	@Override
	public void subscribeToDepotDelivery(ItemUpdateCallback callback) {
		this.addSubscription(new Subscription(ContainerId.DELIVERY, callback));
	}

	@Override
	public void subscribeToDepotDelivered(ItemUpdateCallback callback) {
		this.addSubscription(new Subscription(ContainerId.DELIVERED, callback));
	}

	@Override
	public void subscribeToDepotRecycled(ItemUpdateCallback callback) {
		this.addSubscription(new Subscription(ContainerId.RECYCLED, callback));
	}

	@Override
	public void subscribeToDepotOrder(ItemUpdateCallback callback) {
		this.addSubscription(new Subscription(ContainerId.ORDER, callback));
	}

	@Override
	public void subscribeToListDistributorRegistrations(ItemUpdateCallback callback) {
		this.addSubscription(new Subscription(ContainerId.DISTRIBUTOR, callback));
	}

	@Override
	public String getNewId(IdType type) throws LogisticsException, InterruptedException {
		while (!Thread.interrupted()) {
			try {
				FactoryTransaction transaction = this.begin();
				String newId = transaction.getNewId(type);
				transaction.commit();
				return newId;
			} catch (LogisticsTransactionTimeoutException e) {
				String msg = String.format("Transaction for getting new id for id type \"%s\" timed out.", type.name());
				log.warn(msg);
			} catch (LogisticsTransactionException e) {
				String msg = String.format("Failed to get new id for id type \"%s\".", type.name());
				log.error(msg, e);
				throw new LogisticsException(msg, e);
			} catch (MzsCoreRuntimeException e) {
				if (e.getCause() instanceof InterruptedException) {
					throw (InterruptedException) e.getCause();
				}
				throw e;
			}
		}

		throw new InterruptedException(String.format("thread interrupted while getting new id for id type \"%s\".",
				type.name()));
	}

	@Override
	public void waitForBenchmarkSignal() {
		ContainerId cId = ContainerId.BENCHMARK;
		long requestTimeout = MzsConstants.RequestTimeout.INFINITE;
		List<Selector> selectors = new ArrayList<>();
		selectors.add(AnyCoordinator.newSelector(1));

		while (true) {
			try {
				try {
					ContainerReference cRef = this.getContainerRefs().get(cId);
					this.getCapi().read(cRef, selectors, requestTimeout, null);
					log.info("Benchmark start signalled.");
					return;
				} catch (MzsTimeoutException e) {
					log.info("Transaction timeout while waiting for benchmark start signal.");
					continue;
				}
			} catch (LogisticsException | MzsCoreException e) {
				throw new RuntimeException(e);
			}
		}
	}

	@Override
	public void signalBenchmarkStart() {
		Entry entry = new Entry(new Casing("BENCHMARK", "BENCHMARK"));
		ContainerId cId = ContainerId.BENCHMARK;

		try {
			this.getCapi().write(entry, this.getContainerRefs().get(cId), MzsConstants.RequestTimeout.INFINITE, null);
			log.info("Signalling Benchmark start!");
		} catch (MzsCoreException | LogisticsException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean isTransactionReusable() {
		return false;
	}
}
