package at.ac.tuwien.sbc2014s.g6.umf.util;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public class Config {

	public static Configuration getConfig(String propertyFile) {
		try {
			return new PropertiesConfiguration(propertyFile);
		} catch (ConfigurationException e) {
			throw new RuntimeException(e);
		}
	}

}
