package at.ac.tuwien.sbc2014s.g6.umf.util;

import java.util.Random;

public class RandomGenerator {

	private static Random rand = new Random();

	/**
	 * Returns a pseudo-random number between min and max, inclusive. The
	 * difference between min and max can be at most
	 * <code>Integer.MAX_VALUE - 1</code>.
	 *
	 * @param min
	 *            Minimum value
	 * @param max
	 *            Maximum value. Must be greater than min.
	 * @return Integer between min and max, inclusive.
	 * @see java.util.Random#nextInt(int)
	 * @see http
	 *      ://stackoverflow.com/questions/363681/generating-random-numbers-in
	 *      -a-range-with-java
	 */
	public static int rand(int min, int max) {
		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		int randomNum = rand.nextInt((max - min) + 1) + min;
		return randomNum;
	}

}
