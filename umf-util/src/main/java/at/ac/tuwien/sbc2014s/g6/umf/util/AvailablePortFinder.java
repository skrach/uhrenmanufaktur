package at.ac.tuwien.sbc2014s.g6.umf.util;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeSet;

/**
 * Finds currently available server ports.
 *
 * @author The Apache Directory Project (mina-dev@directory.apache.org)
 * @version $Rev: 555855 $
 * @see <a href="http://www.iana.org/assignments/port-numbers">IANA.org</a>
 */
public class AvailablePortFinder {
	/**
	 * The minimum number of server port number.
	 */
	public static final int MIN_PORT_NUMBER = 1024;

	/**
	 * The maximum number of server port number.
	 */
	public static final int MAX_PORT_NUMBER = 49151;

	/**
	 * Creates a new instance.
	 */
	private AvailablePortFinder() {
	}

	/**
	 * Returns the {@link Set} of currently available port numbers (
	 * {@link Integer}). This method is identical to
	 * <code>getAvailablePorts(MIN_PORT_NUMBER, MAX_PORT_NUMBER)</code>.
	 *
	 * WARNING: this can take a very long time.
	 */
	public static Set<Integer> getAvailablePorts() {
		return getAvailablePorts(MIN_PORT_NUMBER, MAX_PORT_NUMBER);
	}

	/**
	 * Gets the next available port starting at the lowest port number.
	 *
	 * @throws NoSuchElementException
	 *             if there are no ports available
	 */
	public static int getNextAvailable() {
		return getNextAvailable(MIN_PORT_NUMBER);
	}

	/**
	 * Gets the next available port starting at a port.
	 *
	 * @param fromPort
	 *            the port to scan for availability
	 * @throws NoSuchElementException
	 *             if there are no ports available
	 */
	public static int getNextAvailable(int fromPort) {
		if ((fromPort < MIN_PORT_NUMBER) || (fromPort > MAX_PORT_NUMBER)) {
			throw new IllegalArgumentException("Invalid start port: " + fromPort);
		}

		for (int i = fromPort; i <= MAX_PORT_NUMBER; i++) {
			if (available(i)) {
				return i;
			}
		}

		throw new NoSuchElementException("Could not find an available port above " + fromPort);
	}

	/**
	 * @see http://stackoverflow.com/questions/434718/sockets-discover-port-availability-using-java
	 * @param port
	 * @return
	 */
	public static boolean available(int port) {
		Socket s = null;
		try {
			s = new Socket("localhost", port);
			return false;
		} catch (IOException e) {
			return true;
		} finally {
			if (s != null) {
				try {
					s.close();
				} catch (IOException e) {
					throw new RuntimeException("You should handle this error.", e);
				}
			}
		}
	}

	/**
	 * Returns the {@link Set} of currently avaliable port numbers (
	 * {@link Integer}) between the specified port range.
	 *
	 * @throws IllegalArgumentException
	 *             if port range is not between {@link #MIN_PORT_NUMBER} and
	 *             {@link #MAX_PORT_NUMBER} or <code>fromPort</code> if greater
	 *             than <code>toPort</code>.
	 */
	public static Set<Integer> getAvailablePorts(int fromPort, int toPort) {
		if ((fromPort < MIN_PORT_NUMBER) || (toPort > MAX_PORT_NUMBER) || (fromPort > toPort)) {
			throw new IllegalArgumentException("Invalid port range: " + fromPort + " ~ " + toPort);
		}

		Set<Integer> result = new TreeSet<Integer>();

		for (int i = fromPort; i <= toPort; i++) {
			ServerSocket s = null;

			try {
				s = new ServerSocket(i);
				result.add(new Integer(i));
			} catch (IOException e) {
			} finally {
				if (s != null) {
					try {
						s.close();
					} catch (IOException e) {
						/* should not be thrown */
					}
				}
			}
		}

		return result;
	}
}
