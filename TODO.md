# JMS Logistics
- Think about replacing browse methods with a durable subscriber (to get all data by diff)
- takeWatch(int qualityRatingLowerBound, int qualityRatingUpperBound, int timeout) has to work with Constants.TIMEOUT_DEFAULT (default from config)
- read depot dauert ewig... also minuten

# ALL
- replace use of l4j2 by slf4j
- env.sh in einer windows version als cmd mit doskey bereitstellen
- es kommt immer wieder vor, dass clockworks verschwinden
- ganz selten kann es zu ConcurrentModificationExceptions ibeim Updaten der Supplier Treeview kommen
- statistic: zellen könnte man noch als property binden, anstatt den ganzen table immer neu zu erstellen machen

# MZS logistics
- Known issue: Es werden extrem viele TcpSockets erstellt, beispielsweise beim Logistics Worker wreden pro take,
  das dann ins request timeout läuft, werden 2 neue threads erstellt
- UI Update könnte man unter umständen immer dann starten, wenn neue notifications reingekommen sind, und nicht automatisch restarten

