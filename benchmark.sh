. env.sh

rm -f log.txt

set -e

if [ "$#" -ne 1 ]; then
	echo "Usage: benchmark.sh JMS|MZS"
	exit 1
fi

TYPE="$1"

echo "Building project..."
# mvn clean install

echo ""
echo "Ready"
echo "Please ensure you started the server"
read -p "Press [Enter] key to start benchmark..."

echo "Starting supplier..."

# produce resources
echo "Prodducing type CASING"
umf_worker_supplier $TYPE CASING 1500
echo "Prodducing type CLOCKWORK"
umf_worker_supplier $TYPE CLOCKWORK 1500
echo "Prodducing type HAND"
umf_worker_supplier $TYPE HAND 3750
echo "Prodducing type LEATHER_WRISTBAND"
umf_worker_supplier $TYPE LEATHER_WRISTBAND 750
echo "Prodducing type METAL_WRISTBAND"
umf_worker_supplier $TYPE METAL_WRISTBAND 750

while true
do
    sleep 10
    if [ "$(ps -ef | grep java| grep SupplierWorker)" == "" ]; then
        echo "Supplier finished!"
        break
    else    
        echo "Supplier still running..."
    fi
    
    sleep 5
done

echo ""
echo "Checking inventory..."
mvn -f umf-gui/pom.xml exec:java -Dexec.mainClass="at.ac.tuwien.sbc2014s.g6.umf.gui.UmfGui" -Dexec.args="dump"

echo ""
echo "Preparing workers..."


# start worker
umf_worker_assembly $TYPE
umf_worker_assembly $TYPE
umf_worker_qa $TYPE

umf_worker_logistics $TYPE A
umf_worker_logistics $TYPE B

echo "Waiting 30 seconds to let workers start up..."
sleep 30

read -p "Press [Enter] to signal benchmark start "

echo "Sending signal to start workers..."

umf_worker_benchmark $TYPE

echo "Workers started!"
echo "Waiting for 60 seconds"
sleep 60

# kill all workers
echo "Killing workers..."
ps -ef | grep java| grep Worker | awk -F ' ' '{print $2}'|xargs kill -9

echo "Collecting statistics..."
mvn -f umf-gui/pom.xml exec:java -Dexec.mainClass="at.ac.tuwien.sbc2014s.g6.umf.gui.UmfGui" -Dexec.args="dump"
echo "DONE"
