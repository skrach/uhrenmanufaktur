# Open Issues

(michi) - FacilityControl:
  -  Different icons for wristband types (componentImages)
  -  private class Order -> Support multiple wristbands (gui muss man sie auch ordern können)
  -  property files and die neuen config konstanten anpassen
  
# Logik Factory.takeNextDistributor()

Um die Uhren gleichmäßig auf die Distributoren zu verteilen, sind alle DistributorRegistration Objekte mit einem `lastWatchReceived` Date ausgestattet. Dieses wird wenn der Distributor eine Uhr erhält, mittels `DistributorRegistration.updateLastWatchReceived()` aktualisiert. Zum erhalten des nächsten Distributors, wird immer jener Distributor herangezogen, dessen `lastWatchReceived` Date am weitesten in der Vergangenheit liegt, und der eine Uhr diees Typs benötigt.

# frage: (THEMA assembly worker)
wir brauchen für die components auch noch ein read (ohne take), damit der assemblyworker feststellen kann was es für komponenten gibt.
das read wird eh in einer transaktion ausgeführt.
take macht er erst, wenn weiß, welche watch er herstellen soll...

# Watch Delivery

- put(Watch) erweitern so das Watch'es mit gesetzter distributor_id in's Depot `Delivered` kommen. 

# antwort:
jms kann bei read nicht locken innerhalb der session, wir führen die funktionen ein, damit die mzs read operation eingesetzt werden kann

FactoryTransaction.isComponentAvailable(ComponentType, int count()): boolean

jms take operationen für komponenten sollen ein in der config angegebenes timeout beim take berücksichtigen, und bei eintreten des timeouts eine LogisticsTransactionTimeoutException werfen (simultion des transaction timeouts das es beimzs gibt)

der assembly worker fragt zuerst ab ob:
 - gibt es ein lederarmband? (wenn ja, dann sagen wir, dass wir eine classic watch herstellen können)
 - gibt es ein metallarmband? (wenn ja, dann sagen wir, dass wir eine sportuhr herstellen können)
 - gibt es drei hands? (wenn ja, dann sagen wir, dass wir eine sportuhr tz herstellen können)
 - können wir zumindest eine uhr herstellen, fragen wir die elect funktion (MAGIC_A) was wir herstellen sollen
 - können wir momentan keine uhr herstellen, suchen wir uns randomized eine aus und machen ein take auf die komponenten in der folgenden reihenfolge:
	armband -> hand 1 2 3 -> casing und clockwork
 - sollte lang genug nichts kommen kommt das transaction timeout zu tragen, und es hgibt ein rollback


# TODO

zwei neue Queues / Spaces:
 - Orders (für Aufträge) (FIFO?, mit prioritäten und ordercound filterbar)
 - Distributors (für Großhändler, Round Robin(über fifo lösbar))
 

Also tolle magic function: MAGIC_A was soll ich herstellen, von dem was ich herstellen kann
input: welche modelle kann ich alle herstellen (zB 3 booleans)
output: stelle modell x her
verhalten:
1) select auftrag where prio=high and ( openOrderCountA > 0 OR openOrderCountB > 0 OR openOrderCountC > 0 ) je nachdem welche modelle hergestellt werdne können LIMIT 1
 bei mehreren aufträgen,. kommt immer der mit dem frühesten createdate zum tragen
2) if there is an auftrag der das satisfied, dann scuhe ich das modell aus, von dem noch am meisten in dem auftrag offen ist, bei gleichstand random
3) if there is no auftrag satisfying (1) dann das gleich mit prio = normal
4) if there is no auftrag satisfying (1-normal) dann das gleich mit prio = low
5) if there is no auftrag satisfying (1-low) dann choose random model das hergestellt werden kann
return model_enum
comment:
das abfragen der aufträge passiert in einer implizirten transaktion, es werden keine objekt entnommen oder gelockt, weil funktion hat nur steeruing aufgabe, der auftrag zu dem die uhr dann kommt, kann ein ganz anderer sein

wenn hergestellt, zweite tolle generalisierte function: uhr abgeben
input: habe fertige uhr. und sag dir, ob ich im letzten cycle einen auftrag erledigt habe, oder normale produktion gemacht hab
output: auftrag objekt zur info zu welchem auftrag diese uhr ginzugefügt wurde oder null, if normal production
1) so wie MAGIC_A, nur, dass schritt (4) nur dann durchgefpührt wird, wenn der worker sagtg, er hätte im letzten cycle normal produziert und nicht für einen auftrag
return: Auftrag zu dme die Uhr hinzugefügt wurde (uhrobjekt wurde mit auftragsid ergänzt) oder null, wenn die uhr ins normale delivery lager gekommen ist




Kundenbetreuer (AccountManager)
verhalten:
subscribes to changes on depot delivery 
immer ein event reinkommt, dann wird Logistics.getNextDistributor( WatchType ) aufgerufen. (das gibt den nächsten zu bedienenden Distributor aus, der noch uhren des angegebenen typs braucht)

subscribes to changes on depot distributors
immer ein event reinkommt, dann checke ob der neue distributor was braucht

(!) event items müssen extra su dem delivery container geholt werden, weil die als parameterübergebnene gehören ja nicht dem account manager


2 container / queues für distributor: Stock, Sold

# Done

neue items:
statt wristband -> LetherWristband und MetalWristband

neue Watches: (3 abgeleitet von Watch und Watch wird abstract)
ClassicWatch
SportWatch
SportWatchTZS

irgendwo außerhalb der watch klassen (zb logisrtics interface oder auftrag klasse)
enum für alle drei watch typen


Distributor interface im logistics package
 - die distributor implementierung wir direkt instaziert und bekommt einen logistics psn als parameter (und einen optionalen port für den broker oder space den der distributor auf macht)
 - put (Watch) throws Exception wenn watch not needed
 - readDepot
 - subscribeToDepotChange
 - sellWatch() -> schaufelt eine randomized watch aus depot stock nach sold und updates Distributor Entry in Facility Logistics

Großhändler (Distributor)
 - id (aus in psn angegebener distributor id space queue frickel sache)
 - psn
 - Hashmap Model_ENUM -> StockInfo
 - anzahl der auf lager liegenden uhren unterswchiedlicher modelle
 - boolean satisfied (wird bei update der stockinfo auf true gesetzt wenn beide ints gleich sind, sonst false)

 public inner class StockInfo
	int want to have so many please
	int already have so many, yeah!
	
Auftrag (Order)
 - id
 - prio {low normal high}
 - openOrderCountModelA
 - openOrderCountModelB
 - openOrderCountModelC
 - Hashmap Model_ENUM -> List Instances
 - getter für open order count via model_enum