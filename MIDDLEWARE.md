# Evaluierung

## Spaces Technologien

Beschreibung der Spaces Technologien, insbesondere

- Vorteile
- Nachteile

in Bezug auf die Aufgabenstellung

### Mozart Spaces
### Giga Spaces
### Java Spaces


## Alternativ Technologien

Beschreibung der alternativen Technologien, insbesondere

- Vorteile
- Nachteile

in Bezug auf die Aufgabenstellung

### Remote Method Invocation (RMI)
### Java Message Service (JMS)
### Sockets

# Ergebnis

Beschreibung der gewählten Technologien