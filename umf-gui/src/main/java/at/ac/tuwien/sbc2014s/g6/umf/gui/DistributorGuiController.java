package at.ac.tuwien.sbc2014s.g6.umf.gui;

import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import org.apache.commons.configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ac.tuwien.sbc2014s.g6.umf.gui.config.DistributorGuiConfig;

public class DistributorGuiController implements Initializable {

	private static final Logger log = LogManager.getLogger(DistributorGuiController.class);

	@FXML
	private HBox hbxMain;

	private final Set<DistributorControl> distributorControls = new HashSet<>();

	private String factoryPsn;

	private String distributorClassName;

	private Stage stage;

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		// intentionally left blank
	}

	Configuration config;

	public void setPsn(String factoryPsn, String distributorClassName) {
		this.config = DistributorGuiConfig.getConfiguration();

		if (factoryPsn == null) {
			factoryPsn = this.config.getString(DistributorGuiConfig.FACTORY_PSN);
		}
		this.factoryPsn = factoryPsn;

		if (distributorClassName == null) {
			distributorClassName = this.config.getString(DistributorGuiConfig.DISTRIBUTOR_SERVICECLASSNAME);
		}
		this.distributorClassName = distributorClassName;

		log.info(String.format("Factory PSN: \"%s\". Distributor implementation: \"%s\"", this.factoryPsn,
				this.distributorClassName));

		this.addDistributor();
	}

	private void addDistributor() {
		DistributorControl dControl = new DistributorControl(this.factoryPsn, this.distributorClassName,
				this.config.getLong(DistributorGuiConfig.DISTRIBUTOR_UIUPDATEINTERVAL));
		this.distributorControls.add(dControl);

		dControl.getIsStartedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (oldValue == false && newValue == true) {
					DistributorGuiController.this.addDistributor();
				}
				if (oldValue == true && newValue == false) {
					DistributorGuiController.this.distributorControls.remove(dControl);
					DistributorGuiController.this.hbxMain.getChildren().remove(dControl);
				}
			}
		});

		dControl.setTearDownHook(this.stage);

		this.hbxMain.getChildren().add(dControl);
	}

	public void setTearDownHook(Stage stage) {
		this.stage = stage;
	}
}
