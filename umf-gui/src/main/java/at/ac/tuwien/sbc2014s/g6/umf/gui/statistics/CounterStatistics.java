package at.ac.tuwien.sbc2014s.g6.umf.gui.statistics;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.Configuration;

import at.ac.tuwien.sbc2014s.g6.umf.gui.FactoryNotificationEntry;
import at.ac.tuwien.sbc2014s.g6.umf.gui.FactoryNotificationEntry.NotificationType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Casing;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Clockwork;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Hand;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.LeatherWristband;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.MetalWristband;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.RecycleableComponent;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Watch;
import at.ac.tuwien.sbc2014s.g6.umf.worker.WorkerConfig;

public class CounterStatistics implements Statistic {

	private int availableCasingCount = 0;
	private int availableClockworkCount = 0;
	private int availableHandCount = 0;
	private int availableLeatherWristbandCount = 0;
	private int availableMetalWristbandCount = 0;
	private int availableAssembledWatchCount = 0;
	private int availableTestedWatchCount = 0;
	private int deliveryWatchCount = 0;
	private int deliveredWatchCount = 0;
	private int deliveryClassAWatchCount = 0;
	private int deliveryClassBWatchCount = 0;
	private int recycledWatchCount = 0;
	private int orderCount = 0;
	private int distributorCount = 0;
	private int highestComponentRecycleCount = 0;
	private Instant earliestCreateDate = null;
	private Instant latestModificationDate = null;

	private final int classALowerBound;
	private final int classAUpperBound;
	private final int classBLowerBound;
	private final int classBUpperBound;

	public CounterStatistics() {
		super();
		Configuration workerConfig = WorkerConfig.getWorkerConfiguration();
		this.classALowerBound = workerConfig.getInt(WorkerConfig.QUALITYRATINGCLASS_A_LOWERBOUND);
		this.classAUpperBound = workerConfig.getInt(WorkerConfig.QUALITYRATINGCLASS_A_UPPERBOUND);
		this.classBLowerBound = workerConfig.getInt(WorkerConfig.QUALITYRATINGCLASS_B_LOWERBOUND);
		this.classBUpperBound = workerConfig.getInt(WorkerConfig.QUALITYRATINGCLASS_B_UPPERBOUND);
	}

	@Override
	public void process(List<FactoryNotificationEntry> notificationEntries) {
		for (FactoryNotificationEntry entry : notificationEntries) {
			switch (entry.getDepotId()) {
			case COMPONENTS:
				if (entry.getType() == NotificationType.TAKE) {
					entry.getItems().forEach(e -> {
						if (e instanceof Casing) {
							this.availableCasingCount--;
						} else if (e instanceof Clockwork) {
							this.availableClockworkCount--;
						} else if (e instanceof Hand) {
							this.availableHandCount--;
						} else if (e instanceof LeatherWristband) {
							this.availableLeatherWristbandCount--;
						} else if (e instanceof MetalWristband) {
							this.availableMetalWristbandCount--;
						}
					});
				} else {
					entry.getItems().forEach(e -> {
						if (e instanceof Casing) {
							this.availableCasingCount++;
						} else if (e instanceof Clockwork) {
							this.availableClockworkCount++;
						} else if (e instanceof Hand) {
							this.availableHandCount++;
						} else if (e instanceof LeatherWristband) {
							this.availableLeatherWristbandCount++;
						} else if (e instanceof MetalWristband) {
							this.availableMetalWristbandCount++;
						}
					});
				}

				break;
			case ASSEMBLED:
				if (entry.getType() == NotificationType.TAKE) {
					entry.getItems().forEach(e -> {
						this.availableAssembledWatchCount--;
					});
				} else {
					entry.getItems().forEach(e -> {
						this.availableAssembledWatchCount++;
						if (this.earliestCreateDate == null) {
							this.earliestCreateDate = e.getCreateDate();
						} else if (this.earliestCreateDate.compareTo(e.getCreateDate()) > 0) {
							this.earliestCreateDate = e.getCreateDate();
						}
					});
				}
				break;
			case TESTED:
				if (entry.getType() == NotificationType.TAKE) {
					entry.getItems().forEach(e -> {
						this.availableTestedWatchCount--;
					});
				} else {
					entry.getItems().forEach(e -> {
						this.availableTestedWatchCount++;
						if (this.earliestCreateDate == null) {
							this.earliestCreateDate = e.getCreateDate();
						} else if (this.earliestCreateDate.compareTo(e.getCreateDate()) > 0) {
							this.earliestCreateDate = e.getCreateDate();
						}
						Watch w = (Watch) e;
						if (this.latestModificationDate == null) {
							this.latestModificationDate = w.getModificationDate();
						} else if (this.latestModificationDate.compareTo(w.getModificationDate()) < 0) {
							this.latestModificationDate = w.getModificationDate();
						}
					});
				}
				break;
			case DELIVERY:
				if (entry.getType() == NotificationType.TAKE) {
					entry.getItems().forEach(e -> {
						this.deliveryWatchCount--;
						Watch w = (Watch) e;
						int qR = w.getQualityRating();
						if (qR <= this.classAUpperBound && qR >= this.classALowerBound) {
							this.deliveryClassAWatchCount--;
						} else if (qR <= this.classBUpperBound && qR >= this.classBLowerBound) {
							this.deliveryClassBWatchCount--;
						}
					});
				} else {
					entry.getItems().forEach(e -> {
						this.deliveryWatchCount++;
						if (this.earliestCreateDate == null) {
							this.earliestCreateDate = e.getCreateDate();
						} else if (this.earliestCreateDate.compareTo(e.getCreateDate()) > 0) {
							this.earliestCreateDate = e.getCreateDate();
						}
						Watch w = (Watch) e;
						if (this.latestModificationDate == null) {
							this.latestModificationDate = w.getModificationDate();
						} else if (this.latestModificationDate.compareTo(w.getModificationDate()) < 0) {
							this.latestModificationDate = w.getModificationDate();
						}
						int qR = w.getQualityRating();
						if (qR <= this.classAUpperBound && qR >= this.classALowerBound) {
							this.deliveryClassAWatchCount++;
						} else if (qR <= this.classBUpperBound && qR >= this.classBLowerBound) {
							this.deliveryClassBWatchCount++;
						}
						w.getComponents().forEach(c -> {
							if (c instanceof RecycleableComponent) {
								RecycleableComponent rC = (RecycleableComponent) c;
								if (rC.getRecycleCount() > this.highestComponentRecycleCount) {
									this.highestComponentRecycleCount = rC.getRecycleCount();
								}
							}
						});
					});
				}
				break;
			case DELIVERED:
				if (entry.getType() == NotificationType.TAKE) {
					entry.getItems().forEach(e -> {
						this.deliveredWatchCount--;
					});
				} else {
					entry.getItems().forEach(e -> {
						this.deliveredWatchCount++;
						if (this.earliestCreateDate == null) {
							this.earliestCreateDate = e.getCreateDate();
						} else if (this.earliestCreateDate.isAfter(e.getCreateDate())) {
							this.earliestCreateDate = e.getCreateDate();
						}
						Watch w = (Watch) e;
						if (this.latestModificationDate == null) {
							this.latestModificationDate = w.getModificationDate();
						} else if (this.latestModificationDate.isBefore(w.getModificationDate())) {
							this.latestModificationDate = w.getModificationDate();
						}
					});
				}
				break;
			case RECYCLED:
				if (entry.getType() == NotificationType.TAKE) {
					entry.getItems().forEach(e -> {
						this.recycledWatchCount--;
					});
				} else {
					entry.getItems().forEach(e -> {
						this.recycledWatchCount++;
						if (this.earliestCreateDate == null) {
							this.earliestCreateDate = e.getCreateDate();
						} else if (this.earliestCreateDate.compareTo(e.getCreateDate()) > 0) {
							this.earliestCreateDate = e.getCreateDate();
						}
						Watch w = (Watch) e;
						if (this.latestModificationDate == null) {
							this.latestModificationDate = w.getModificationDate();
						} else if (this.latestModificationDate.compareTo(w.getModificationDate()) < 0) {
							this.latestModificationDate = w.getModificationDate();
						}
					});
				}
				break;
			case ORDERS:
				if (entry.getType() == NotificationType.TAKE) {
					entry.getItems().forEach(e -> {
						this.orderCount--;
					});
				} else {
					entry.getItems().forEach(e -> {
						this.orderCount++;
					});
				}
				break;
			case DISTRIBUTORS:
				if (entry.getType() == NotificationType.TAKE) {
					entry.getItems().forEach(e -> {
						this.distributorCount--;
					});
				} else {
					entry.getItems().forEach(e -> {
						this.distributorCount++;
					});
				}
				break;
			}
		}
	}

	@Override
	public List<StatisticValue> getCurrentData() {
		List<StatisticValue> currentData = new ArrayList<>();
		currentData.add(new StatisticIntegerValue("availableCasingCount", this.availableCasingCount));
		currentData.add(new StatisticIntegerValue("availableClockworkCount", this.availableClockworkCount));
		currentData.add(new StatisticIntegerValue("availableHandCount", this.availableHandCount));
		currentData.add(new StatisticIntegerValue("availableLeatherWristbandCount", this.availableLeatherWristbandCount));
		currentData.add(new StatisticIntegerValue("availableMetalWristbandCount", this.availableMetalWristbandCount));
		currentData.add(new StatisticIntegerValue("availableAssembledWatchCount", this.availableAssembledWatchCount));
		currentData.add(new StatisticIntegerValue("availableTestedWatchCount", this.availableTestedWatchCount));

		currentData.add(new StatisticIntegerValue("deliveryWatchCount", this.deliveryWatchCount));
		currentData.add(new StatisticIntegerValue("deliveryClassAWatchCount", this.deliveryClassAWatchCount));
		currentData.add(new StatisticIntegerValue("deliveryClassBWatchCount", this.deliveryClassBWatchCount));

		int watchesTotal = this.deliveryWatchCount + this.recycledWatchCount;
		currentData.add(new StatisticPercentValue("percentClassAWatches",
				(watchesTotal > 0) ? (float) this.deliveryClassAWatchCount / watchesTotal : 0));
		currentData.add(new StatisticPercentValue("percentClassBWatches",
				(watchesTotal > 0) ? (float) this.deliveryClassBWatchCount / watchesTotal : 0));
		currentData.add(new StatisticPercentValue("percentRecycledWatches",
				(watchesTotal > 0) ? (float) this.recycledWatchCount / watchesTotal : 0));

		currentData.add(new StatisticIntegerValue("recycledWatchCount", this.recycledWatchCount));
		currentData.add(new StatisticIntegerValue("highestComponentRecycleCount", this.highestComponentRecycleCount));

		currentData.add(new StatisticIntegerValue("deliveredWatchCount", this.deliveredWatchCount));

		currentData.add(new StatisticIntegerValue("orderCount", this.orderCount));
		currentData.add(new StatisticIntegerValue("distributorCount", this.distributorCount));

		currentData.add(new StatisticInstantValue("earliestCreateDate", this.earliestCreateDate));
		currentData.add(new StatisticInstantValue("latestModificationDate", this.latestModificationDate));
		currentData.add(new StatisticFloatValue("deliveryWatchesPerSecond",
				(this.earliestCreateDate != null && this.latestModificationDate != null) ? (float) watchesTotal
						/ (Duration.between(this.earliestCreateDate, this.latestModificationDate).toMillis() / 1000) : 0));
		return currentData;
	}
}
