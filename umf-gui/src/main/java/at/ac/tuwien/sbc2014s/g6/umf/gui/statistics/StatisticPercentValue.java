package at.ac.tuwien.sbc2014s.g6.umf.gui.statistics;

public class StatisticPercentValue extends StatisticValue {

	private final float value;

	public float getValue() {
		return this.value;
	}

	public StatisticPercentValue(String name, float value) {
		super(name);
		this.value = value;
	}

	@Override
	public String toString() {
		return String.format("%.2f %%", this.value * 100);
	}

}
