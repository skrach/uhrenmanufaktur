package at.ac.tuwien.sbc2014s.g6.umf.gui;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controlsfx.dialog.Dialogs;

import at.ac.tuwien.sbc2014s.g6.umf.gui.config.UmfGuiConfig;

public class DistributorGui extends Application {

	private static final Logger log = LogManager.getLogger(DistributorGui.class);

	private static String factoryPsnArgument = null;

	private static String distributorClassNameArgument = null;

	static {
		Locale.setDefault(Locale.ENGLISH);
	}

	private static ResourceBundle bundle = ResourceBundle.getBundle("localization.distributor-gui", Locale.getDefault());

	public static ResourceBundle getBundle() {
		return bundle;
	}

	@Override
	public void start(Stage stage) throws Exception {
		log.info("JavaFX Runtime Version: " + System.getProperties().get("javafx.runtime.version"));

		try {
			URL location = this.getClass().getResource("/distributorgui.fxml");
			FXMLLoader fxmlLoader = new FXMLLoader();
			fxmlLoader.setLocation(location);
			fxmlLoader.setResources(getBundle());
			// fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());

			Parent root = fxmlLoader.load(location.openStream());
			Scene scene = new Scene(root);

			stage.setTitle(String.format("%s (%dms %s)", bundle.getString("applicationName"), UmfGuiConfig
					.getConfiguration().getLong(UmfGuiConfig.FACILITY_UIUPDATEINTERVAL), bundle
					.getString("title.updateInterval")));
			stage.getIcons().add(new Image("/images/distributor_16.png"));
			stage.getIcons().add(new Image("/images/distributor_32.png"));
			stage.getIcons().add(new Image("/images/distributor_48.png"));
			stage.getIcons().add(new Image("/images/distributor_64.png"));
			stage.getIcons().add(new Image("/images/distributor_128.png"));
			stage.setScene(scene);

			DistributorGuiController controller = fxmlLoader.getController();
			controller.setTearDownHook(stage);
			controller.setPsn(factoryPsnArgument, distributorClassNameArgument);
			stage.show();
		} catch (Exception e) {
			String msg = "A runtime exception occurred.";
			log.error(msg, e);
			Dialogs.create().title(bundle.getString("errormsg.error.title"))
					.masthead(bundle.getString("errormsg.error.masthead")).nativeTitleBar().showException(e);
		}
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) {
		if (args.length >= 1) {
			factoryPsnArgument = args[0];
		}

		if (args.length >= 2) {
			distributorClassNameArgument = args[1];
		}

		launch(args);
	}

}
