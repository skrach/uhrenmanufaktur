package at.ac.tuwien.sbc2014s.g6.umf.gui.statistics;

public abstract class StatisticValue {
	private final String name;

	public String getName() {
		return this.name;
	}

	public StatisticValue(String name) {
		super();
		this.name = name;
	}

}
