package at.ac.tuwien.sbc2014s.g6.umf.gui;

import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;

import org.apache.commons.configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ac.tuwien.sbc2014s.g6.umf.gui.config.UmfGuiConfig;

public class UmfGuiController implements Initializable {

	private static final Logger log = LogManager.getLogger(UmfGuiController.class);

	@FXML
	private TabPane tapMain;

	private final Set<FacilityControl> facilityControls = new HashSet<>();

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		Configuration config = UmfGuiConfig.getConfiguration();

		String[] facilityPSNs = config.getStringArray(UmfGuiConfig.FACILITY_PSNLIST);

		if (facilityPSNs.length == 0) {
			log.warn(String.format("No \"%s\" properties found in configuration file.", UmfGuiConfig.FACILITY_PSNLIST));
		}

		for (String psn : facilityPSNs) {
			FacilityControl fControl = new FacilityControl(psn, config.getLong(UmfGuiConfig.FACILITY_UIUPDATEINTERVAL));
			this.facilityControls.add(fControl);
			Tab tab = new Tab(psn);
			tab.setContent(fControl);
			this.tapMain.getTabs().add(tab);
		}
	}

	public void setTearDownHook(Stage stage) {
		this.facilityControls.forEach(e -> e.setTearDownHook(stage));
	}
}
