package at.ac.tuwien.sbc2014s.g6.umf.gui;

import java.util.Collections;
import java.util.List;

import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Item;

public class FactoryNotificationEntry {

	public enum DepotId {
		COMPONENTS, ASSEMBLED, TESTED, DELIVERY, DELIVERED, RECYCLED, ORDERS, DISTRIBUTORS
	}

	public enum NotificationType {
		PUT, TAKE
	}

	private final NotificationType type;
	private final DepotId depotId;
	private final List<? extends Item> items;

	public NotificationType getType() {
		return this.type;
	}

	public DepotId getDepotId() {
		return this.depotId;
	}

	public List<? extends Item> getItems() {
		return this.items;
	}

	public FactoryNotificationEntry(NotificationType type, DepotId depotId, List<? extends Item> items) {
		super();
		this.type = type;
		this.depotId = depotId;
		this.items = Collections.unmodifiableList(items);
	}
}