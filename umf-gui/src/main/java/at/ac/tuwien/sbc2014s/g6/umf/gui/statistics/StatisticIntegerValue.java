package at.ac.tuwien.sbc2014s.g6.umf.gui.statistics;

public class StatisticIntegerValue extends StatisticValue {

	private final int value;

	public int getValue() {
		return this.value;
	}

	public StatisticIntegerValue(String name, int value) {
		super(name);
		this.value = value;
	}

	@Override
	public String toString() {
		return String.format("%d", this.value);
	}

}
