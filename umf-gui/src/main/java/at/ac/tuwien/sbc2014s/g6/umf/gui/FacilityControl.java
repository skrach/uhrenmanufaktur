package at.ac.tuwien.sbc2014s.g6.umf.gui;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

import javafx.application.Platform;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import javafx.util.Duration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controlsfx.dialog.Dialogs;

import at.ac.tuwien.sbc2014s.g6.umf.gui.FactoryNotificationEntry.DepotId;
import at.ac.tuwien.sbc2014s.g6.umf.gui.FactoryNotificationEntry.NotificationType;
import at.ac.tuwien.sbc2014s.g6.umf.gui.statistics.CounterStatistics;
import at.ac.tuwien.sbc2014s.g6.umf.gui.statistics.Statistic;
import at.ac.tuwien.sbc2014s.g6.umf.gui.statistics.StatisticValue;
import at.ac.tuwien.sbc2014s.g6.umf.gui.worker.SupplierTaskWorker;
import at.ac.tuwien.sbc2014s.g6.umf.gui.worker.TaskWorker;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.ItemUpdateCallback;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.WatchType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LoadingProviderFailedException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionTimeoutException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.Factory;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.FactoryTransaction;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.IdType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Casing;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Clockwork;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Component;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.DistributorRegistration;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Hand;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Item;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.LeatherWristband;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.MetalWristband;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Order;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.RecycleableComponent;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Watch;
import at.ac.tuwien.sbc2014s.g6.umf.worker.supplier.ComponentType;
import at.ac.tuwien.sbc2014s.g6.umf.worker.supplier.SupplierWorker;

public class FacilityControl extends StackPane implements Initializable {

	private static final Logger log = LogManager.getLogger(FacilityControl.class);

	long uiUpdateInterval = 1000;

	// private static DateTimeFormatter formatter =
	// DateTimeFormatter.ofPattern("HH:mm:ss.SSS (yyyy-MM-dd)");

	private final Map<TreeItem<TreeItemObjectWrapper<?>>, Map<Object, TreeItem<TreeItemObjectWrapper<?>>>> treeItemObjectMap = new HashMap<>();
	private final Map<TreeItem<TreeItemObjectWrapper<?>>, Boolean> autoExpandTreeItem = new HashMap<>();

	private final List<SupplierTaskWorker> supplierTaskWorkers = new ArrayList<>();

	public final ConcurrentLinkedQueue<FactoryNotificationEntry> notifationQueue = new ConcurrentLinkedQueue<>();
	private ScheduledService<Void> uiUpdateService;
	private boolean uiUpdateRunning = false;

	public synchronized boolean isUiUpdateRunning() {
		return this.uiUpdateRunning;
	}

	public synchronized void setUiUpdateRunning(boolean uiUpdateRunning) {
		this.uiUpdateRunning = uiUpdateRunning;
	}

	private final List<Statistic> statistics = new ArrayList<>();

	@FXML
	private TreeView<TreeItemObjectWrapper<?>> trvComponents;
	private TreeItem<TreeItemObjectWrapper<?>> triComponents;
	@FXML
	private TitledPane tpnComponents;

	@FXML
	private TreeView<TreeItemObjectWrapper<?>> trvAssembled;
	private TreeItem<TreeItemObjectWrapper<?>> triAssembled;
	@FXML
	private TitledPane tpnAssembled;

	@FXML
	private TreeView<TreeItemObjectWrapper<?>> trvTested;
	private TreeItem<TreeItemObjectWrapper<?>> triTested;
	@FXML
	private TitledPane tpnTested;

	@FXML
	private TreeView<TreeItemObjectWrapper<?>> trvDelivery;
	private TreeItem<TreeItemObjectWrapper<?>> triDelivery;
	@FXML
	private TitledPane tpnDelivery;

	@FXML
	private TreeView<TreeItemObjectWrapper<?>> trvDelivered;
	private TreeItem<TreeItemObjectWrapper<?>> triDelivered;
	@FXML
	private TitledPane tpnDelivered;

	@FXML
	private TreeView<TreeItemObjectWrapper<?>> trvRecycled;
	private TreeItem<TreeItemObjectWrapper<?>> triRecycled;
	@FXML
	private TitledPane tpnRecycled;

	@FXML
	private TreeView<TreeItemObjectWrapper<?>> trvOrders;
	private TreeItem<TreeItemObjectWrapper<?>> triOrders;
	@FXML
	private TitledPane tpnOrders;

	@FXML
	private TreeView<TreeItemObjectWrapper<?>> trvSupplier;
	private TreeItem<TreeItemObjectWrapper<?>> triSupplier;
	@FXML
	private TitledPane tpnSupplier;

	@FXML
	private TreeView<TreeItemObjectWrapper<?>> trvPurchaseOrders;
	private TreeItem<TreeItemObjectWrapper<?>> triPurchaseOrders;
	@FXML
	private TitledPane tpnPurchaseOrders;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnProcess;

	@FXML
	private Button btnAdd;

	@FXML
	private Button btnStop;

	@FXML
	private Button btnClear;

	@FXML
	private Button btnCloseErrorView;

	@FXML
	private Button btnStartBenchmark;

	@FXML
	private TextField txfCasings;

	@FXML
	private TextField txfClockworks;

	@FXML
	private TextField txfHands;

	@FXML
	private TextField txfWristbandsL;

	@FXML
	private TextField txfWristbandsM;

	@FXML
	private TextField txfClassicWatch;

	@FXML
	private TextField txfSportsWatch;

	@FXML
	private TextField txfSportsWatchTZ;

	@FXML
	private ComboBox<Integer> cbxPriority;

	@FXML
	private ImageView imgArrowPurchase1;

	@FXML
	private ImageView imgArrowPurchase2;

	@FXML
	private ImageView imgArrowOrder1;

	@FXML
	private ImageView imgArrowOrder2;

	@FXML
	private TableView<StatisticValue> tbvStatistics;

	@FXML
	private TableColumn<StatisticValue, String> tclStatisticsName;

	@FXML
	private TableColumn<StatisticValue, String> tclStatisticsValue;

	@FXML
	private TabPane tapOrders;

	@FXML
	private Tab tabPurchase;

	@FXML
	private Tab tabOrder;

	@FXML
	private AnchorPane errorPane;

	@FXML
	private TextArea errorPaneTextArea;

	private final String psn;

	private Factory factory;

	private ResourceBundle bundle;

	/**
	 * Constructor that loads the fxml
	 *
	 * @param psn
	 *            Provider source name for facility
	 */
	public FacilityControl(String psn, long uiUpdateInterval) {
		this.psn = psn;
		this.uiUpdateInterval = uiUpdateInterval;

		// load component fxml
		FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/facility.fxml"), UmfGui.getBundle());
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		// initialize gui
		this.bundle = rb;
		this.errorPane.setVisible(false);

		this.statistics.add(new CounterStatistics());

		// initialize tbvStatistics columns
		this.tclStatisticsName
				.setCellValueFactory(new Callback<CellDataFeatures<StatisticValue, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(CellDataFeatures<StatisticValue, String> param) {
						return new ReadOnlyStringWrapper(FacilityControl.this.bundle.getString("statistics.name."
								+ param.getValue().getName()));
					}
				});

		this.tclStatisticsValue
				.setCellValueFactory(new Callback<CellDataFeatures<StatisticValue, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(CellDataFeatures<StatisticValue, String> param) {
						return new ReadOnlyStringWrapper(param.getValue().toString());
					}
				});

		// initialize TabPane
		this.tapOrders.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
			@Override
			public void changed(ObservableValue<? extends Tab> ov, Tab oldValue, Tab newValue) {
				if (newValue == FacilityControl.this.tabOrder) {
					FacilityControl.this.imgArrowPurchase1.setVisible(false);
					FacilityControl.this.imgArrowPurchase2.setVisible(false);
					FacilityControl.this.imgArrowOrder1.setVisible(true);
					FacilityControl.this.imgArrowOrder2.setVisible(true);
				} else {
					FacilityControl.this.imgArrowPurchase1.setVisible(true);
					FacilityControl.this.imgArrowPurchase2.setVisible(true);
					FacilityControl.this.imgArrowOrder1.setVisible(false);
					FacilityControl.this.imgArrowOrder2.setVisible(false);
				}
			}
		});

		this.cbxPriority.getItems().add(Order.PRIORITY_HIGH);
		this.cbxPriority.getItems().add(Order.PRIORITY_NORMAL);
		this.cbxPriority.getItems().add(Order.PRIORITY_LOW);
		this.cbxPriority.setCellFactory(new Callback<ListView<Integer>, ListCell<Integer>>() {
			@Override
			public ListCell<Integer> call(ListView<Integer> p) {
				return new ListCell<Integer>() {
					@Override
					protected void updateItem(Integer item, boolean empty) {
						super.updateItem(item, empty);

						if (item == null || empty) {
							this.setText(null);
						} else {
							this.setText(FacilityControl.this.bundle.getString("priority." + item));
						}
					}
				};
			}
		});
		this.cbxPriority.setButtonCell(new ListCell<Integer>() {
			@Override
			protected void updateItem(Integer item, boolean empty) {
				super.updateItem(item, empty);
				if (empty) {
					this.setText("");
				} else {
					this.setText(FacilityControl.this.bundle.getString("priority." + item));
				}

			}
		});
		this.cbxPriority.getSelectionModel().select(new Integer(Order.PRIORITY_NORMAL));

		// initialize TreeViews
		this.triComponents = new TreeItem<>();
		this.trvComponents.setRoot(this.triComponents);
		this.treeItemObjectMap.put(this.triComponents, new HashMap<>());
		this.autoExpandTreeItem.put(this.triComponents, false);

		this.triAssembled = new TreeItem<>();
		this.trvAssembled.setRoot(this.triAssembled);
		this.treeItemObjectMap.put(this.triAssembled, new HashMap<>());
		this.autoExpandTreeItem.put(this.triAssembled, false);

		this.triTested = new TreeItem<>();
		this.trvTested.setRoot(this.triTested);
		this.treeItemObjectMap.put(this.triTested, new HashMap<>());
		this.autoExpandTreeItem.put(this.triTested, false);

		this.triDelivery = new TreeItem<>();
		this.triDelivery.setExpanded(false);
		this.trvDelivery.setRoot(this.triDelivery);
		this.treeItemObjectMap.put(this.triDelivery, new HashMap<>());
		this.autoExpandTreeItem.put(this.triDelivery, false);

		this.triDelivered = new TreeItem<>();
		this.triDelivered.setExpanded(false);
		this.trvDelivered.setRoot(this.triDelivered);
		this.treeItemObjectMap.put(this.triDelivered, new HashMap<>());
		this.autoExpandTreeItem.put(this.triDelivered, false);

		this.triRecycled = new TreeItem<>();
		this.trvRecycled.setRoot(this.triRecycled);
		this.treeItemObjectMap.put(this.triRecycled, new HashMap<>());
		this.autoExpandTreeItem.put(this.triRecycled, false);

		this.triOrders = new TreeItem<>();
		this.trvOrders.setRoot(this.triOrders);
		this.treeItemObjectMap.put(this.triOrders, new HashMap<>());
		this.autoExpandTreeItem.put(this.triOrders, false);

		this.triSupplier = new TreeItem<>();
		this.trvSupplier.setRoot(this.triSupplier);
		this.autoExpandTreeItem.put(this.triSupplier, true);
		this.triSupplier.getChildren().addListener(new ListChangeListener<TreeItem<TreeItemObjectWrapper<?>>>() {
			@Override
			public void onChanged(Change<? extends TreeItem<TreeItemObjectWrapper<?>>> c) {
				String title = FacilityControl.this.bundle.getString("facility.title.supplier");
				if (FacilityControl.this.triSupplier.getChildren().isEmpty()) {
					FacilityControl.this.tpnSupplier.setText(title);
					FacilityControl.this.btnClear.setDisable(true);
					FacilityControl.this.btnStop.setDisable(true);
				} else {
					FacilityControl.this.tpnSupplier.setText(String.format("%s (%d)", title,
							FacilityControl.this.triSupplier.getChildren().size()));
				}
			}
		});
		this.trvSupplier.getSelectionModel().selectedItemProperty()
				.addListener(new ChangeListener<TreeItem<TreeItemObjectWrapper<?>>>() {
					@Override
					public void changed(ObservableValue<? extends TreeItem<TreeItemObjectWrapper<?>>> value,
							TreeItem<TreeItemObjectWrapper<?>> oldValue, TreeItem<TreeItemObjectWrapper<?>> newValue) {
						if (newValue != null && newValue.getParent() == FacilityControl.this.triSupplier) {
							FacilityControl.this.btnStop.setDisable(false);
						} else {
							FacilityControl.this.btnStop.setDisable(true);
						}
					}
				});

		this.triPurchaseOrders = new TreeItem<>();
		this.trvPurchaseOrders.setRoot(this.triPurchaseOrders);
		this.autoExpandTreeItem.put(this.triPurchaseOrders, true);
		this.triPurchaseOrders.getChildren().addListener(new ListChangeListener<TreeItem<TreeItemObjectWrapper<?>>>() {
			@Override
			public void onChanged(Change<? extends TreeItem<TreeItemObjectWrapper<?>>> c) {
				String title = FacilityControl.this.bundle.getString("facility.title.purchaseorders");
				if (FacilityControl.this.triPurchaseOrders.getChildren().isEmpty()) {
					FacilityControl.this.btnProcess.setDisable(true);
					FacilityControl.this.btnDelete.setDisable(true);
					FacilityControl.this.tpnPurchaseOrders.setText(title);
				} else {
					FacilityControl.this.btnProcess.setDisable(false);
					FacilityControl.this.tpnPurchaseOrders.setText(String.format("%s (%d)", title,
							FacilityControl.this.triPurchaseOrders.getChildren().size()));
				}
			}
		});
		this.trvPurchaseOrders.getSelectionModel().selectedItemProperty()
				.addListener(new ChangeListener<TreeItem<TreeItemObjectWrapper<?>>>() {
					@Override
					public void changed(ObservableValue<? extends TreeItem<TreeItemObjectWrapper<?>>> value,
							TreeItem<TreeItemObjectWrapper<?>> oldValue, TreeItem<TreeItemObjectWrapper<?>> newValue) {
						if (newValue != null && newValue.getParent() == FacilityControl.this.triPurchaseOrders) {
							FacilityControl.this.btnDelete.setDisable(false);
						} else {
							FacilityControl.this.btnDelete.setDisable(true);
						}
					}
				});

		// initialize factory binding
		try {
			this.factory = Factory.createFactory(this.psn);
		} catch (LoadingProviderFailedException e) {
			String msg = String.format("Failed loading factory provider with psn \"%s\".", this.psn);
			log.fatal(msg, e);
			this.showError(msg, e);
			return;
		}

		// initialize notification callbacks
		this.factory.subscribeToDepotComponent(new NotificationCallback(DepotId.COMPONENTS));
		this.factory.subscribeToDepotAssembled(new NotificationCallback(DepotId.ASSEMBLED));
		this.factory.subscribeToDepotTested(new NotificationCallback(DepotId.TESTED));
		this.factory.subscribeToDepotDelivery(new NotificationCallback(DepotId.DELIVERY));
		this.factory.subscribeToDepotDelivered(new NotificationCallback(DepotId.DELIVERED));
		this.factory.subscribeToDepotRecycled(new NotificationCallback(DepotId.RECYCLED));
		this.factory.subscribeToDepotOrder(new NotificationCallback(DepotId.ORDERS));
		this.factory.subscribeToListDistributorRegistrations(new NotificationCallback(DepotId.DISTRIBUTORS));

		// start factory interface
		try {
			this.factory.start();
		} catch (LogisticsException e) {
			String msg = "Error starting factory interface subscriptions.";
			log.fatal(msg, e);
			this.showError(msg, e);
		}

		// read initial depot contents
		try {
			log.info("Reading current depot information...");
			this.notifationQueue.add(new FactoryNotificationEntry(NotificationType.PUT, DepotId.COMPONENTS, this.factory
					.readDepotComponent()));
			this.notifationQueue.add(new FactoryNotificationEntry(NotificationType.PUT, DepotId.ASSEMBLED, this.factory
					.readDepotAssembled()));
			this.notifationQueue.add(new FactoryNotificationEntry(NotificationType.PUT, DepotId.TESTED, this.factory
					.readDepotTested()));
			this.notifationQueue.add(new FactoryNotificationEntry(NotificationType.PUT, DepotId.DELIVERY, this.factory
					.readDepotDelivery()));
			this.notifationQueue.add(new FactoryNotificationEntry(NotificationType.PUT, DepotId.DELIVERED, this.factory
					.readDepotDelivered()));
			this.notifationQueue.add(new FactoryNotificationEntry(NotificationType.PUT, DepotId.RECYCLED, this.factory
					.readDepotRecycled()));
			this.notifationQueue.add(new FactoryNotificationEntry(NotificationType.PUT, DepotId.ORDERS, this.factory
					.readDepotOrder()));
			this.notifationQueue.add(new FactoryNotificationEntry(NotificationType.PUT, DepotId.DISTRIBUTORS, this.factory
					.readListDistributorRegistrations()));
			log.info("Read current depot information.");
		} catch (LogisticsException e) {
			String msg = "Failed loading depot information.";
			log.error(msg, e);
			this.showError(msg, e);
			return;
		}

		// initialize ui update service
		this.uiUpdateService = new ScheduledService<Void>() {
			@Override
			protected Task<Void> createTask() {
				return new NotificationProcessor();
			}
		};
		this.uiUpdateService.setPeriod(Duration.millis(this.uiUpdateInterval));
		this.uiUpdateService.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			@Override
			public void handle(WorkerStateEvent event) {
				// update depot titles
				String title;

				title = FacilityControl.this.bundle.getString("facility.title.components");
				if (FacilityControl.this.triComponents.getChildren().isEmpty()) {
					FacilityControl.this.tpnComponents.setText(title);
				} else {
					FacilityControl.this.tpnComponents.setText(String.format("%s (%d)", title,
							FacilityControl.this.triComponents.getChildren().size()));
				}

				title = FacilityControl.this.bundle.getString("facility.title.assembled");
				if (FacilityControl.this.triAssembled.getChildren().isEmpty()) {
					FacilityControl.this.tpnAssembled.setText(title);
				} else {
					FacilityControl.this.tpnAssembled.setText(String.format("%s (%d)", title,
							FacilityControl.this.triAssembled.getChildren().size()));
				}

				title = FacilityControl.this.bundle.getString("facility.title.tested");
				if (FacilityControl.this.triTested.getChildren().isEmpty()) {
					FacilityControl.this.tpnTested.setText(title);
				} else {
					FacilityControl.this.tpnTested.setText(String.format("%s (%d)", title, FacilityControl.this.triTested
							.getChildren().size()));
				}

				title = FacilityControl.this.bundle.getString("facility.title.delivery");
				if (FacilityControl.this.triDelivery.getChildren().isEmpty()) {
					FacilityControl.this.tpnDelivery.setText(title);
				} else {
					FacilityControl.this.tpnDelivery.setText(String.format("%s (%d)", title,
							FacilityControl.this.triDelivery.getChildren().size()));
				}

				title = FacilityControl.this.bundle.getString("facility.title.delivered");
				if (FacilityControl.this.triDelivered.getChildren().isEmpty()) {
					FacilityControl.this.tpnDelivered.setText(title);
				} else {
					FacilityControl.this.tpnDelivered.setText(String.format("%s (%d)", title,
							FacilityControl.this.triDelivered.getChildren().size()));
				}

				title = FacilityControl.this.bundle.getString("facility.title.recycled");
				if (FacilityControl.this.triRecycled.getChildren().isEmpty()) {
					FacilityControl.this.tpnRecycled.setText(title);
				} else {
					FacilityControl.this.tpnRecycled.setText(String.format("%s (%d)", title,
							FacilityControl.this.triRecycled.getChildren().size()));
				}

				title = FacilityControl.this.bundle.getString("facility.title.orders");
				if (FacilityControl.this.triOrders.getChildren().isEmpty()) {
					FacilityControl.this.tpnOrders.setText(title);
				} else {
					FacilityControl.this.tpnOrders.setText(String.format("%s (%d)", title, FacilityControl.this.triOrders
							.getChildren().size()));
				}

				// update statistics table
				FacilityControl.this.tbvStatistics.getItems().clear();
				FacilityControl.this.statistics.forEach(e -> FacilityControl.this.tbvStatistics.getItems().addAll(
						e.getCurrentData()));
			}
		});

		this.uiUpdateService.start();
	}

	public void setTearDownHook(Stage stage) {
		if (stage == null) {
			if (this.getScene() == null) {
				throw new IllegalArgumentException("Cannot determine scene.");
			}
			stage = ((Stage) this.getScene().getWindow());
		}

		final EventHandler<WindowEvent> prevCloseHandler = stage.getOnCloseRequest();
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				if (prevCloseHandler != null) {
					prevCloseHandler.handle(event);
				}
				FacilityControl.this.tearDown();
			}
		});

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				FacilityControl.this.tearDown();
			}
		});
	}

	private void tearDown() {
		log.info(String.format("Tear down of %s (%s) ...", this.getClass().getName(), this.psn));

		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				if (FacilityControl.this.uiUpdateService != null) {
					FacilityControl.this.uiUpdateService.cancel();
				}
			}
		});

		this.supplierTaskWorkers.forEach(e -> {
			if (e.getTask().cancel()) {
				log.info(String.format("Canceled %s.", e.getTask()));
			} else {
				log.info(String.format("Canceling of %s failed.", e.getTask()));
			}
		});

		if (this.factory != null) {
			try {
				this.factory.stop();
			} catch (LogisticsException e) {
				String msg = "Failed stopping factory interface.";
				log.fatal(msg, e);
				throw new RuntimeException(msg, e);
			}
		}
	}

	private void showError(String msg, Throwable e) {
		StringWriter stringWriter = new StringWriter();
		e.printStackTrace(new PrintWriter(stringWriter));
		this.errorPaneTextArea.setText(msg + "\n\n" + stringWriter.toString() + "\n\n" + this.errorPaneTextArea.getText());
		this.errorPane.setVisible(true);
	}

	@FXML
	private void btnCloseErrorViewAction() {
		this.errorPane.setVisible(false);
	}

	@FXML
	private void btnStartBenchmarkAction() {
		this.factory.signalBenchmarkStart();
	}

	@FXML
	private void btnOpenWorkerUtilAction() {
		Stage stage = new Stage();
		stage.initStyle(StageStyle.DECORATED);
		stage.initModality(Modality.NONE);
		stage.setResizable(true);
		stage.setTitle(String.format("%s (%s)", this.bundle.getString("facility.title.workerUtil"), this.psn));
		stage.setWidth(800);
		stage.setHeight(400);
		stage.getIcons().addAll(Images.workerUtilImages);

		WorkerUtilityControl workerUtil = new WorkerUtilityControl(this.psn);
		Scene scene = new Scene(workerUtil);
		stage.setScene(scene);
		workerUtil.setTearDownHook(stage);
		stage.show();
	}

	@FXML
	private void btnAddAction() {
		if (this.tapOrders.getSelectionModel().getSelectedItem() == this.tabOrder) {
			this.addOrder();
		} else {
			this.addPurchaseOrder();
		}
	}

	private void addOrder() {
		List<String> errors = new ArrayList<>();
		boolean containsValidOrder = false;

		int classicCount = 0;
		if (!this.txfClassicWatch.getText().trim().isEmpty()) {
			try {
				classicCount = Integer.parseInt(this.txfClassicWatch.getText());
				if (classicCount < 0) {
					errors.add(this.bundle.getString("errormsg.classicWatchCount.range"));
				}
				if (classicCount > 0) {
					containsValidOrder = true;
				}
			} catch (NumberFormatException e) {
				errors.add(this.bundle.getString("errormsg.classicWatchCount.format"));
			}
		}

		int sportsCount = 0;
		if (!this.txfSportsWatch.getText().trim().isEmpty()) {
			try {
				sportsCount = Integer.parseInt(this.txfSportsWatch.getText());
				if (sportsCount < 0) {
					errors.add(this.bundle.getString("errormsg.sportsWatchCount.range"));
				}
				if (sportsCount > 0) {
					containsValidOrder = true;
				}
			} catch (NumberFormatException e) {
				errors.add(this.bundle.getString("errormsg.sportsWatchCount.format"));
			}
		}

		int sportsTzCount = 0;
		if (!this.txfSportsWatchTZ.getText().trim().isEmpty()) {
			try {
				sportsTzCount = Integer.parseInt(this.txfSportsWatchTZ.getText());
				if (sportsTzCount < 0) {
					errors.add(this.bundle.getString("errormsg.sportsTzWatchCount.range"));
				}
				if (sportsTzCount > 0) {
					containsValidOrder = true;
				}
			} catch (NumberFormatException e) {
				errors.add(this.bundle.getString("errormsg.sportsTzWatchCount.format"));
			}
		}

		if (errors.isEmpty()) {
			if (containsValidOrder) {
				// add order to factory
				FactoryTransaction transaction = null;
				try {
					transaction = this.factory.begin();

					Order order = new Order(transaction.getNewId(IdType.ITEM_ORDER), this.cbxPriority.getSelectionModel()
							.getSelectedItem(), classicCount, sportsCount, sportsTzCount);

					transaction.putNewOrder(order);
					transaction.commit();
				} catch (LogisticsTransactionException | LogisticsTransactionTimeoutException | InterruptedException e) {
					log.error("Error creating order.", e);
					Dialogs.create().title(this.bundle.getString("errormsg.error.title"))
							.masthead(this.bundle.getString("errormsg.error.masthead")).nativeTitleBar().showException(e);
				}
			}
		} else {
			Dialogs.create().title(this.bundle.getString("errormsg.inputError.title"))
					.masthead(this.bundle.getString("errormsg.inputError.masthead"))
					.message(errors.stream().collect(Collectors.joining("\n"))).nativeTitleBar().showWarning();
		}
	}

	private void addPurchaseOrder() {
		List<String> errors = new ArrayList<>();
		boolean containsValidOrder = false;

		int casingCount = 0;
		if (!this.txfCasings.getText().trim().isEmpty()) {
			try {
				casingCount = Integer.parseInt(this.txfCasings.getText());
				if (casingCount < 0) {
					errors.add(this.bundle.getString("errormsg.casingCount.range"));
				}
				if (casingCount > 0) {
					containsValidOrder = true;
				}
			} catch (NumberFormatException e) {
				errors.add(this.bundle.getString("errormsg.casingCount.format"));
			}
		}

		int clockworkCount = 0;
		if (!this.txfClockworks.getText().trim().isEmpty()) {
			try {
				clockworkCount = Integer.parseInt(this.txfClockworks.getText());
				if (clockworkCount < 0) {
					errors.add(this.bundle.getString("errormsg.clockworkCount.range"));
				}
				if (clockworkCount > 0) {
					containsValidOrder = true;
				}
			} catch (NumberFormatException e) {
				errors.add(this.bundle.getString("errormsg.clockworkCount.format"));
			}
		}

		int handCount = 0;
		if (!this.txfHands.getText().trim().isEmpty()) {
			try {
				handCount = Integer.parseInt(this.txfHands.getText());
				if (handCount < 0) {
					errors.add(this.bundle.getString("errormsg.handCount.range"));
				}
				if (handCount > 0) {
					containsValidOrder = true;
				}
			} catch (NumberFormatException e) {
				errors.add(this.bundle.getString("errormsg.handCount.format"));
			}
		}

		int wristbandLCount = 0;
		if (!this.txfWristbandsL.getText().trim().isEmpty()) {
			try {
				wristbandLCount = Integer.parseInt(this.txfWristbandsL.getText());
				if (wristbandLCount < 0) {
					errors.add(this.bundle.getString("errormsg.wristbandLCount.range"));
				}
				if (wristbandLCount > 0) {
					containsValidOrder = true;
				}
			} catch (NumberFormatException e) {
				errors.add(this.bundle.getString("errormsg.wristbandLCount.format"));
			}
		}

		int wristbandMCount = 0;
		if (!this.txfWristbandsM.getText().trim().isEmpty()) {
			try {
				wristbandMCount = Integer.parseInt(this.txfWristbandsM.getText());
				if (wristbandMCount < 0) {
					errors.add(this.bundle.getString("errormsg.wristbandMCount.range"));
				}
				if (wristbandMCount > 0) {
					containsValidOrder = true;
				}
			} catch (NumberFormatException e) {
				errors.add(this.bundle.getString("errormsg.wristbandMCount.format"));
			}
		}

		if (errors.isEmpty()) {
			if (containsValidOrder) {
				PurchaseOrder order = new PurchaseOrder(casingCount, clockworkCount, handCount, wristbandLCount,
						wristbandMCount);
				this.addTreeItemNode(this.triPurchaseOrders, order);
			}
		} else {
			Dialogs.create().title(this.bundle.getString("errormsg.inputError.title"))
					.masthead(this.bundle.getString("errormsg.inputError.masthead"))
					.message(errors.stream().collect(Collectors.joining("\n"))).nativeTitleBar().showWarning();
		}
	}

	@FXML
	private void btnDeleteAction() {
		this.triPurchaseOrders.getChildren().remove(this.trvPurchaseOrders.getSelectionModel().getSelectedItem());
		this.btnDelete.setDisable(true);
	}

	@FXML
	private void btnProcessAction() {
		this.triPurchaseOrders.getChildren().removeIf(e -> {
			PurchaseOrder order = (PurchaseOrder) e.getValue().getValue();
			order.orderAmounts.forEach((t, a) -> {
				if (a < 1) {
					return;
				}

				final SupplierTaskWorker supplier = new SupplierTaskWorker(t, a, this.bundle);

				Task<Void> task = new Task<Void>() {
					@Override
					protected Void call() throws Exception {
						SupplierWorker worker = new SupplierWorker(FacilityControl.this.psn, t, a) {
							@Override
							protected void infoProgress(int progress, Integer all) {
								super.infoProgress(progress, all);
								updateProgress(progress, all);
							}

							@Override
							protected void infoMessage(String msg) {
								super.infoMessage(msg);
								updateMessage(msg);
							}

							@Override
							protected void infoSuccess(String msg) {
								super.infoSuccess(msg);
								updateMessage(msg);
							}

							@Override
							protected void infoFailure(String msg, Throwable e) {
								super.infoFailure(msg, e);
								updateMessage(msg);
							}

							@Override
							public void setWorkerId(String workerId) {
								super.setWorkerId(workerId);
								Platform.runLater(new Runnable() {
									@Override
									public void run() {
										supplier.getWorkerIdProperty().setValue(workerId);
									}
								});
							}
						};

						try {
							worker.run();
						} catch (Exception e) {
							log.fatal("Exception thrown in worker thread.", e);
						}

						return null;
					}
				};

				task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
					@Override
					public void handle(WorkerStateEvent event) {
						FacilityControl.this.btnClear.setDisable(false);
					}
				});

				supplier.setTask(task);
				this.addTreeItemNode(this.triSupplier, supplier);

				Thread th = new Thread(task);
				th.setDaemon(true);
				th.start();
			});
			return true;
		});
	}

	@FXML
	private void btnClearAction() {
		this.supplierTaskWorkers.removeIf(e -> {
			if (e.getTask().getState() == Worker.State.SUCCEEDED) {
				this.triSupplier.getChildren().remove(e.getTreeItem());
				return true;
			}
			return false;
		});

		this.btnClear.setDisable(true);
	}

	@FXML
	private void btnStopAction() {
		TreeItem<TreeItemObjectWrapper<?>> treeItem = this.trvSupplier.getSelectionModel().getSelectedItem();
		((TaskWorker) treeItem.getValue().getValue()).getTask().cancel(true);
		this.triSupplier.getChildren().remove(treeItem);
		this.btnStop.setDisable(true);
	}

	private TreeItem<TreeItemObjectWrapper<?>> addTreeItemNode(TreeItem<TreeItemObjectWrapper<?>> root, PurchaseOrder value) {
		Boolean autoExpand = this.autoExpandTreeItem.get(root);
		if (autoExpand == null) {
			autoExpand = root.isExpanded();
		}

		TreeItem<TreeItemObjectWrapper<?>> valueNode = new TreeItem<>(new TreeItemObjectWrapper<PurchaseOrder>(value),
				new ImageView(Images.iconOrder));
		valueNode.setExpanded(autoExpand);

		value.orderAmounts.forEach((t, a) -> {
			if (a > 0) {
				valueNode.getChildren().add(
						new TreeItem<>(new TreeItemObjectWrapper<String>(String.format("%s: %d",
								this.bundle.getString("supplier.type." + t.name().toLowerCase()), a)), new ImageView(
								Images.componentImages.get(t))));
			}
		});

		root.getChildren().add(valueNode);
		return valueNode;
	}

	private TreeItem<TreeItemObjectWrapper<?>> addTreeItemNode(TreeItem<TreeItemObjectWrapper<?>> root,
			SupplierTaskWorker value) {
		Boolean autoExpand = this.autoExpandTreeItem.get(root);
		if (autoExpand == null) {
			autoExpand = root.isExpanded();
		}

		TreeItem<TreeItemObjectWrapper<?>> valueNode = new TreeItem<>(new TreeItemObjectWrapper<SupplierTaskWorker>(value),
				new ImageView(Images.iconSupplier));
		valueNode.setExpanded(autoExpand);

		final TreeItem<TreeItemObjectWrapper<?>> workerIdTreeItem = new TreeItem<>(new TreeItemObjectWrapper<String>(
				String.format("%s unknown", this.bundle.getString("facility.label.supplier.Id"))), new ImageView(
				Images.iconWorkerId));
		value.getWorkerIdProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> value, String oldValue, String newValue) {
				// log.debug(String.format("UPDATE workerIdTreeItem with from %s to %s.",
				// oldValue, newValue));
				workerIdTreeItem.setValue(new TreeItemObjectWrapper<String>(newValue));
			}
		});
		valueNode.getChildren().add(workerIdTreeItem);

		final TreeItem<TreeItemObjectWrapper<?>> progressTreeItem = new TreeItem<>(new TreeItemObjectWrapper<String>(""),
				new ProgressBar(0));
		value.getTask().progressProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> value, Number oldValue, Number newValue) {
				// log.info(String.format("UPDATE progressTreeItem with from %s to %s.",
				// oldValue, newValue));
				((ProgressBar) progressTreeItem.getGraphic()).setProgress((double) newValue);
				progressTreeItem.setValue(new TreeItemObjectWrapper<String>(String.format("%.1f %%",
						((double) newValue) * 100)));
			}
		});
		valueNode.getChildren().add(progressTreeItem);

		final TreeItem<TreeItemObjectWrapper<?>> msgTreeItem = new TreeItem<>(new TreeItemObjectWrapper<String>(
				this.bundle.getString("facility.worker.initializeMessage")));
		value.getTask().messageProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> value, String oldValue, String newValue) {
				// log.info(String.format("UPDATE msgTreeItem with from %s to %s.",
				// oldValue, newValue));
				msgTreeItem.setValue(new TreeItemObjectWrapper<String>(newValue));
			}
		});
		valueNode.getChildren().add(msgTreeItem);

		value.setTreeItem(valueNode);
		this.supplierTaskWorkers.add(value);
		root.getChildren().add(valueNode);
		return valueNode;
	}

	private TreeItem<TreeItemObjectWrapper<?>> addTreeItemNode(TreeItem<TreeItemObjectWrapper<?>> root, Item value) {
		Boolean autoExpand = this.autoExpandTreeItem.get(root);
		if (autoExpand == null) {
			autoExpand = root.isExpanded();
		}

		Map<Object, TreeItem<TreeItemObjectWrapper<?>>> objectMap = this.treeItemObjectMap.get(root);
		if (objectMap != null && objectMap.containsKey(value)) {
//			return objectMap.get(value);
			TreeItem<TreeItemObjectWrapper<?>> tItem = objectMap.get(value);
			if (tItem != null) {
				root.getChildren().remove(tItem);
				autoExpand = tItem.isExpanded();
				objectMap.remove(value);
			}
		}

		TreeItem<TreeItemObjectWrapper<?>> valueNode = new TreeItem<>(new TreeItemObjectWrapper<Item>(value));
		valueNode.setExpanded(autoExpand);

		valueNode.getChildren().add(
				new TreeItem<>(new TreeItemObjectWrapper<String>(String.format("%s %s",
						this.bundle.getString("facility.label.item.Id"), value.getId())), new ImageView(Images.iconId)));

		valueNode.getChildren().add(
				new TreeItem<>(new TreeItemObjectWrapper<String>(String.format("%s %s",
						this.bundle.getString("facility.label.item.created"), /*
																			 * formatter
																			 * .
																			 * format
																			 * (
																			 * value
																			 * .
																			 * getCreateDate
																			 * (
																			 * )
																			 * )
																			 */value.getCreateDate())), new ImageView(
						Images.iconCreated)));

		if (value instanceof Component) {
			Component cValue = (Component) value;
			valueNode.getChildren().add(
					new TreeItem<>(new TreeItemObjectWrapper<String>(String.format("%s %s",
							this.bundle.getString("facility.label.component.supplierId"), cValue.getSupplierId())),
							new ImageView(Images.iconWorkerId)));
		}

		if (value instanceof RecycleableComponent) {
			RecycleableComponent cValue = (RecycleableComponent) value;
			if (cValue.getRecycleCount() > 0) {
				valueNode.getChildren().add(
						new TreeItem<>(new TreeItemObjectWrapper<String>(String.format("%s %d",
								this.bundle.getString("facility.label.recycleablecomponent.recycled"),
								cValue.getRecycleCount())), new ImageView(Images.iconRecycled)));
			}
		}

		if (value instanceof Casing) {
			valueNode.setGraphic(new ImageView(Images.iconCasing));
		} else if (value instanceof Clockwork) {
			valueNode.setGraphic(new ImageView(Images.iconClockwork));
		} else if (value instanceof Hand) {
			valueNode.setGraphic(new ImageView(Images.iconHand));
		} else if (value instanceof LeatherWristband) {
			valueNode.setGraphic(new ImageView(Images.iconWristbandLeather));
		} else if (value instanceof MetalWristband) {
			valueNode.setGraphic(new ImageView(Images.iconWristbandMetal));
		} else if (value instanceof Watch) {
			valueNode.setGraphic(new ImageView(Images.iconWatch));

			Watch cValue = (Watch) value;
			if (cValue.getAssemblyWorkerId() != null) {
				valueNode.getChildren().add(
						new TreeItem<>(new TreeItemObjectWrapper<String>(
								String.format("%s %s", this.bundle.getString("facility.label.watch.assemblyWorkerId"),
										cValue.getAssemblyWorkerId())), new ImageView(Images.iconWorkerId)));
			}
			if (cValue.getQaWorkerId() != null) {
				valueNode.getChildren().add(
						new TreeItem<>(new TreeItemObjectWrapper<String>(String.format("%s %s",
								this.bundle.getString("facility.label.watch.qaWorkerId"), cValue.getQaWorkerId())),
								new ImageView(Images.iconWorkerId)));
			}
			if (cValue.getLogisticsWorkerId() != null) {
				valueNode.getChildren().add(
						new TreeItem<>(new TreeItemObjectWrapper<String>(String.format("%s %s",
								this.bundle.getString("facility.label.watch.logisticsWorkerId"),
								cValue.getLogisticsWorkerId())), new ImageView(Images.iconWorkerId)));
			}
			if (cValue.getQualityRating() != null) {
				valueNode.getChildren().add(
						new TreeItem<>(new TreeItemObjectWrapper<String>(String.format("%s %d",
								this.bundle.getString("facility.label.watch.qualityRating"), cValue.getQualityRating())),
								new ImageView(Images.iconRating)));
			}
			if (cValue.getOrderId() != null) {
				valueNode.getChildren().add(
						new TreeItem<>(new TreeItemObjectWrapper<String>(String.format("%s %s",
								this.bundle.getString("facility.label.watch.order"), cValue.getOrderId())), new ImageView(
								Images.iconOrderId)));
			}
			if (cValue.getModificationDate() != null) {
				valueNode.getChildren().add(
						new TreeItem<>(new TreeItemObjectWrapper<String>(String.format("%s %s",
								this.bundle.getString("facility.label.watch.modified"), /*
																						 * formatter
																						 * .
																						 * format
																						 * (
																						 * cValue
																						 * .
																						 * getModificationDate
																						 * (
																						 * )
																						 * )
																						 */cValue.getModificationDate())),
								new ImageView(Images.iconModified)));
			}

			TreeItem<TreeItemObjectWrapper<?>> componentsNode = new TreeItem<>(new TreeItemObjectWrapper<String>(
					this.bundle.getString("facility.label.watch.components")), new ImageView(Images.iconComponents));
			componentsNode.setExpanded(valueNode.isExpanded());

			cValue.getComponents().forEach(e -> this.addTreeItemNode(componentsNode, e));

			valueNode.getChildren().add(componentsNode);
		} else if (value instanceof Order) {
			valueNode.setGraphic(new ImageView(Images.iconOrder));

			Order cValue = (Order) value;
			if (cValue.getModificationDate() != null) {
				valueNode.getChildren().add(
						new TreeItem<>(new TreeItemObjectWrapper<String>(String.format("%s %s",
								this.bundle.getString("facility.label.watch.modified"), /*
																						 * formatter
																						 * .
																						 * format
																						 * (
																						 * cValue
																						 * .
																						 * getModificationDate
																						 * (
																						 * )
																						 * )
																						 */cValue.getModificationDate())),
								new ImageView(Images.iconModified)));
			}
			valueNode.getChildren().add(
					new TreeItem<>(new TreeItemObjectWrapper<String>(this.bundle.getString("priority."
							+ cValue.getPriority())), new ImageView(Images.iconPriority)));
			if (cValue.getOrderCountClassicWatch() > 0) {
				TreeItem<TreeItemObjectWrapper<?>> watchesNode = new TreeItem<>(new TreeItemObjectWrapper<String>(
						String.format("%d / %d %s",
								cValue.getOrderCountClassicWatch() - cValue.getOpenOrderCountClassicWatch(),
								cValue.getOrderCountClassicWatch(),
								this.bundle.getString("facility.label.order.classicWatch"))), new ImageView(
						Images.iconOrderWatches));

				if (cValue.getOrderCountClassicWatch() - cValue.getOpenOrderCountClassicWatch() != 0) {
					cValue.getOrderedWatches().get(WatchType.CLASSIC_WATCH)
							.forEach(e -> this.addTreeItemNode(watchesNode, e));
				}

				valueNode.getChildren().add(watchesNode);
			}
			if (cValue.getOrderCountSportWatch() > 0) {
				TreeItem<TreeItemObjectWrapper<?>> watchesNode = new TreeItem<>(
						new TreeItemObjectWrapper<String>(String.format("%d / %d %s", cValue.getOrderCountSportWatch()
								- cValue.getOpenOrderCountSportWatch(), cValue.getOrderCountSportWatch(),
								this.bundle.getString("facility.label.order.sportsWatch"))), new ImageView(
								Images.iconOrderWatches));

				if (cValue.getOrderCountSportWatch() - cValue.getOpenOrderCountSportWatch() != 0) {
					cValue.getOrderedWatches().get(WatchType.SPORT_WATCH).forEach(e -> this.addTreeItemNode(watchesNode, e));
				}

				valueNode.getChildren().add(watchesNode);
			}
			if (cValue.getOrderCountSportWatchTZS() > 0) {
				TreeItem<TreeItemObjectWrapper<?>> watchesNode = new TreeItem<>(new TreeItemObjectWrapper<String>(
						String.format("%d / %d %s",
								cValue.getOrderCountSportWatchTZS() - cValue.getOpenOrderCountSportWatchTZS(),
								cValue.getOrderCountSportWatchTZS(),
								this.bundle.getString("facility.label.order.sportsTzWatch"))), new ImageView(
						Images.iconOrderWatches));

				if (cValue.getOrderCountSportWatchTZS() - cValue.getOpenOrderCountSportWatchTZS() != 0) {
					cValue.getOrderedWatches().get(WatchType.SPORT_WATCH_TZS)
							.forEach(e -> this.addTreeItemNode(watchesNode, e));
				}

				valueNode.getChildren().add(watchesNode);
			}
		}

		root.getChildren().add(valueNode);
		if (objectMap != null) {
			objectMap.put(value, valueNode);
		}
		return valueNode;
	}

	private class NotificationCallback implements ItemUpdateCallback {

		private final DepotId depotId;

		public NotificationCallback(DepotId depotId) {
			super();
			this.depotId = depotId;
		}

		@Override
		public void notifyTake(List<? extends Item> items) {
			FacilityControl.this.notifationQueue
					.add(new FactoryNotificationEntry(NotificationType.TAKE, this.depotId, items));
		}

		@Override
		public void notifyPut(List<? extends Item> items) {
			FacilityControl.this.notifationQueue
					.add(new FactoryNotificationEntry(NotificationType.PUT, this.depotId, items));
		}
	}

	private class NotificationProcessor extends Task<Void> {
		@Override
		public Void call() {
			if (FacilityControl.this.isUiUpdateRunning()) {
				log.warn("UI update dropped, because previouse update process is still running. Should not happen!");
				return null;
			} else {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						FacilityControl.this.setUiUpdateRunning(true);
						log.debug("Updating user interface...");

						final List<FactoryNotificationEntry> entryList = new ArrayList<>();
						try {
							FactoryNotificationEntry entry;

							// update tree views
							while ((entry = FacilityControl.this.notifationQueue.poll()) != null && !Thread.interrupted()) {
								entryList.add(entry);
								final TreeItem<TreeItemObjectWrapper<?>> treeItem;
								switch (entry.getDepotId()) {
								case COMPONENTS:
									treeItem = FacilityControl.this.triComponents;
									break;
								case ASSEMBLED:
									treeItem = FacilityControl.this.triAssembled;
									break;
								case TESTED:
									treeItem = FacilityControl.this.triTested;
									break;
								case DELIVERY:
									treeItem = FacilityControl.this.triDelivery;
									break;
								case DELIVERED:
									treeItem = FacilityControl.this.triDelivered;
									break;
								case RECYCLED:
									treeItem = FacilityControl.this.triRecycled;
									break;
								case ORDERS:
									treeItem = FacilityControl.this.triOrders;
//									log.warn(String.format("ORDER type %s", entry.getType().name()));
									break;
								case DISTRIBUTORS:
									FactoryNotificationEntry.NotificationType type = entry.getType();
									entry.getItems()
											.forEach(
													e -> {
														if (e instanceof DistributorRegistration) {
															DistributorRegistration dReg = (DistributorRegistration) e;
															log.info(String.format(
																	"dReg %s: %s [C: %d/%d; S: %s/%d; STZ: %d/%d]",
																	type.name(),
																	dReg.getWorkerId(),
																	dReg.getStockInfo(WatchType.CLASSIC_WATCH)
																			.getStockCount(),
																	dReg.getStockInfo(WatchType.CLASSIC_WATCH)
																			.getStockCapacity(),
																	dReg.getStockInfo(WatchType.SPORT_WATCH).getStockCount(),
																	dReg.getStockInfo(WatchType.SPORT_WATCH)
																			.getStockCapacity(),
																	dReg.getStockInfo(WatchType.SPORT_WATCH_TZS)
																			.getStockCount(),
																	dReg.getStockInfo(WatchType.SPORT_WATCH_TZS)
																			.getStockCapacity()));
														}
													});
									continue;
								default:
									log.info(String.format("No tree view update for entry for depot %s.", entry.getDepotId()));
									continue;
								}

								switch (entry.getType()) {
								case TAKE:
									Map<Object, TreeItem<TreeItemObjectWrapper<?>>> objectMap = FacilityControl.this.treeItemObjectMap
											.get(treeItem);
									entry.getItems().forEach(
											e -> {
												// hack for order display
												if ( !(e instanceof Order) ) {
													TreeItem<TreeItemObjectWrapper<?>> tItem = objectMap.get(e);
													if (tItem != null) {
														treeItem.getChildren().remove(tItem);
														objectMap.remove(e);
													} else {
														log.warn(String.format(
																"WARNING! Could not find ui treeItem for object \"%s\"!",
																e.toString()));
													}
												}
											});
									break;
								case PUT:
									entry.getItems().forEach(e -> FacilityControl.this.addTreeItemNode(treeItem, e));
									break;
								}
							}

							// update statistics
							FacilityControl.this.statistics.forEach(e -> e.process(entryList));
						} catch (Exception e) {
							log.error("User interface update exception.", e);
						}
						log.debug(String.format("Updating user interface done. Processed %d notification entries.",
								entryList.size()));
						FacilityControl.this.setUiUpdateRunning(false);
					}
				});
			}

			return null;
		}
	}

	private class PurchaseOrder {
		private final Map<ComponentType, Integer> orderAmounts = new HashMap<>();

		public PurchaseOrder(int casingCount, int clockworkCount, int handCount, int wristbandLCount, int wristbandMCount) {
			this.orderAmounts.put(ComponentType.CASING, casingCount);
			this.orderAmounts.put(ComponentType.CLOCKWORK, clockworkCount);
			this.orderAmounts.put(ComponentType.HAND, handCount);
			this.orderAmounts.put(ComponentType.LEATHER_WRISTBAND, wristbandLCount);
			this.orderAmounts.put(ComponentType.METAL_WRISTBAND, wristbandMCount);
		}

		@Override
		public String toString() {
			return "Purchase order";
		}
	}

}
