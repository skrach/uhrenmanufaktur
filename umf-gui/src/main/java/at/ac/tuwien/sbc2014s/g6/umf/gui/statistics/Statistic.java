package at.ac.tuwien.sbc2014s.g6.umf.gui.statistics;

import java.util.List;

import at.ac.tuwien.sbc2014s.g6.umf.gui.FactoryNotificationEntry;

public interface Statistic {

	/**
	 * Processes notification entries to update statistical data.
	 *
	 * @param notificationEntries
	 *            Unmodifiable list of notification entries
	 */
	public void process(List<FactoryNotificationEntry> notificationEntries);

	/**
	 * Returns the current statistical data.
	 *
	 * @return List of current statistics data
	 */
	public List<StatisticValue> getCurrentData();

}
