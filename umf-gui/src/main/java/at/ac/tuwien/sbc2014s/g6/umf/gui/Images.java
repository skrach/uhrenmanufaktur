package at.ac.tuwien.sbc2014s.g6.umf.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.scene.image.Image;
import at.ac.tuwien.sbc2014s.g6.umf.worker.supplier.ComponentType;

public class Images {
	public static final Image iconOrder = new Image("/images/order_16.png");
	public static final Image iconSupplier = new Image("/images/orderproc_16.png");
	public static final Image iconCasing = new Image("/images/casing_16.png");
	public static final Image iconClockwork = new Image("/images/clockwork_16.png");
	public static final Image iconHand = new Image("/images/hands_16.png");
	public static final Image iconWristbandLeather = new Image("/images/wristbandleather_16.png");
	public static final Image iconWristbandMetal = new Image("/images/wristbandmetal_16.png");
	public static final Image iconId = new Image("/images/id_16.png");
	public static final Image iconWorkerId = new Image("/images/workerid_16.png");
	public static final Image iconCreated = new Image("/images/created_16.png");
	public static final Image iconRecycled = new Image("/images/recycled_16.png");
	public static final Image iconWatch = new Image("/images/clock_16.png");
	public static final Image iconComponents = new Image("/images/components_16.png");
	public static final Image iconRating = new Image("/images/rating_16.png");
	public static final Image iconModified = new Image("/images/modified_16.png");
	public static final Image iconWorker = new Image("/images/worker_16.png");
	public static final Image iconNewWorkerUtil = new Image("/images/newworkerutil_16.png");
	public static final Image iconPriority = new Image("/images/prio_16.png");
	public static final Image iconOrderWatches = new Image("/images/workerutil_16.png");
	public static final Image iconOrderId = new Image("/images/orderid_16.png");

	public static final Map<ComponentType, Image> componentImages = new HashMap<>();

	public static final List<Image> workerUtilImages = new ArrayList<>();

	static {
		componentImages.put(ComponentType.CASING, Images.iconCasing);
		componentImages.put(ComponentType.CLOCKWORK, Images.iconClockwork);
		componentImages.put(ComponentType.HAND, Images.iconHand);
		componentImages.put(ComponentType.LEATHER_WRISTBAND, Images.iconWristbandLeather);
		componentImages.put(ComponentType.METAL_WRISTBAND, Images.iconWristbandMetal);

		workerUtilImages.add(new Image("/images/workerutil_16.png"));
		workerUtilImages.add(new Image("/images/workerutil_32.png"));
		workerUtilImages.add(new Image("/images/workerutil_48.png"));
		workerUtilImages.add(new Image("/images/workerutil_64.png"));
		workerUtilImages.add(new Image("/images/workerutil_128.png"));
	}
}
