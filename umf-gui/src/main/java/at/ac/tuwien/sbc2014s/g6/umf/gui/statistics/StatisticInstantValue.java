package at.ac.tuwien.sbc2014s.g6.umf.gui.statistics;

import java.time.Instant;

public class StatisticInstantValue extends StatisticValue {

//	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss.SSS (yyyy-MM-dd)");

	private final Instant value;

	public Instant getValue() {
		return this.value;
	}

	public StatisticInstantValue(String name, Instant value) {
		super(name);
		this.value = value;
	}

	@Override
	public String toString() {
//		return (this.value == null ? "NULL" : formatter.format(this.value));
		return (this.value == null ? "NULL" : this.value.toString());
	}

}
