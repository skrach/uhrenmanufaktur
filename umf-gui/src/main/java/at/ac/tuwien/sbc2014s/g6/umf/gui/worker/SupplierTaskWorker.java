package at.ac.tuwien.sbc2014s.g6.umf.gui.worker;

import java.util.ResourceBundle;

import at.ac.tuwien.sbc2014s.g6.umf.worker.supplier.ComponentType;

public class SupplierTaskWorker extends TaskWorker {

	private final ComponentType type;

	public ComponentType getType() {
		return this.type;
	}

	private final int amount;

	public int getAmount() {
		return this.amount;
	}

	private final ResourceBundle bundle;

	public SupplierTaskWorker(ComponentType type, int amount, ResourceBundle bundle) {
		this.type = type;
		this.amount = amount;
		this.bundle = bundle;
	}

	@Override
	public String toString() {
		return String.format("%s %s (%d)", this.bundle.getString("supplier.type." + this.type.name().toLowerCase()),
				SupplierTaskWorker.class.getSimpleName(), this.amount);
	}
}
