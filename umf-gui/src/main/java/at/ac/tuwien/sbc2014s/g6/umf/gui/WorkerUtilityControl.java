package at.ac.tuwien.sbc2014s.g6.umf.gui;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ac.tuwien.sbc2014s.g6.umf.gui.worker.AmTaskWorker;
import at.ac.tuwien.sbc2014s.g6.umf.gui.worker.AssemblyTaskWorker;
import at.ac.tuwien.sbc2014s.g6.umf.gui.worker.LogisticsTaskWorker;
import at.ac.tuwien.sbc2014s.g6.umf.gui.worker.QaTaskWorker;
import at.ac.tuwien.sbc2014s.g6.umf.gui.worker.TaskWorker;
import at.ac.tuwien.sbc2014s.g6.umf.worker.accountmanager.AccountManagerWorker;
import at.ac.tuwien.sbc2014s.g6.umf.worker.assembly.AssemblyWorker;
import at.ac.tuwien.sbc2014s.g6.umf.worker.logistics.LogisticsWorker;
import at.ac.tuwien.sbc2014s.g6.umf.worker.logistics.LogisticsWorker.QaType;
import at.ac.tuwien.sbc2014s.g6.umf.worker.qa.QaWorker;

public class WorkerUtilityControl extends ScrollPane implements Initializable {

	private static final Logger log = LogManager.getLogger(WorkerUtilityControl.class);

	private final Map<TreeItem<TreeItemObjectWrapper<?>>, List<TaskWorker>> taskWorkers = new HashMap<>();
	private final Map<TreeItem<TreeItemObjectWrapper<?>>, Boolean> autoExpandTreeItem = new HashMap<>();

	@FXML
	private TreeView<TreeItemObjectWrapper<?>> trvAssemblyWorkers;
	private TreeItem<TreeItemObjectWrapper<?>> triAssemblyWorkers;
	@FXML
	private TitledPane tpnAssemblyWorkers;

	@FXML
	private TreeView<TreeItemObjectWrapper<?>> trvQaWorkers;
	private TreeItem<TreeItemObjectWrapper<?>> triQaWorkers;
	@FXML
	private TitledPane tpnQaWorkers;

	@FXML
	private TreeView<TreeItemObjectWrapper<?>> trvLogisticsWorkers;
	private TreeItem<TreeItemObjectWrapper<?>> triLogisticsWorkers;
	@FXML
	private TitledPane tpnLogisticsWorkers;

	@FXML
	private TreeView<TreeItemObjectWrapper<?>> trvAmWorkers;
	private TreeItem<TreeItemObjectWrapper<?>> triAmWorkers;
	@FXML
	private TitledPane tpnAmWorkers;

	@FXML
	private Button btnAssemblyWorkerStop;

	@FXML
	private Button btnAssemblyWorkerAdd;

	@FXML
	private Button btnQaWorkerStop;

	@FXML
	private Button btnQaWorkerAdd;

	@FXML
	private Button btnLogisticsWorkerStop;

	@FXML
	private Button btnLogisticsWorkerAdd;

	@FXML
	private Button btnAmWorkerStop;

	@FXML
	private Button btnAmWorkerAdd;

	@FXML
	private ToggleButton tbtClassA;

	@FXML
	private ToggleButton tbtClassB;

	private final String psn;

	private ResourceBundle bundle;

	/**
	 * Constructor that loads the fxml
	 *
	 * @param psn
	 *            Provider source name for facility
	 */
	public WorkerUtilityControl(String psn) {
		this.psn = psn;

		// load component fxml
		FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/workerutil.fxml"), UmfGui.getBundle());
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		// initialize gui
		this.bundle = rb;

		ToggleGroup tGroup = new ToggleGroup();
		this.tbtClassA.setToggleGroup(tGroup);
		this.tbtClassB.setToggleGroup(tGroup);
		this.tbtClassA.setSelected(true);
		this.tbtClassA.addEventFilter(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				if (WorkerUtilityControl.this.tbtClassA.equals(tGroup.getSelectedToggle())) {
					mouseEvent.consume();
				}
			}
		});
		this.tbtClassB.addEventFilter(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				if (WorkerUtilityControl.this.tbtClassB.equals(tGroup.getSelectedToggle())) {
					mouseEvent.consume();
				}
			}
		});

		this.triAssemblyWorkers = new TreeItem<>();
		this.trvAssemblyWorkers.setRoot(this.triAssemblyWorkers);
		this.taskWorkers.put(this.triAssemblyWorkers, new ArrayList<>());
		this.autoExpandTreeItem.put(this.triAssemblyWorkers, true);
		this.triAssemblyWorkers.getChildren().addListener(new ListChangeListener<TreeItem<TreeItemObjectWrapper<?>>>() {
			@Override
			public void onChanged(Change<? extends TreeItem<TreeItemObjectWrapper<?>>> c) {
				String title = WorkerUtilityControl.this.bundle.getString("facility.title.assemblyWorkers");
				if (WorkerUtilityControl.this.triAssemblyWorkers.getChildren().isEmpty()) {
					WorkerUtilityControl.this.tpnAssemblyWorkers.setText(title);
				} else {
					WorkerUtilityControl.this.tpnAssemblyWorkers.setText(String.format("%s (%d)", title,
							WorkerUtilityControl.this.triAssemblyWorkers.getChildren().size()));
				}
			}
		});
		this.trvAssemblyWorkers.getSelectionModel().selectedItemProperty()
				.addListener(new ChangeListener<TreeItem<TreeItemObjectWrapper<?>>>() {
					@Override
					public void changed(ObservableValue<? extends TreeItem<TreeItemObjectWrapper<?>>> value,
							TreeItem<TreeItemObjectWrapper<?>> oldValue, TreeItem<TreeItemObjectWrapper<?>> newValue) {
						if (newValue != null && newValue.getParent() == WorkerUtilityControl.this.triAssemblyWorkers) {
							WorkerUtilityControl.this.btnAssemblyWorkerStop.setDisable(false);
						} else {
							WorkerUtilityControl.this.btnAssemblyWorkerStop.setDisable(true);
						}
					}
				});

		this.triQaWorkers = new TreeItem<>();
		this.trvQaWorkers.setRoot(this.triQaWorkers);
		this.taskWorkers.put(this.triQaWorkers, new ArrayList<>());
		this.autoExpandTreeItem.put(this.triQaWorkers, true);
		this.triQaWorkers.getChildren().addListener(new ListChangeListener<TreeItem<TreeItemObjectWrapper<?>>>() {
			@Override
			public void onChanged(Change<? extends TreeItem<TreeItemObjectWrapper<?>>> c) {
				String title = WorkerUtilityControl.this.bundle.getString("facility.title.qaWorkers");
				if (WorkerUtilityControl.this.triQaWorkers.getChildren().isEmpty()) {
					WorkerUtilityControl.this.tpnQaWorkers.setText(title);
				} else {
					WorkerUtilityControl.this.tpnQaWorkers.setText(String.format("%s (%d)", title,
							WorkerUtilityControl.this.triQaWorkers.getChildren().size()));
				}
			}
		});
		this.trvQaWorkers.getSelectionModel().selectedItemProperty()
				.addListener(new ChangeListener<TreeItem<TreeItemObjectWrapper<?>>>() {
					@Override
					public void changed(ObservableValue<? extends TreeItem<TreeItemObjectWrapper<?>>> value,
							TreeItem<TreeItemObjectWrapper<?>> oldValue, TreeItem<TreeItemObjectWrapper<?>> newValue) {
						if (newValue != null && newValue.getParent() == WorkerUtilityControl.this.triQaWorkers) {
							WorkerUtilityControl.this.btnQaWorkerStop.setDisable(false);
						} else {
							WorkerUtilityControl.this.btnQaWorkerStop.setDisable(true);
						}
					}
				});

		this.triLogisticsWorkers = new TreeItem<>();
		this.trvLogisticsWorkers.setRoot(this.triLogisticsWorkers);
		this.taskWorkers.put(this.triLogisticsWorkers, new ArrayList<>());
		this.autoExpandTreeItem.put(this.triLogisticsWorkers, true);
		this.triLogisticsWorkers.getChildren().addListener(new ListChangeListener<TreeItem<TreeItemObjectWrapper<?>>>() {
			@Override
			public void onChanged(Change<? extends TreeItem<TreeItemObjectWrapper<?>>> c) {
				String title = WorkerUtilityControl.this.bundle.getString("facility.title.logisticsWorkers");
				if (WorkerUtilityControl.this.triLogisticsWorkers.getChildren().isEmpty()) {
					WorkerUtilityControl.this.tpnLogisticsWorkers.setText(title);
				} else {
					WorkerUtilityControl.this.tpnLogisticsWorkers.setText(String.format("%s (%d)", title,
							WorkerUtilityControl.this.triLogisticsWorkers.getChildren().size()));
				}
			}
		});
		this.trvLogisticsWorkers.getSelectionModel().selectedItemProperty()
				.addListener(new ChangeListener<TreeItem<TreeItemObjectWrapper<?>>>() {
					@Override
					public void changed(ObservableValue<? extends TreeItem<TreeItemObjectWrapper<?>>> value,
							TreeItem<TreeItemObjectWrapper<?>> oldValue, TreeItem<TreeItemObjectWrapper<?>> newValue) {
						if (newValue != null && newValue.getParent() == WorkerUtilityControl.this.triLogisticsWorkers) {
							WorkerUtilityControl.this.btnLogisticsWorkerStop.setDisable(false);
						} else {
							WorkerUtilityControl.this.btnLogisticsWorkerStop.setDisable(true);
						}
					}
				});

		this.triAmWorkers = new TreeItem<>();
		this.trvAmWorkers.setRoot(this.triAmWorkers);
		this.taskWorkers.put(this.triAmWorkers, new ArrayList<>());
		this.autoExpandTreeItem.put(this.triAmWorkers, true);
		this.triAmWorkers.getChildren().addListener(new ListChangeListener<TreeItem<TreeItemObjectWrapper<?>>>() {
			@Override
			public void onChanged(Change<? extends TreeItem<TreeItemObjectWrapper<?>>> c) {
				String title = WorkerUtilityControl.this.bundle.getString("facility.title.accountManagerWorkers");
				if (WorkerUtilityControl.this.triAmWorkers.getChildren().isEmpty()) {
					WorkerUtilityControl.this.tpnAmWorkers.setText(title);
				} else {
					WorkerUtilityControl.this.tpnAmWorkers.setText(String.format("%s (%d)", title,
							WorkerUtilityControl.this.triAmWorkers.getChildren().size()));
				}
			}
		});
		this.trvAmWorkers.getSelectionModel().selectedItemProperty()
				.addListener(new ChangeListener<TreeItem<TreeItemObjectWrapper<?>>>() {
					@Override
					public void changed(ObservableValue<? extends TreeItem<TreeItemObjectWrapper<?>>> value,
							TreeItem<TreeItemObjectWrapper<?>> oldValue, TreeItem<TreeItemObjectWrapper<?>> newValue) {
						if (newValue != null && newValue.getParent() == WorkerUtilityControl.this.triAmWorkers) {
							WorkerUtilityControl.this.btnAmWorkerStop.setDisable(false);
						} else {
							WorkerUtilityControl.this.btnAmWorkerStop.setDisable(true);
						}
					}
				});
	}

	public void setTearDownHook(Stage stage) {
		if (stage == null) {
			if (this.getScene() == null) {
				throw new IllegalArgumentException("Cannot determine scene.");
			}
			stage = ((Stage) this.getScene().getWindow());
		}

		final EventHandler<WindowEvent> prevCloseHandler = stage.getOnCloseRequest();
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				if (prevCloseHandler != null) {
					prevCloseHandler.handle(event);
				}
				WorkerUtilityControl.this.tearDown();
			}
		});

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				WorkerUtilityControl.this.tearDown();
			}
		});
	}

	private void tearDown() {
		log.info(String.format("Tear down of %s (%s) ...", this.getClass().getName(), this.psn));
		this.taskWorkers.forEach((ti, l) -> l.forEach(e -> {
			if (e.getTask().cancel()) {
				log.info(String.format("Canceled %s.", e.getTask()));
			} else {
				log.info(String.format("Canceling of %s failed.", e.getTask()));
			}
		}));
	}

	@FXML
	private void btnAssemblyWorkerStopAction() {
		TreeItem<TreeItemObjectWrapper<?>> treeItem = this.trvAssemblyWorkers.getSelectionModel().getSelectedItem();
		((TaskWorker) treeItem.getValue().getValue()).getTask().cancel(true);
		this.triAssemblyWorkers.getChildren().remove(treeItem);
		this.btnAssemblyWorkerStop.setDisable(true);
	}

	@FXML
	private void btnAssemblyWorkerAddAction() {
		final AssemblyTaskWorker taskWorker = new AssemblyTaskWorker();

		Task<Void> task = new Task<Void>() {
			@Override
			protected Void call() throws Exception {
				AssemblyWorker worker = new AssemblyWorker(WorkerUtilityControl.this.psn) {
					@Override
					protected void infoProgress(int progress, Integer all) {
						super.infoProgress(progress, all);
						updateProgress(progress, all);
					}

					@Override
					protected void infoMessage(String msg) {
						super.infoMessage(msg);
						updateMessage(msg);
					}

					@Override
					protected void infoSuccess(String msg) {
						super.infoSuccess(msg);
						updateMessage(msg);
					}

					@Override
					protected void infoFailure(String msg, Throwable e) {
						super.infoFailure(msg, e);
						updateMessage(msg);
					}

					@Override
					public void setWorkerId(String workerId) {
						super.setWorkerId(workerId);
						Platform.runLater(new Runnable() {
							@Override
							public void run() {
								taskWorker.getWorkerIdProperty().setValue(workerId);
							}
						});
					}
				};

				try {
					worker.run();
				} catch (Exception e) {
					log.fatal("Exception thrown in worker thread.", e);
				}

				return null;
			}
		};

		taskWorker.setTask(task);
		this.addTreeItemNode(this.triAssemblyWorkers, taskWorker);

		Thread th = new Thread(task);
		th.setDaemon(true);
		th.start();
	}

	@FXML
	private void btnQaWorkerStopAction() {
		TreeItem<TreeItemObjectWrapper<?>> treeItem = this.trvQaWorkers.getSelectionModel().getSelectedItem();
		((TaskWorker) treeItem.getValue().getValue()).getTask().cancel(true);
		this.triQaWorkers.getChildren().remove(treeItem);
		this.btnQaWorkerStop.setDisable(true);
	}

	@FXML
	private void btnQaWorkerAddAction() {
		final QaTaskWorker taskWorker = new QaTaskWorker();

		Task<Void> task = new Task<Void>() {
			@Override
			protected Void call() throws Exception {
				QaWorker worker = new QaWorker(WorkerUtilityControl.this.psn) {
					@Override
					protected void infoProgress(int progress, Integer all) {
						super.infoProgress(progress, all);
						updateProgress(progress, all);
					}

					@Override
					protected void infoMessage(String msg) {
						super.infoMessage(msg);
						updateMessage(msg);
					}

					@Override
					protected void infoSuccess(String msg) {
						super.infoSuccess(msg);
						updateMessage(msg);
					}

					@Override
					protected void infoFailure(String msg, Throwable e) {
						super.infoFailure(msg, e);
						updateMessage(msg);
					}

					@Override
					public void setWorkerId(String workerId) {
						super.setWorkerId(workerId);
						Platform.runLater(new Runnable() {
							@Override
							public void run() {
								taskWorker.getWorkerIdProperty().setValue(workerId);
							}
						});
					}
				};

				try {
					worker.run();
				} catch (Exception e) {
					log.fatal("Exception thrown in worker thread.", e);
				}

				return null;
			}
		};

		taskWorker.setTask(task);
		this.addTreeItemNode(this.triQaWorkers, taskWorker);

		Thread th = new Thread(task);
		th.setDaemon(true);
		th.start();
	}

	@FXML
	private void btnLogisticsWorkerStopAction() {
		TreeItem<TreeItemObjectWrapper<?>> treeItem = this.trvLogisticsWorkers.getSelectionModel().getSelectedItem();
		((TaskWorker) treeItem.getValue().getValue()).getTask().cancel(true);
		this.triLogisticsWorkers.getChildren().remove(treeItem);
		this.btnLogisticsWorkerStop.setDisable(true);
	}

	@FXML
	private void btnLogisticsWorkerAddAction() {
		final LogisticsTaskWorker taskWorker;

		if (this.tbtClassA.isSelected()) {
			taskWorker = new LogisticsTaskWorker(QaType.A);
		} else {
			taskWorker = new LogisticsTaskWorker(QaType.B);
		}

		Task<Void> task = new Task<Void>() {
			@Override
			protected Void call() throws Exception {
				LogisticsWorker worker = new LogisticsWorker(WorkerUtilityControl.this.psn, taskWorker.getType()) {
					@Override
					protected void infoProgress(int progress, Integer all) {
						super.infoProgress(progress, all);
						updateProgress(progress, all);
					}

					@Override
					protected void infoMessage(String msg) {
						super.infoMessage(msg);
						updateMessage(msg);
					}

					@Override
					protected void infoSuccess(String msg) {
						super.infoSuccess(msg);
						updateMessage(msg);
					}

					@Override
					protected void infoFailure(String msg, Throwable e) {
						super.infoFailure(msg, e);
						updateMessage(msg);
					}

					@Override
					public void setWorkerId(String workerId) {
						super.setWorkerId(workerId);
						Platform.runLater(new Runnable() {
							@Override
							public void run() {
								taskWorker.getWorkerIdProperty().setValue(workerId);
							}
						});
					}
				};

				try {
					worker.run();
				} catch (Exception e) {
					log.fatal("Exception thrown in worker thread.", e);
				}

				return null;
			}
		};

		taskWorker.setTask(task);
		this.addTreeItemNode(this.triLogisticsWorkers, taskWorker);

		Thread th = new Thread(task);
		th.setDaemon(true);
		th.start();
	}

	@FXML
	private void btnAmWorkerStopAction() {
		TreeItem<TreeItemObjectWrapper<?>> treeItem = this.trvAmWorkers.getSelectionModel().getSelectedItem();
		((TaskWorker) treeItem.getValue().getValue()).getTask().cancel(true);
		this.triAmWorkers.getChildren().remove(treeItem);
		this.btnAmWorkerStop.setDisable(true);
	}

	@FXML
	private void btnAmWorkerAddAction() {
		final AmTaskWorker taskWorker = new AmTaskWorker();

		Task<Void> task = new Task<Void>() {
			@Override
			protected Void call() throws Exception {
				AccountManagerWorker worker = new AccountManagerWorker(WorkerUtilityControl.this.psn) {
					@Override
					protected void infoProgress(int progress, Integer all) {
						super.infoProgress(progress, all);
						updateProgress(progress, all);
					}

					@Override
					protected void infoMessage(String msg) {
						super.infoMessage(msg);
						updateMessage(msg);
					}

					@Override
					protected void infoSuccess(String msg) {
						super.infoSuccess(msg);
						updateMessage(msg);
					}

					@Override
					protected void infoFailure(String msg, Throwable e) {
						super.infoFailure(msg, e);
						updateMessage(msg);
					}

					@Override
					public void setWorkerId(String workerId) {
						super.setWorkerId(workerId);
						Platform.runLater(new Runnable() {
							@Override
							public void run() {
								taskWorker.getWorkerIdProperty().setValue(workerId);
							}
						});
					}
				};

				try {
					worker.run();
				} catch (Exception e) {
					log.fatal("Exception thrown in worker thread.", e);
				}

				return null;
			}
		};

		taskWorker.setTask(task);
		this.addTreeItemNode(this.triAmWorkers, taskWorker);

		Thread th = new Thread(task);
		th.setDaemon(true);
		th.start();
	}

	private TreeItem<TreeItemObjectWrapper<?>> addTreeItemNode(TreeItem<TreeItemObjectWrapper<?>> root, TaskWorker value) {
		Boolean autoExpand = this.autoExpandTreeItem.get(root);
		if (autoExpand == null) {
			autoExpand = root.isExpanded();
		}

		TreeItem<TreeItemObjectWrapper<?>> valueNode = new TreeItem<>(new TreeItemObjectWrapper<TaskWorker>(value),
				new ImageView(Images.iconWorker));
		valueNode.setExpanded(autoExpand);

		final TreeItem<TreeItemObjectWrapper<?>> workerIdTreeItem = new TreeItem<>(new TreeItemObjectWrapper<String>(
				String.format("%s unknown", this.bundle.getString("facility.label.worker.Id"))), new ImageView(
				Images.iconWorkerId));
		value.getWorkerIdProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> value, String oldValue, String newValue) {
				// log.info(String.format("UPDATE workerIdTreeItem with from %s to %s.",
				// oldValue, newValue));
				workerIdTreeItem.setValue(new TreeItemObjectWrapper<String>(newValue));
			}
		});
		valueNode.getChildren().add(workerIdTreeItem);

		final TreeItem<TreeItemObjectWrapper<?>> progressTreeItem = new TreeItem<>(new TreeItemObjectWrapper<String>(""),
				new ProgressBar(0));
		value.getTask().progressProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> value, Number oldValue, Number newValue) {
				// log.info(String.format("UPDATE progressTreeItem with from %s to %s.",
				// oldValue, newValue));
				((ProgressBar) progressTreeItem.getGraphic()).setProgress((double) newValue);
				progressTreeItem.setValue(new TreeItemObjectWrapper<String>(String.format("%.1f %%",
						((double) newValue) * 100)));
			}
		});
		valueNode.getChildren().add(progressTreeItem);

		final TreeItem<TreeItemObjectWrapper<?>> msgTreeItem = new TreeItem<>(new TreeItemObjectWrapper<String>(
				this.bundle.getString("facility.worker.initializeMessage")));
		value.getTask().messageProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> value, String oldValue, String newValue) {
				// log.info(String.format("UPDATE msgTreeItem with from %s to %s.",
				// oldValue, newValue));
				msgTreeItem.setValue(new TreeItemObjectWrapper<String>(newValue));
			}
		});
		valueNode.getChildren().add(msgTreeItem);

		this.taskWorkers.get(root).add(value);
		root.getChildren().add(valueNode);
		return valueNode;
	}

}
