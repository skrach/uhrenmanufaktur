package at.ac.tuwien.sbc2014s.g6.umf.gui.worker;

import at.ac.tuwien.sbc2014s.g6.umf.worker.logistics.LogisticsWorker;


public class LogisticsTaskWorker extends TaskWorker {

	private final LogisticsWorker.QaType type;

	public LogisticsWorker.QaType getType() {
		return this.type;
	}

	public LogisticsTaskWorker(LogisticsWorker.QaType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return String.format("%s (%s)", this.getClass().getSimpleName(), this.type.name());
	}
}
