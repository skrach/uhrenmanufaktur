package at.ac.tuwien.sbc2014s.g6.umf.gui.worker;


public class AssemblyTaskWorker extends TaskWorker {

	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	}
}
