package at.ac.tuwien.sbc2014s.g6.umf.gui.config;

import org.apache.commons.configuration.Configuration;

import at.ac.tuwien.sbc2014s.g6.umf.util.Config;

public class DistributorGuiConfig {

	public static final String FACTORY_PSN = "Factory.ProviderSourceName";
	public static final String DISTRIBUTOR_SERVICECLASSNAME = "Distributor.ServiceClassName";
	public static final String DISTRIBUTOR_UIUPDATEINTERVAL = "Distributor.UiUpdateInterval";

	public static Configuration getConfiguration() {
		return Config.getConfig("distributorgui.properties");
	}
}
