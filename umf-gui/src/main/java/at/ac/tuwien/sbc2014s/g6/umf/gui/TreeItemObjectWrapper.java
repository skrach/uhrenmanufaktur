package at.ac.tuwien.sbc2014s.g6.umf.gui;

public class TreeItemObjectWrapper<T> {

	public TreeItemObjectWrapper(T value) {
		super();
		this.value = value;
	}

	private T value;

	public T getValue() {
		return this.value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return this.value.toString();
	}

	@Override
	public int hashCode() {
		return this.value.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return this.value.equals(obj);
	}

}
