package at.ac.tuwien.sbc2014s.g6.umf.gui;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controlsfx.dialog.Dialogs;

import at.ac.tuwien.sbc2014s.g6.umf.gui.DistributorNotificationEntry.DepotId;
import at.ac.tuwien.sbc2014s.g6.umf.gui.DistributorNotificationEntry.NotificationType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.ItemUpdateCallback;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.WatchType;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LoadingProviderFailedException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsTransactionTimeoutException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.distributor.Distributor;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.Factory;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.FactoryTransaction;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Casing;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Clockwork;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Component;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.DistributorRegistration;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Hand;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Item;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.LeatherWristband;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.MetalWristband;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.RecycleableComponent;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.item.Watch;
import at.ac.tuwien.sbc2014s.g6.umf.worker.distributor.DistributorWorker;

public class DistributorControl extends VBox implements Initializable {

	private static final Logger log = LogManager.getLogger(DistributorControl.class);

	long uiUpdateInterval = 1000;

	private final Map<TreeItem<TreeItemObjectWrapper<?>>, Map<Object, TreeItem<TreeItemObjectWrapper<?>>>> treeItemObjectMap = new HashMap<>();
	private final Map<TreeItem<TreeItemObjectWrapper<?>>, Boolean> autoExpandTreeItem = new HashMap<>();

	public final ConcurrentLinkedQueue<DistributorNotificationEntry> notifationQueue = new ConcurrentLinkedQueue<>();
	private ScheduledService<Void> uiUpdateService;
	private boolean uiUpdateRunning = false;

	public synchronized boolean isUiUpdateRunning() {
		return this.uiUpdateRunning;
	}

	public synchronized void setUiUpdateRunning(boolean uiUpdateRunning) {
		this.uiUpdateRunning = uiUpdateRunning;
	}

	@FXML
	private TreeView<TreeItemObjectWrapper<?>> trvStock;
	private TreeItem<TreeItemObjectWrapper<?>> triStock;
	@FXML
	private TitledPane tpnStock;

	@FXML
	private TreeView<TreeItemObjectWrapper<?>> trvSold;
	private TreeItem<TreeItemObjectWrapper<?>> triSold;
	@FXML
	private TitledPane tpnSold;

	@FXML
	private TitledPane tpnDistributor;

	@FXML
	private Button btnStart;

	@FXML
	private Button btnStop;

	@FXML
	private Button btnUpload;

	@FXML
	private TextField txfClassicWatch;

	@FXML
	private TextField txfSportsWatch;

	@FXML
	private TextField txfSportsWatchTZ;

	@FXML
	private ImageView imgPlus;

	@FXML
	private Label lblMessage;

	@FXML
	private StackPane stpDepots;

	private final String factoryPsn;
	private final String distributorServiceClassName;

	private Distributor distributor = null;

	private ResourceBundle bundle;

	Task<Void> workerTask = null;
	Thread workerThread = null;

	private final SimpleBooleanProperty isStarted = new SimpleBooleanProperty(false);

	public BooleanProperty getIsStartedProperty() {
		return this.isStarted;
	}

	/**
	 * Constructor that loads the fxml
	 *
	 * @param factoryPsn
	 *            Provider source name for facility
	 */
	public DistributorControl(String factoryPsn, String distributorServiceClassName, long uiUpdateInterval) {
		this.factoryPsn = factoryPsn;
		this.distributorServiceClassName = distributorServiceClassName;
		this.uiUpdateInterval = uiUpdateInterval;

		// load component fxml
		FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/distributor.fxml"), DistributorGui.getBundle());
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		// initialize gui
		this.bundle = rb;

		// initialize TreeViews
		this.triStock = new TreeItem<>();
		this.trvStock.setRoot(this.triStock);
		this.treeItemObjectMap.put(this.triStock, new HashMap<>());
		this.autoExpandTreeItem.put(this.triStock, false);

		this.triSold = new TreeItem<>();
		this.trvSold.setRoot(this.triSold);
		this.treeItemObjectMap.put(this.triSold, new HashMap<>());
		this.autoExpandTreeItem.put(this.triSold, false);

		// initialize ui update service
		this.uiUpdateService = new ScheduledService<Void>() {
			@Override
			protected Task<Void> createTask() {
				return new NotificationProcessor();
			}
		};
		this.uiUpdateService.setPeriod(Duration.millis(this.uiUpdateInterval));
		this.uiUpdateService.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			@Override
			public void handle(WorkerStateEvent event) {
				// update depot titles
				String title;

				title = DistributorControl.this.bundle.getString("distributor.title.stock");
				if (DistributorControl.this.triStock.getChildren().isEmpty()) {
					DistributorControl.this.tpnStock.setText(title);
				} else {
					DistributorControl.this.tpnStock.setText(String.format("%s (%d)", title,
							DistributorControl.this.triStock.getChildren().size()));
				}

				title = DistributorControl.this.bundle.getString("distributor.title.sold");
				if (DistributorControl.this.triSold.getChildren().isEmpty()) {
					DistributorControl.this.tpnSold.setText(title);
				} else {
					DistributorControl.this.tpnSold.setText(String.format("%s (%d)", title, DistributorControl.this.triSold
							.getChildren().size()));
				}
			}
		});

		this.uiUpdateService.start();
	}

	public void setTearDownHook(Stage stage) {
		if (stage == null) {
			if (this.getScene() == null) {
				throw new IllegalArgumentException("Cannot determine scene.");
			}
			stage = ((Stage) this.getScene().getWindow());
		}

		final EventHandler<WindowEvent> prevCloseHandler = stage.getOnCloseRequest();
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				if (prevCloseHandler != null) {
					prevCloseHandler.handle(event);
				}
				DistributorControl.this.tearDown();
			}
		});

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				DistributorControl.this.tearDown();
			}
		});
	}

	private void tearDown() {
		log.info(String.format("Tear down of %s (%s; %s) ...", this.getClass().getName(), this.factoryPsn,
				(this.distributor == null ? null : this.distributor.getDistributorPsn())));

		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				if ( DistributorControl.this.uiUpdateService != null ) {
					DistributorControl.this.uiUpdateService.cancel();
				}
			}
		});

		if (this.workerTask != null) {
			this.workerTask.cancel(true);
		}

		if (this.distributor != null) {
			try {
				this.distributor.stop();
			} catch (LogisticsException e) {
				String msg = "Failed stopping distributor interface.";
				log.fatal(msg, e);
				throw new RuntimeException(msg, e);
			}
		}

		// if (this.workerThread != null) {
		// this.workerThread.interrupt();
		// }
	}

	@FXML
	private void btnStartAction() {
		List<String> errors = new ArrayList<>();

		int classicCount = 0;
		if (!this.txfClassicWatch.getText().trim().isEmpty()) {
			try {
				classicCount = Integer.parseInt(this.txfClassicWatch.getText());
				if (classicCount < 0) {
					errors.add(this.bundle.getString("errormsg.classicWatchCount.range"));
				}
			} catch (NumberFormatException e) {
				errors.add(this.bundle.getString("errormsg.classicWatchCount.format"));
			}
		}

		int sportsCount = 0;
		if (!this.txfSportsWatch.getText().trim().isEmpty()) {
			try {
				sportsCount = Integer.parseInt(this.txfSportsWatch.getText());
				if (sportsCount < 0) {
					errors.add(this.bundle.getString("errormsg.sportsWatchCount.range"));
				}
			} catch (NumberFormatException e) {
				errors.add(this.bundle.getString("errormsg.sportsWatchCount.format"));
			}
		}

		int sportsTzCount = 0;
		if (!this.txfSportsWatchTZ.getText().trim().isEmpty()) {
			try {
				sportsTzCount = Integer.parseInt(this.txfSportsWatchTZ.getText());
				if (sportsTzCount < 0) {
					errors.add(this.bundle.getString("errormsg.sportsTzWatchCount.range"));
				}
			} catch (NumberFormatException e) {
				errors.add(this.bundle.getString("errormsg.sportsTzWatchCount.format"));
			}
		}

		if (errors.isEmpty()) {
			if (this.distributor == null) {
				this.startDistributor(classicCount, sportsCount, sportsTzCount);
			} else {
				this.updateDistributor(classicCount, sportsCount, sportsTzCount);
			}
		} else {
			Dialogs.create().title(this.bundle.getString("errormsg.inputError.title"))
					.masthead(this.bundle.getString("errormsg.inputError.masthead"))
					.message(errors.stream().collect(Collectors.joining("\n"))).nativeTitleBar().showWarning();
		}
	}

	private void startDistributor(int stockCountClassicWatch, int stockCountSportsWatch, int stockCountSportsWatchTZ) {
		this.workerTask = new Task<Void>() {
			@Override
			protected Void call() throws Exception {
				DistributorWorker worker = new DistributorWorker(DistributorControl.this.factoryPsn,
						DistributorControl.this.distributorServiceClassName, stockCountClassicWatch, stockCountSportsWatch,
						stockCountSportsWatchTZ) {
					@Override
					protected void infoProgress(int progress, Integer all) {
						super.infoProgress(progress, all);
						updateProgress(progress, all);
					}

					@Override
					protected void infoMessage(String msg) {
						super.infoMessage(msg);
						updateMessage(msg);
					}

					@Override
					protected void infoSuccess(String msg) {
						super.infoSuccess(msg);
						updateMessage(msg);
					}

					@Override
					protected void infoFailure(String msg, Throwable e) {
						super.infoFailure(msg, e);
						updateMessage(msg);
					}

					@Override
					public void setWorkerId(String workerId) {
						super.setWorkerId(workerId);
						Platform.runLater(new Runnable() {
							@Override
							public void run() {
								DistributorControl.this.tpnDistributor.setText(String.format("%s %s",
										DistributorControl.this.bundle.getString("distributor.title"), workerId));
							}
						});
					}

					@Override
					public void setDistributorPsn(String distributorPsn) {
						super.setDistributorPsn(distributorPsn);
						Platform.runLater(new Runnable() {
							@Override
							public void run() {
								DistributorControl.this.attachToDistributor(distributorPsn);
							}
						});
					}

				};

				try {
					worker.run();
				} catch (Exception e) {
					log.fatal("Exception thrown in worker thread.", e);
				}

				return null;
			}
		};

		this.workerTask.messageProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> value, String oldValue, String newValue) {
				log.debug(String.format("Update distributor worker message from \"%s\" to \"%s\".", oldValue, newValue));
				DistributorControl.this.lblMessage.setText(newValue);
			}
		});

		log.info("Starting DistributorWorker thread ...");
		this.workerThread = new Thread(this.workerTask);
		this.workerThread.setDaemon(true);
		this.workerThread.start();

		this.btnStart.setVisible(false);
		this.btnStop.setVisible(true);
		this.btnUpload.setDisable(false);
		this.isStarted.setValue(true);
		this.stpDepots.setDisable(false);
		this.imgPlus.setVisible(false);
	}

	protected void attachToDistributor(String psn) {
		try {
			this.distributor = Distributor.createDistributor(psn);
		} catch (LoadingProviderFailedException e) {
			String msg = String.format("Failed loading Distributor provider with psn \"%s\".", psn);
			log.fatal(msg, e);
			Dialogs.create().title(this.bundle.getString("errormsg.error.title"))
					.masthead(this.bundle.getString("errormsg.error.masthead")).nativeTitleBar().showException(e);
			return;
		}

		// initialize notification callbacks
		this.distributor.subscribeToDepotStock(new NotificationCallback(DepotId.STOCK));
		this.distributor.subscribeToDepotSold(new NotificationCallback(DepotId.SOLD));

		// start distributor interface
		try {
			this.distributor.start();
		} catch (LogisticsException e) {
			String msg = "Error starting distributor interface subscriptions.";
			log.fatal(msg, e);
			Dialogs.create().title(this.bundle.getString("errormsg.error.title"))
					.masthead(this.bundle.getString("errormsg.error.masthead")).nativeTitleBar().showException(e);
			return;
		}

		// read initial depot contents
		try {
			log.info("Reading current depot information...");
			this.notifationQueue.add(new DistributorNotificationEntry(NotificationType.PUT, DepotId.STOCK, this.distributor
					.readDepotStock()));
			this.notifationQueue.add(new DistributorNotificationEntry(NotificationType.PUT, DepotId.SOLD, this.distributor
					.readDepotSold()));
			log.info("Read current depot information.");
		} catch (LogisticsException e) {
			String msg = "Failed loading depot information.";
			log.error(msg, e);
			Dialogs.create().title(this.bundle.getString("errormsg.error.title"))
					.masthead(this.bundle.getString("errormsg.error.masthead")).nativeTitleBar().showException(e);
			return;
		}
	}

	private void updateDistributor(int stockCountClassicWatch, int stockCountSportsWatch, int stockCountSportsWatchTZ) {
		// initialize factory binding
		try {
			log.info(String.format("Connecting to Factory with psn \"%s\"", this.factoryPsn));
			Factory factory = Factory.createFactory(this.factoryPsn);
			factory.start();

			log.info(String.format("Updating registration for distributor with psn \"%s\"",
					this.distributor.getDistributorPsn()));
			FactoryTransaction transaction = factory.begin();
			DistributorRegistration dReg = transaction.takeDistributorRegistration(this.distributor.getDistributorPsn());
			dReg.setStockCapacity(WatchType.CLASSIC_WATCH, stockCountClassicWatch);
			dReg.setStockCapacity(WatchType.SPORT_WATCH, stockCountSportsWatch);
			dReg.setStockCapacity(WatchType.SPORT_WATCH_TZS, stockCountSportsWatchTZ);
			transaction.update(dReg);
			transaction.commit();
		} catch (LogisticsException | LogisticsTransactionException | InterruptedException
				| LogisticsTransactionTimeoutException | LoadingProviderFailedException e) {
			log.error("Error updating distributor registration.", e);
			Dialogs.create().title(this.bundle.getString("errormsg.error.title"))
					.masthead(this.bundle.getString("errormsg.error.masthead")).nativeTitleBar().showException(e);
		}
	}

	@FXML
	private void btnStopAction() {
		this.tearDown();
		this.isStarted.setValue(false);
	}

	private TreeItem<TreeItemObjectWrapper<?>> addTreeItemNode(TreeItem<TreeItemObjectWrapper<?>> root, Item value) {
		Boolean autoExpand = this.autoExpandTreeItem.get(root);
		if (autoExpand == null) {
			autoExpand = root.isExpanded();
		}

		Map<Object, TreeItem<TreeItemObjectWrapper<?>>> objectMap = this.treeItemObjectMap.get(root);
		if (objectMap != null && objectMap.containsKey(value)) {
			return objectMap.get(value);
		}

		TreeItem<TreeItemObjectWrapper<?>> valueNode = new TreeItem<>(new TreeItemObjectWrapper<Item>(value));
		valueNode.setExpanded(autoExpand);

		valueNode.getChildren().add(
				new TreeItem<>(new TreeItemObjectWrapper<String>(String.format("%s %s",
						this.bundle.getString("facility.label.item.Id"), value.getId())), new ImageView(Images.iconId)));

		valueNode.getChildren().add(
				new TreeItem<>(new TreeItemObjectWrapper<String>(String.format("%s %s",
						this.bundle.getString("facility.label.item.created"), value.getCreateDate())), new ImageView(
						Images.iconCreated)));

		if (value instanceof Component) {
			Component cValue = (Component) value;
			valueNode.getChildren().add(
					new TreeItem<>(new TreeItemObjectWrapper<String>(String.format("%s %s",
							this.bundle.getString("facility.label.component.supplierId"), cValue.getSupplierId())),
							new ImageView(Images.iconWorkerId)));
		}

		if (value instanceof RecycleableComponent) {
			RecycleableComponent cValue = (RecycleableComponent) value;
			if (cValue.getRecycleCount() > 0) {
				valueNode.getChildren().add(
						new TreeItem<>(new TreeItemObjectWrapper<String>(String.format("%s %d",
								this.bundle.getString("facility.label.recycleablecomponent.recycled"),
								cValue.getRecycleCount())), new ImageView(Images.iconRecycled)));
			}
		}

		if (value instanceof Casing) {
			valueNode.setGraphic(new ImageView(Images.iconCasing));
		} else if (value instanceof Clockwork) {
			valueNode.setGraphic(new ImageView(Images.iconClockwork));
		} else if (value instanceof Hand) {
			valueNode.setGraphic(new ImageView(Images.iconHand));
		} else if (value instanceof LeatherWristband) {
			valueNode.setGraphic(new ImageView(Images.iconWristbandLeather));
		} else if (value instanceof MetalWristband) {
			valueNode.setGraphic(new ImageView(Images.iconWristbandMetal));
		} else if (value instanceof Watch) {
			valueNode.setGraphic(new ImageView(Images.iconWatch));

			Watch cValue = (Watch) value;
			if (cValue.getAssemblyWorkerId() != null) {
				valueNode.getChildren().add(
						new TreeItem<>(new TreeItemObjectWrapper<String>(
								String.format("%s %s", this.bundle.getString("facility.label.watch.assemblyWorkerId"),
										cValue.getAssemblyWorkerId())), new ImageView(Images.iconWorkerId)));
			}
			if (cValue.getQaWorkerId() != null) {
				valueNode.getChildren().add(
						new TreeItem<>(new TreeItemObjectWrapper<String>(String.format("%s %s",
								this.bundle.getString("facility.label.watch.qaWorkerId"), cValue.getQaWorkerId())),
								new ImageView(Images.iconWorkerId)));
			}
			if (cValue.getLogisticsWorkerId() != null) {
				valueNode.getChildren().add(
						new TreeItem<>(new TreeItemObjectWrapper<String>(String.format("%s %s",
								this.bundle.getString("facility.label.watch.logisticsWorkerId"),
								cValue.getLogisticsWorkerId())), new ImageView(Images.iconWorkerId)));
			}
			if (cValue.getQualityRating() != null) {
				valueNode.getChildren().add(
						new TreeItem<>(new TreeItemObjectWrapper<String>(String.format("%s %d",
								this.bundle.getString("facility.label.watch.qualityRating"), cValue.getQualityRating())),
								new ImageView(Images.iconRating)));
			}
			if (cValue.getModificationDate() != null) {
				valueNode.getChildren().add(
						new TreeItem<>(new TreeItemObjectWrapper<String>(String.format("%s %s",
								this.bundle.getString("facility.label.watch.modified"), cValue.getModificationDate())),
								new ImageView(Images.iconModified)));
			}

			TreeItem<TreeItemObjectWrapper<?>> componentsNode = new TreeItem<>(new TreeItemObjectWrapper<String>(
					this.bundle.getString("facility.label.watch.components")), new ImageView(Images.iconComponents));
			componentsNode.setExpanded(valueNode.isExpanded());

			cValue.getComponents().forEach(e -> this.addTreeItemNode(componentsNode, e));

			valueNode.getChildren().add(componentsNode);
		}

		root.getChildren().add(valueNode);
		if (objectMap != null) {
			objectMap.put(value, valueNode);
		}
		return valueNode;
	}

	private class NotificationCallback implements ItemUpdateCallback {
		private final DepotId depotId;

		public NotificationCallback(DepotId depotId) {
			super();
			this.depotId = depotId;
		}

		@Override
		public void notifyTake(List<? extends Item> items) {
			DistributorControl.this.notifationQueue.add(new DistributorNotificationEntry(NotificationType.TAKE,
					this.depotId, items));
		}

		@Override
		public void notifyPut(List<? extends Item> items) {
			DistributorControl.this.notifationQueue.add(new DistributorNotificationEntry(NotificationType.PUT, this.depotId,
					items));
		}
	}

	private class NotificationProcessor extends Task<Void> {
		@Override
		public Void call() {
			if (DistributorControl.this.isUiUpdateRunning()) {
				log.warn("UI update dropped, because previouse update process is still running. Should not happen!");
				return null;
			} else {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						DistributorControl.this.setUiUpdateRunning(true);
						log.debug("Updating user interface...");

						final List<DistributorNotificationEntry> entryList = new ArrayList<>();
						try {
							DistributorNotificationEntry entry;

							// update tree views
							while ((entry = DistributorControl.this.notifationQueue.poll()) != null && !Thread.interrupted()) {
								entryList.add(entry);
								final TreeItem<TreeItemObjectWrapper<?>> treeItem;
								switch (entry.getDepotId()) {
								case STOCK:
									treeItem = DistributorControl.this.triStock;
									break;
								case SOLD:
									treeItem = DistributorControl.this.triSold;
									break;
								default:
									log.info(String.format("Dropped notification entry for depot %s.", entry.getDepotId()));
									continue;
								}

								switch (entry.getType()) {
								case TAKE:
									Map<Object, TreeItem<TreeItemObjectWrapper<?>>> objectMap = DistributorControl.this.treeItemObjectMap
											.get(treeItem);
									entry.getItems().forEach(e -> {
										TreeItem<TreeItemObjectWrapper<?>> tItem = objectMap.get(e);
										if (tItem != null) {
											treeItem.getChildren().remove(tItem);
										}
									});
									break;
								case PUT:
									entry.getItems().forEach(e -> DistributorControl.this.addTreeItemNode(treeItem, e));
									break;
								}
							}
						} catch (Exception e) {
							log.error("User interface update exception.", e);
						}
						log.debug(String.format("Updating user interface done. Processed %d notification entries.",
								entryList.size()));
						DistributorControl.this.setUiUpdateRunning(false);
					}
				});
			}

			return null;
		}
	}

}
