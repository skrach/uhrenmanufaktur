package at.ac.tuwien.sbc2014s.g6.umf.gui.statistics;

public class StatisticFloatValue extends StatisticValue {

	private final float value;

	public float getValue() {
		return this.value;
	}

	public StatisticFloatValue(String name, float value) {
		super(name);
		this.value = value;
	}

	@Override
	public String toString() {
		return String.format("%.3f", this.value);
	}

}
