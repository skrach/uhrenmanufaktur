package at.ac.tuwien.sbc2014s.g6.umf.gui.worker;

import javafx.beans.property.SimpleStringProperty;
import javafx.concurrent.Task;
import javafx.scene.control.TreeItem;
import at.ac.tuwien.sbc2014s.g6.umf.gui.TreeItemObjectWrapper;

public abstract class TaskWorker {

	private final SimpleStringProperty workerIdProperty = new SimpleStringProperty();

	public SimpleStringProperty getWorkerIdProperty() {
		return this.workerIdProperty;
	}

	private Task<Void> task;

	public Task<Void> getTask() {
		return this.task;
	}

	public void setTask(Task<Void> task) {
		this.task = task;
	}

	private TreeItem<TreeItemObjectWrapper<?>> treeItem;

	public TreeItem<TreeItemObjectWrapper<?>> getTreeItem() {
		return this.treeItem;
	}

	public void setTreeItem(TreeItem<TreeItemObjectWrapper<?>> treeItem) {
		this.treeItem = treeItem;
	}

}
