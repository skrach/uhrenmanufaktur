package at.ac.tuwien.sbc2014s.g6.umf.gui;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controlsfx.dialog.Dialogs;

import at.ac.tuwien.sbc2014s.g6.umf.gui.FactoryNotificationEntry.DepotId;
import at.ac.tuwien.sbc2014s.g6.umf.gui.FactoryNotificationEntry.NotificationType;
import at.ac.tuwien.sbc2014s.g6.umf.gui.config.UmfGuiConfig;
import at.ac.tuwien.sbc2014s.g6.umf.gui.statistics.CounterStatistics;
import at.ac.tuwien.sbc2014s.g6.umf.gui.statistics.Statistic;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LoadingProviderFailedException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.common.exception.LogisticsException;
import at.ac.tuwien.sbc2014s.g6.umf.logistics.factory.Factory;

public class UmfGui extends Application {

	private static final Logger log = LogManager.getLogger(UmfGui.class);

	static {
		Locale.setDefault(Locale.ENGLISH);
	}

	private static ResourceBundle bundle = ResourceBundle.getBundle("localization.umf-gui", Locale.getDefault());

	public static ResourceBundle getBundle() {
		return bundle;
	}

	@Override
	public void start(Stage stage) throws Exception {
		log.info("JavaFX Runtime Version: " + System.getProperties().get("javafx.runtime.version"));

		try {
			URL location = this.getClass().getResource("/umfgui.fxml");
			FXMLLoader fxmlLoader = new FXMLLoader();
			fxmlLoader.setLocation(location);
			fxmlLoader.setResources(getBundle());
			// fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());

			Parent root = fxmlLoader.load(location.openStream());
			Scene scene = new Scene(root);

			stage.setTitle(String.format("%s (%dms %s)", bundle.getString("applicationName"), UmfGuiConfig
					.getConfiguration().getLong(UmfGuiConfig.FACILITY_UIUPDATEINTERVAL), bundle
					.getString("title.updateInterval")));
			stage.getIcons().add(new Image("/images/icon_16.png"));
			stage.getIcons().add(new Image("/images/icon_32.png"));
			stage.getIcons().add(new Image("/images/icon_48.png"));
			stage.getIcons().add(new Image("/images/icon_64.png"));
			stage.getIcons().add(new Image("/images/icon_128.png"));
			stage.setScene(scene);

			UmfGuiController controller = fxmlLoader.getController();
			controller.setTearDownHook(stage);
			stage.show();
		} catch (Exception e) {
			String msg = "A runtime exception occurred.";
			log.error(msg, e);
			Dialogs.create().title(bundle.getString("errormsg.error.title"))
					.masthead(bundle.getString("errormsg.error.masthead")).nativeTitleBar().showException(e);
		}
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) {
		if (args.length > 0 && args[0].equals("dump")) {
			String[] facilityPSNs = UmfGuiConfig.getConfiguration().getStringArray(UmfGuiConfig.FACILITY_PSNLIST);

			if (facilityPSNs.length == 0) {
				log.warn(String.format("No \"%s\" properties found in configuration file.", UmfGuiConfig.FACILITY_PSNLIST));
			}

			for (String psn : facilityPSNs) {
				List<FactoryNotificationEntry> entries = new ArrayList<FactoryNotificationEntry>();

				try {
					log.info("Reading current depot information...");
					Factory f = Factory.createFactory(psn);
					f.start();
					entries.add(new FactoryNotificationEntry(NotificationType.PUT, DepotId.COMPONENTS, f.readDepotComponent()));
					entries.add(new FactoryNotificationEntry(NotificationType.PUT, DepotId.ASSEMBLED, f.readDepotAssembled()));
					entries.add(new FactoryNotificationEntry(NotificationType.PUT, DepotId.TESTED, f.readDepotTested()));
					entries.add(new FactoryNotificationEntry(NotificationType.PUT, DepotId.DELIVERY, f.readDepotDelivery()));
					entries.add(new FactoryNotificationEntry(NotificationType.PUT, DepotId.DELIVERED, f.readDepotDelivered()));
					entries.add(new FactoryNotificationEntry(NotificationType.PUT, DepotId.RECYCLED, f.readDepotRecycled()));
					entries.add(new FactoryNotificationEntry(NotificationType.PUT, DepotId.ORDERS, f.readDepotOrder()));
					entries.add(new FactoryNotificationEntry(NotificationType.PUT, DepotId.DISTRIBUTORS, f
							.readListDistributorRegistrations()));
					log.info("Read current depot information.");
					f.stop();
				} catch (LoadingProviderFailedException | LogisticsException e) {
					throw new RuntimeException(e);
				}

				// calc statistics
				List<Statistic> statistics = new ArrayList<>();
				statistics.add(new CounterStatistics());

				statistics.forEach(s -> s.process(entries));

				System.out.println(String.format("Stats for \"%s\":", psn));
				statistics.forEach(s -> s.getCurrentData().forEach(
						e -> System.out.println(String.format("%s\t%s", e.getName(), e.toString()))));

				Platform.exit();
			}
		} else {
			launch(args);
		}
	}
}
