package at.ac.tuwien.sbc2014s.g6.umf.gui.config;

import org.apache.commons.configuration.Configuration;

import at.ac.tuwien.sbc2014s.g6.umf.util.Config;

public class UmfGuiConfig {

	public static final String FACILITY_PSNLIST = "Facility.ProviderSourceName";
	public static final String FACILITY_UIUPDATEINTERVAL = "Facility.UiUpdateInterval";

	public static Configuration getConfiguration() {
		return Config.getConfig("umfgui.properties");
	}
}
